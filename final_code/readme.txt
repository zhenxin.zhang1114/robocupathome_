To use these files,firstly copy all folder to your own workspace

flowerfanggirl: save all srv and msg files, compile this file first use command:

$ catkin_make -DCATKIN_WHITELIST_PACKAGES="flowerfanggirl"

then catkin make other package with the same command one by one

for package final_manipulation, you should change the path in file "/final_manipulation/include/final_manipulation/final_manipulation_inc.h", 175th row, to your own file path. 

to use this whole project:
$ roslaunch pocketsphinx robocup.launch
$ roslaunch object_recognition yolo_train.launch
$ roslaunch object_recognition object_position.launch
$ roslaunch movebase_controller 
$ rosrun final_manipulation final_manipulation_node
$ rosrun final_navigation_integration final_navigation_integration_node
