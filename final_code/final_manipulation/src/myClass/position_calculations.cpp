#include<final_manipulation/final_manipulation_inc.h>
namespace final_manipulation
{
    objectinfo manipulationClass::get_realtime_pose(KDL::JntArray joint_pos, KDL::Chain chain)
    {
        KDL::ChainFkSolverPos_recursive fksolver = KDL::ChainFkSolverPos_recursive(chain);
        // compute the pose of end effector with forward kinematics
        KDL::Frame cartesian_pose;
        bool kinematics_status_forward;
        kinematics_status_forward = fksolver.JntToCart(joint_pos, cartesian_pose);
        ROS_INFO_STREAM("frame:"<<chain.getSegment(0).getName());  

        
        objectinfo ret;
        ret.x_ = cartesian_pose.p(0);
        ret.y_ = cartesian_pose.p(1);
        ret.z_ = cartesian_pose.p(2);
        
        double R,P,Y;
        cartesian_pose.M.GetRPY(R,P,Y);
        ret.rx_ = R;
        ret.ry_ = P;
        ret.rz_ = Y;
        ROS_INFO_STREAM("end effector pos !:"<<cartesian_pose.p<<R<<P<<Y);
        return ret;
    }
}