#include <final_manipulation/final_manipulation_inc.h>//include the headfile
namespace final_manipulation
{
    void manipulationClass::kdl_arm()
    {
        using namespace KDL;
        ROS_INFO_STREAM("in kdl arm");
        KDL::Tree my_tree;
        kdl_parser::treeFromFile("/home/zhang/t5_ws/src/final_manipulation/configs/tiagotest.urdf",my_tree);
        bool exit_value;
        Chain chain;
        exit_value = my_tree.getChain("torso_lift_link","arm_tool_link",chain);
        unsigned int js=chain.getNrOfSegments();
        unsigned int nj=chain.getNrOfJoints();
        ROS_INFO_STREAM("js:"<<js<<"nj:"<<nj);
        for(int i=0;i<chain.getNrOfSegments();i++)
        {
            ROS_INFO_STREAM(chain.getSegment(i).getJoint().getName());
        }
        ChainFkSolverPos_recursive fksolver = ChainFkSolverPos_recursive(chain);
        JntArray jointpositions = JntArray(nj);
        for(unsigned int i=0;i<js;i++){
            float myinput; printf("Enter the position of joint %i: ",i);
            scanf("%e",&myinput);
            jointpositions(i)=(double)myinput; 
        }
        Frame cartpos; 
        bool kinematics_status;
        kinematics_status = fksolver.JntToCart(jointpositions,cartpos);
        if(kinematics_status>=0){
            std::cout << cartpos << std::endl;
            std::cout << cartpos(0,3) << std::endl;
            printf("%s \n","Success, thanks KDL!");
        }
        else{
            printf("%s \n","Error:could not calculate forward kinematics : ");
        }
    }

    
}