#include <final_manipulation/final_manipulation_inc.h>//include the headfile

namespace final_manipulation
{

   // double manipulationClass::torque_Callback(const geometry_msgs::WrenchStamped::ConstPtr& msg)
   // {

   // }


    void manipulationClass::low_torso(double height)
    {
        ros::Publisher pub_torso_down = nh_.advertise<trajectory_msgs::JointTrajectory>("/torso_controller/command",100);
        //ros::Subscriber sub_torque = n.subscribe("/wrist_ft", 100, &manipulationClass::torque_Callback,this);
        ROS_INFO_STREAM(" torso down : ");
        //ros::Duration(3).sleep();
        ros::Rate r_torso(10);

        ros::Time t_torso=ros::Time::now();
        trajectory_msgs::JointTrajectory Torso_down;
        trajectory_msgs::JointTrajectoryPoint Torso_set;
        Torso_down.joint_names.push_back("torso_lift_joint");

        Torso_set.positions.push_back(height);

        Torso_set.time_from_start = ros::Duration(2);
        Torso_down.points.push_back(Torso_set);

        while((ros::Time::now().toSec()-t_torso.toSec())<6)
        {
            pub_torso_down.publish(Torso_down);
            //ROS_INFO_STREAM("in low torso");
            ros::spinOnce();
            r_torso.sleep();
        }
        ROS_INFO_STREAM(" low torso finished ! ");
    }

    void manipulationClass::gripper_pick(double distance)
    {
        ros::Publisher pub_gripper_pick = nh_.advertise<trajectory_msgs::JointTrajectory>("/gripper_controller/command",100);
        ROS_INFO_STREAM(" gripper pick : ");
        ros::Duration(3).sleep();
        ros::Rate r_gripper(10);

        ros::Time t_gripper=ros::Time::now();
        trajectory_msgs::JointTrajectory Gripper;
        trajectory_msgs::JointTrajectoryPoint Gripper_set;

        Gripper.joint_names.push_back("gripper_left_finger_joint");
        Gripper.joint_names.push_back( "gripper_right_finger_joint");
        Gripper_set.positions.push_back(distance);
        Gripper_set.positions.push_back(distance);

        Gripper_set.time_from_start = ros::Duration(2);
        Gripper.points.push_back(Gripper_set);

        while((ros::Time::now().toSec()-t_gripper.toSec())<7)
        {
            pub_gripper_pick.publish(Gripper);
            //ROS_INFO_STREAM("in low torso");
            ros::spinOnce();
            r_gripper.sleep();
        }
        ROS_INFO_STREAM(" Gripper pick finished ! ");
    }

    void manipulationClass::arm3_adjust()
    {
        ros::Publisher pub_gripper_pick = nh_.advertise<trajectory_msgs::JointTrajectory>("/arm_controller/command",100);
        ROS_INFO_STREAM(" arm3 move : ");
        ros::Duration(3).sleep();
        ros::Rate r_arm3(10);

        ros::Time t_gripper=ros::Time::now();
        trajectory_msgs::JointTrajectory arm3;
        trajectory_msgs::JointTrajectoryPoint arm3_set;

        arm3.joint_names.push_back( "arm_1_joint" );
        arm3.joint_names.push_back( "arm_2_joint" );
        arm3.joint_names.push_back( "arm_3_joint" );
        arm3.joint_names.push_back( "arm_4_joint" );
        arm3.joint_names.push_back( "arm_5_joint" );
        arm3.joint_names.push_back( "arm_6_joint" );
        arm3.joint_names.push_back( "arm_7_joint" );

        arm3_set.positions.push_back(0.20);
        arm3_set.positions.push_back(-1.34);
        arm3_set.positions.push_back(-0.40);
        arm3_set.positions.push_back(1.94);
        arm3_set.positions.push_back(-1.57);
        arm3_set.positions.push_back(1.37);
        arm3_set.positions.push_back(0.00);

        arm3_set.time_from_start = ros::Duration(2);
        arm3.points.push_back(arm3_set);

        while((ros::Time::now().toSec()-t_gripper.toSec())<3)
        {
            pub_gripper_pick.publish(arm3);
            //ROS_INFO_STREAM("in low torso");
            ros::spinOnce();
            r_arm3.sleep();
        }
        ROS_INFO_STREAM(" arm3 adjust finished ! ");
    }

    void manipulationClass::low_head()
    {
        ROS_INFO_STREAM("head_down");
        ros::Publisher pub_head = nh_.advertise<trajectory_msgs::JointTrajectory>("/head_controller/command",100);

        ros::Duration(3).sleep();
        ros::Rate r(10);
        ros::Time t2=ros::Time::now();
        trajectory_msgs::JointTrajectory Head_move;
        trajectory_msgs::JointTrajectoryPoint Head_set;

        Head_move.joint_names.push_back("head_1_joint");
        Head_move.joint_names.push_back( "head_2_joint");

        Head_set.positions.push_back(0.0);
        Head_set.positions.push_back(-0.3);
        Head_set.time_from_start = ros::Duration(2);
        Head_move.points.push_back(Head_set);
        while((ros::Time::now().toSec()-t2.toSec())<7)
        {

            ROS_INFO_STREAM("in low head");
            pub_head.publish(Head_move);
            ros::spinOnce();
            r.sleep();
        }
    }

    void manipulationClass::movetotarget_arm(geometry_msgs::PoseStamped targetpos)
    {
        ros::AsyncSpinner spinner_arm1(1);
        spinner_arm1.start();
        group_arm.setPlannerId("SBLkConfigDefault");
        group_arm.setPoseReferenceFrame("base_footprint");
        group_arm.setPoseTarget(targetpos);
        ROS_INFO_STREAM("Planning to move " <<
                       group_arm.getEndEffectorLink() << " to a target pose expressed in " <<
                       group_arm.getPlanningFrame());

        group_arm.setStartStateToCurrentState();
        group_arm.setMaxVelocityScalingFactor(1.0);


         // set the plan: pre_grasp
        moveit::planning_interface::MoveGroupInterface::Plan my_plan_pre_grasp1;

         // set maximum time to find a plan for pre_grasp
        group_arm.setPlanningTime(5.0);

         // set bool value to record
        bool pre_grasp_success =static_cast<bool>( group_arm.plan(my_plan_pre_grasp1));
        //bool pre_grasp_success = group_arm.plan(my_plan_pre_grasp1);
        if ( !pre_grasp_success )
        {
            ROS_INFO_STREAM("No plan found");
            spinner_arm1.stop();
        }
        else {
            ROS_INFO_STREAM(" Plan found in " << my_plan_pre_grasp1.planning_time_ << " seconds at ");

            // Execute the plan: pre_grasp
            ros::Time pre_grasp_start = ros::Time::now();
            ROS_INFO_STREAM(my_plan_pre_grasp1.trajectory_.joint_trajectory);
            trajectory_msgs::JointTrajectory test_tra = my_plan_pre_grasp1.trajectory_.joint_trajectory;
            for(int i=0;i<test_tra.joint_names.size();i++){
                std::cout<<"joint:"<<test_tra.joint_names[i]<<std::endl;
                for(int j=0;j<test_tra.points[i].positions.size();j++)
                {
                    std::cout<<"position:"<<test_tra.points[i].positions[j]<<"vel:"<<test_tra.points[i].velocities[j]<<"accel:"<<test_tra.points[i].accelerations[j]<<std::endl;
                }
            }

            group_arm.move();
            ROS_INFO_STREAM("Motion_pre_grasp duration: " << (ros::Time::now() - pre_grasp_start).toSec());
            spinner_arm1.stop();
        }
    }

    void manipulationClass::movetotarget_armtorso(geometry_msgs::PoseStamped targetpos)
    {
        ros::AsyncSpinner spinner_armtorso(1);
        spinner_armtorso.start();
        group_arm_torso.setPlannerId("SBLkConfigDefault");

        group_arm_torso.setPoseReferenceFrame("base_footprint");
        group_arm_torso.setPoseTarget(targetpos);
        ROS_INFO_STREAM("Planning to move " <<
                       group_arm_torso.getEndEffectorLink() << " to a target pose expressed in " <<
                       group_arm_torso.getPlanningFrame());

        group_arm_torso.setStartStateToCurrentState();
        group_arm_torso.setMaxVelocityScalingFactor(1.0);
        ROS_INFO_STREAM("after setting");

         // set the plan: pre_grasp
        moveit::planning_interface::MoveGroupInterface::Plan my_plan_pre_grasp;
         // set maximum time to find a plan for pre_grasp
        group_arm_torso.setPlanningTime(5.0);
        ROS_INFO_STREAM("after set plan");
         // set bool value to record
        //bool pre_grasp_success =static_cast<bool>( group_arm_torso.plan(my_plan_pre_grasp));
        bool pre_grasp_success = (group_arm_torso.plan( my_plan_pre_grasp) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
        //bool pre_grasp_success = true;
        ROS_INFO_STREAM("after bool pre grasp success");
        ROS_INFO_STREAM("PRE_GRESP BOOL:"<<pre_grasp_success);
        if ( !pre_grasp_success )
        {
            // throw std::runtime_error("No plan found");
            ROS_INFO_STREAM("No plan found");
            spinner_armtorso.stop();
        }
        else {
            //ROS_INFO_STREAM("found plan");
            ROS_INFO_STREAM(" Plan found in " << my_plan_pre_grasp.planning_time_ << " seconds at ");
            trajectory_msgs::JointTrajectory test_tra = my_plan_pre_grasp.trajectory_.joint_trajectory;
            // for(int i=0;i<test_tra.joint_names.size();i++){
            //     // std::cout<<"joint:"<<test_tra.joint_names[i]<<std::endl;
            //     // for(int j=0;j<test_tra.points[i].positions.size();j++)
            //     // {
            //     //     std::cout<<"position:"<<test_tra.points[i].positions[j]<<"vel:"<<test_tra.points[i].velocities[j]<<"accel:"<<test_tra.points[i].accelerations[j]<<std::endl;
            //     // }
            //     ROS_INFO_STREAM("joints:"<<test_tra.joint_names[i]);
            //     ROS_INFO_STREAM("points"<<test_tra.points[i]);
            // }
            display_trajectory.trajectory_start = my_plan_pre_grasp.start_state_;
            display_trajectory.trajectory.push_back(my_plan_pre_grasp.trajectory_);
            display_publisher.publish(display_trajectory);
            int judge_num;
            ROS_INFO_STREAM("If the the trajectory safe, press 1, else press 0:");
            std::cin>>judge_num;
            if(judge_num==1)
            {
                //sros::Duration(5).sleep();
                // Execute the plan: pre_grasp
                ros::Time pre_grasp_start = ros::Time::now();
                group_arm_torso.move();
                ROS_INFO_STREAM("Motion_pre_grasp duration: " << (ros::Time::now() - pre_grasp_start).toSec());
                spinner_armtorso.stop();
            }
            else
            {

                ROS_INFO_STREAM("motion cancled");
                spinner_armtorso.stop();

            }

        }
    }


    void manipulationClass::moveto_randomtarget()
    {
        ros::AsyncSpinner spinner(1);
        spinner.start();
        group_arm_torso.setRandomTarget();
        group_arm_torso.move();
        spinner.stop();
    }



    void manipulationClass::getRobotModel(const geometry_msgs::PoseStamped& targetpos)
    {
        ros::AsyncSpinner spinner_getRobotModel(1);
        spinner_getRobotModel.start();
        robot_model_loader::RobotModelLoader robot_model_loader("robot_description");
        robot_model::RobotModelPtr kinematic_model = robot_model_loader.getModel();
        ROS_INFO("Model frame: %s", kinematic_model->getModelFrame().c_str());
        robot_state::RobotStatePtr kinematic_state(new robot_state::RobotState(kinematic_model));
        kinematic_state->setToDefaultValues();
        const robot_state::JointModelGroup* joint_model_group = kinematic_model->getJointModelGroup("arm");

        const std::vector<std::string>& joint_names = joint_model_group->getVariableNames();

        // Get Joint Values
        // ^^^^^^^^^^^^^^^^
        // We can retreive the current set of joint values stored in the state for the Panda arm.
        std::vector<double> joint_values;
        kinematic_state->copyJointGroupPositions(joint_model_group, joint_values);
        for (std::size_t i = 0; i < joint_names.size(); ++i)
        {
            ROS_INFO("Joint %s: %f", joint_names[i].c_str(), joint_values[i]);
        }

        //joint_values[0] = 5.57;
        //kinematic_state->setJointGroupPositions(joint_model_group, joint_values);

        /* Check whether any joint is outside its joint limits */
        // ROS_INFO_STREAM("Current state is " << (kinematic_state->satisfiesBounds() ? "valid" : "not valid"));

        /* Enforce the joint limits for this state and check again*/
        // kinematic_state->enforceBounds();
        // ROS_INFO_STREAM("Current state is " << (kinematic_state->satisfiesBounds() ? "valid" : "not valid"));

        //kinematic_state->setToRandomPositions(joint_model_group);

         geometry_msgs::Pose target = targetpos.pose;
        // target.position.x= 0.4;
        // target.position.y= -0.3;
        // target.position.z= 0.26;
        // target.orientation=tf::createQuaternionMsgFromRollPitchYaw(-0.011,1.57,0.037);

        //kinematic_state->setJointGroupPositions(joint_model_group,reference_point_position);
        //const Eigen::Affine3d& end_effector_state = kinematic_state->getGlobalLinkTransform("arm_7_link");
        /* Print end-effector pose. Remember that this is in the model frame */
        // const geometry_msgs::Pose end_effector_state = targetpos;
        ROS_INFO_STREAM("\n \n                     hello xizi~                   \n" );
        // ROS_INFO_STREAM("Rotation: \n" << end_effector_state.rotation() << "\n");

        std::size_t attempts = 10;
        double timeout = 0.1;
        bool found_ik = kinematic_state->setFromIK(joint_model_group, target, attempts, timeout);
            // Now, we can print out the IK solution (if found):
        if (found_ik)
        {
            kinematic_state->copyJointGroupPositions(joint_model_group, joint_values);
            for (std::size_t i = 0; i < joint_names.size(); ++i)
            {
                ROS_INFO("Joint %s: %f", joint_names[i].c_str(), joint_values[i]);
            }
        }
        else
        {
            ROS_INFO("Did not find IK solution");
        }


        spinner_getRobotModel.stop();

    }

    void manipulationClass::head_move_around()
    {
        ROS_INFO_STREAM("head look around");
        ros::Publisher pub_head_look_around = nh_.advertise<trajectory_msgs::JointTrajectory>("/head_controller/command",100);

        ros::Duration(3).sleep();
        ros::Rate r(10);
        ros::Time t2=ros::Time::now();
        trajectory_msgs::JointTrajectory Head_move;
        trajectory_msgs::JointTrajectoryPoint Head_set_tmp;
        trajectory_msgs::JointTrajectoryPoint head1_trajectory;
        trajectory_msgs::JointTrajectoryPoint head2_trajectory,head8_trajectory,head9_trajectory;
        trajectory_msgs::JointTrajectoryPoint head3_trajectory,head4_trajectory,head5_trajectory,head6_trajectory,head7_trajectory;

        XmlRpc::XmlRpcValue param_list;
        if(!nh_.getParam("/param1/play_motion/motions/head_look_around/points", param_list))
        {
            ROS_ERROR("Failed to get parameter from server.");
        }

        double tmpnum;
        for(int i=0;i<param_list.size();i++)
        {
            std::vector<double> tmpvec;
            XmlRpc::XmlRpcValue &name = param_list[i];
            for(int j=0;j<name["positions"].size();j++)
            {
                if(name["positions"][j].getType()!=XmlRpc::XmlRpcValue::TypeDouble)
                {
                    ROS_INFO_STREAM("name positions"<<name["positions"]);
                    tmpnum=static_cast<double>(static_cast<int>(name["positions"][j]));
                    tmpvec.push_back(tmpnum);
                }
                else
                {
                    tmpnum=static_cast<double>(static_cast<double>(name["positions"][j]));
                    tmpvec.push_back(tmpnum);
                }
                ROS_INFO_STREAM("tmpnum:"<<tmpnum);
            }
            ROS_INFO_STREAM("tmpvec:"<<tmpvec[0]<<tmpvec[1]);
            Head_set_tmp.positions=tmpvec;
            Head_set_tmp.time_from_start=ros::Duration(static_cast<double>(param_list[i]["time_from_start"]));
            Head_move.points.push_back(Head_set_tmp);
        }
        if(!nh_.getParam("/param1/play_motion/motions/head_look_around/joints", param_list))
        {
            ROS_ERROR("Failed to get parameter from server.");
        }
        else
        {
            Head_move.joint_names={param_list[0],param_list[1]};
            ROS_INFO_STREAM("joints:"<<Head_move.joint_names[0]<<Head_move.joint_names[1]);
            ROS_INFO_STREAM("value"<<Head_move.points.size());
        }
        ROS_INFO_STREAM("in low head");
        for(int i=0;i<Head_move.points.size();i++)
        {
            ROS_INFO_STREAM("points:"<<Head_move.points[i].positions[0]<<Head_move.points[i].positions[1]);
        }
        pub_head_look_around.publish(Head_move);
        ROS_INFO_STREAM("after publish");
        ros::Duration(30).sleep();
    }




    bool manipulationClass::headaround(std_srvs::Empty::Request &req,std_srvs::Empty::Response &res)
    {
        head_move_around();
        return true;
    }

    bool manipulationClass::pick1(flowerfanggirl::Targetpoint::Request &req,flowerfanggirl::Targetpoint::Response &res)
    {
        geometry_msgs::PoseStamped target_tmp;
        target_tmp.header.frame_id="base_link";
        target_tmp.pose = req.pose;
        movetotarget_armtorso(target_tmp);
        res.finish=true;
        return true;
    }

    bool manipulationClass::pickRPY1(flowerfanggirl::Targetpoint1::Request &req,flowerfanggirl::Targetpoint1::Response &res)
    {
        geometry_msgs::PoseStamped target_tmp;
        target_tmp.header.frame_id="base_link";
        target_tmp.pose.position=req.pos;
        target_tmp.pose.orientation=tf::createQuaternionMsgFromRollPitchYaw(req.Roll,req.Pitch,req.Yaw);

        movetotarget_armtorso(target_tmp);
        res.finish=true;
        return true;
    }

//     void manipulationClass::getRobotModel_trajectary_replay()
//     {
//         ros::AsyncSpinner spinner_modelreplay(1);
//         spinner_modelreplay.start();

//         const std::string PLANNING_GROUP = "arm_torso";//define the control group
//         robot_model_loader::RobotModelLoaderPtr robot_model_loader(
//         new robot_model_loader::RobotModelLoader("robot_description"));
//         robot_model::RobotModelPtr robot_model = robot_model_loader->getModel();
//         /* Create a RobotState and JointModelGroup to keep track of the current robot pose and planning group*/
//         robot_state::RobotStatePtr robot_state(new robot_state::RobotState(robot_model));
//         const robot_state::JointModelGroup* joint_model_group = robot_state->getJointModelGroup(PLANNING_GROUP);

//         planning_scene::PlanningScenePtr planning_scene(new planning_scene::PlanningScene(robot_model));
//         planning_scene_monitor::PlanningSceneMonitorPtr psm(new planning_scene_monitor::PlanningSceneMonitor(planning_scene, robot_model_loader));
//         psm->startPublishingPlanningScene(planning_scene_monitor::PlanningSceneMonitor::UPDATE_SCENE);
//         psm->startStateMonitor();
//         psm->startSceneMonitor();
//         while (!psm->getStateMonitor()->haveCompleteState() && ros::ok())
//         {
//             ROS_INFO_STREAM_THROTTLE_NAMED(1, "final_integration", "Waiting for complete state from topic ");
//         }

//         boost::scoped_ptr<pluginlib::ClassLoader<planning_interface::PlannerManager>> planner_plugin_loader;
//         planning_interface::PlannerManagerPtr planner_instance;
//         std::string planner_plugin_name;

//         if (!nh_.getParam("planning_plugin", planner_plugin_name))
//             ROS_FATAL_STREAM("Could not find planner plugin name");
//         try
//         {
//             planner_plugin_loader.reset(new pluginlib::ClassLoader<planning_interface::PlannerManager>(
//                 "moveit_core", "planning_interface::PlannerManager"));
//         }
//         catch (pluginlib::PluginlibException& ex)
//         {
//             ROS_FATAL_STREAM("Exception while creating planning plugin loader " << ex.what());
//         }
//         try
//         {
//             planner_instance.reset(planner_plugin_loader->createUnmanagedInstance(planner_plugin_name));
//             if (!planner_instance->initialize(robot_model, nh_.getNamespace()))
//             ROS_FATAL_STREAM("Could not initialize planner instance");
//             ROS_INFO_STREAM("Using planning interface '" << planner_instance->getDescription() << "'");
//         }
//         catch (pluginlib::PluginlibException& ex)
//         {
//             const std::vector<std::string>& classes = planner_plugin_loader->getDeclaredClasses();
//             std::stringstream ss;
//             for (std::size_t i = 0; i < classes.size(); ++i)
//             ss << classes[i] << " ";
//             ROS_ERROR_STREAM("Exception while loading planner '" << planner_plugin_name << "': " << ex.what() << std::endl
//                                                                 << "Available plugins: " << ss.str());
//         }

//         namespace rvt = rviz_visual_tools;
//         moveit_visual_tools::MoveItVisualTools visual_tools("arm_torso", rviz_visual_tools::RVIZ_MARKER_TOPIC, psm);
//         visual_tools.loadRobotStatePub("/display_robot_state");
//         visual_tools.enableBatchPublishing();
//         visual_tools.deleteAllMarkers();  // clear all old markers
//         visual_tools.trigger();

//         visual_tools.loadRemoteControl();


//         Eigen::Isometry3d text_pose = Eigen::Isometry3d::Identity();
//         text_pose.translation().z() = 1.75;
//         visual_tools.publishText(text_pose, "Motion Planning API Demo", rvt::WHITE, rvt::XLARGE);
//         visual_tools.trigger();

//   /* We can also use visual_tools to wait for user input */
//         visual_tools.prompt("Press 'next' in the RvizVisualToolsGui window to start the demo");

//         visual_tools.publishRobotState(planning_scene->getCurrentStateNonConst(), rviz_visual_tools::GREEN);
//         visual_tools.trigger();
//         planning_interface::MotionPlanRequest req;
//         planning_interface::MotionPlanResponse res;
//         geometry_msgs::PoseStamped pose;
//         pose.header.frame_id = "base_link";
//         pose.pose.position.x = 0.3;
//         pose.pose.position.y = 0.3;
//         pose.pose.position.z = 0.3;
//         //pose.pose.orientation.w = 1.0;
//         pose.pose.orientation=tf::createQuaternionMsgFromRollPitchYaw(1.57,0,0);


//         std::vector<double> tolerance_pose(3, 0.01);
//         std::vector<double> tolerance_angle(3, 0.01);

//         moveit_msgs::Constraints pose_goal =
//         kinematic_constraints::constructGoalConstraints("arm_1_joint", pose, tolerance_pose, tolerance_angle);

//         req.group_name = PLANNING_GROUP;
//         req.goal_constraints.push_back(pose_goal);

//         planning_interface::PlanningContextPtr context =
//             planner_instance->getPlanningContext(planning_scene, req, res.error_code_);
//         context->solve(res);
//         if (res.error_code_.val != res.error_code_.SUCCESS)
//         {
//             ROS_ERROR("Could not compute plan successfully");
//         }

//         ros::Publisher display_publisher =
//             nh_.advertise<moveit_msgs::DisplayTrajectory>("/move_group/display_planned_path", 1, true);
//         moveit_msgs::DisplayTrajectory display_trajectory;

//         moveit_msgs::MotionPlanResponse response;
//         res.getMessage(response);

//         display_trajectory.trajectory_start = response.trajectory_start;
//         display_trajectory.trajectory.push_back(response.trajectory);
//         visual_tools.publishTrajectoryLine(display_trajectory.trajectory.back(), joint_model_group);
//         visual_tools.trigger();
//         display_publisher.publish(display_trajectory);

//         robot_state->setJointGroupPositions(joint_model_group, response.trajectory.joint_trajectory.points.back().positions);
//         planning_scene->setCurrentState(*robot_state.get());

//         visual_tools.publishRobotState(planning_scene->getCurrentStateNonConst(), rviz_visual_tools::GREEN);
//         visual_tools.publishAxisLabeled(pose.pose, "goal_1");
//         visual_tools.publishText(text_pose, "Pose Goal (1)", rvt::WHITE, rvt::XLARGE);
//         visual_tools.trigger();

//         visual_tools.prompt("Press 'next' in the RvizVisualToolsGui window to continue the demo");

//         robot_state::RobotState goal_state(robot_model);
//         std::vector<double> joint_values = { -1.0, 0.7, 0.7, -1.5, -0.7, 2.0, 0.0 };
//         goal_state.setJointGroupPositions(joint_model_group, joint_values);
//         moveit_msgs::Constraints joint_goal = kinematic_constraints::constructGoalConstraints(goal_state, joint_model_group);
//         req.goal_constraints.clear();
//         req.goal_constraints.push_back(joint_goal);


//         context = planner_instance->getPlanningContext(planning_scene, req, res.error_code_);
//         context->solve(res);
//         if (res.error_code_.val != res.error_code_.SUCCESS)
//         {
//             ROS_ERROR("Could not compute plan successfully");
//         }
//         res.getMessage(response);
//         display_trajectory.trajectory.push_back(response.trajectory);

//         visual_tools.publishTrajectoryLine(display_trajectory.trajectory.back(), joint_model_group);
//         visual_tools.trigger();
//         display_publisher.publish(display_trajectory);

//         /* We will add more goals. But first, set the state in the planning
//             scene to the final state of the last plan */
//         robot_state->setJointGroupPositions(joint_model_group, response.trajectory.joint_trajectory.points.back().positions);
//         planning_scene->setCurrentState(*robot_state.get());

//         // Display the goal state
//         visual_tools.publishRobotState(planning_scene->getCurrentStateNonConst(), rviz_visual_tools::GREEN);
//         visual_tools.publishAxisLabeled(pose.pose, "goal_2");
//         visual_tools.publishText(text_pose, "Joint Space Goal (2)", rvt::WHITE, rvt::XLARGE);
//         visual_tools.trigger();

//         /* Wait for user input */
//         visual_tools.prompt("Press 'next' in the RvizVisualToolsGui window to continue the demo");

//         /* Now, we go back to the first goal to prepare for orientation constrained planning */
//         req.goal_constraints.clear();
//         req.goal_constraints.push_back(pose_goal);
//         context = planner_instance->getPlanningContext(planning_scene, req, res.error_code_);
//         context->solve(res);
//         res.getMessage(response);

//         display_trajectory.trajectory.push_back(response.trajectory);
//         visual_tools.publishTrajectoryLine(display_trajectory.trajectory.back(), joint_model_group);
//         visual_tools.trigger();
//         display_publisher.publish(display_trajectory);

//         /* Set the state in the planning scene to the final state of the last plan */
//         robot_state->setJointGroupPositions(joint_model_group, response.trajectory.joint_trajectory.points.back().positions);
//         planning_scene->setCurrentState(*robot_state.get());

//         // Display the goal state
//         visual_tools.publishRobotState(planning_scene->getCurrentStateNonConst(), rviz_visual_tools::GREEN);
//         visual_tools.trigger();

//         /* Wait for user input */
//         visual_tools.prompt("Press 'next' in the RvizVisualToolsGui window to continue the demo");



//         pose.pose.position.x = 0.32;
//         pose.pose.position.y = -0.25;
//         pose.pose.position.z = 0.65;
//         pose.pose.orientation.w = 1.0;
//         moveit_msgs::Constraints pose_goal_2 =
//             kinematic_constraints::constructGoalConstraints("panda_link8", pose, tolerance_pose, tolerance_angle);

//         /* Now, let's try to move to this new pose goal*/
//         req.goal_constraints.clear();
//         req.goal_constraints.push_back(pose_goal_2);

//         /* But, let's impose a path constraint on the motion.
//             Here, we are asking for the end-effector to stay level*/
//         geometry_msgs::QuaternionStamped quaternion;
//         quaternion.header.frame_id = "arm_1_joint";
//         quaternion.quaternion.w = 1.0;
//         req.path_constraints = kinematic_constraints::constructGoalConstraints("arm_1_joint", quaternion);


//         req.workspace_parameters.min_corner.x = req.workspace_parameters.min_corner.y =
//             req.workspace_parameters.min_corner.z = -2.0;
//         req.workspace_parameters.max_corner.x = req.workspace_parameters.max_corner.y =
//             req.workspace_parameters.max_corner.z = 2.0;

//         // Call the planner and visualize all the plans created so far.
//         context = planner_instance->getPlanningContext(planning_scene, req, res.error_code_);
//         context->solve(res);
//         res.getMessage(response);
//         display_trajectory.trajectory.push_back(response.trajectory);
//         visual_tools.publishTrajectoryLine(display_trajectory.trajectory.back(), joint_model_group);
//         visual_tools.trigger();
//         display_publisher.publish(display_trajectory);

//         /* Set the state in the planning scene to the final state of the last plan */
//         robot_state->setJointGroupPositions(joint_model_group, response.trajectory.joint_trajectory.points.back().positions);
//         planning_scene->setCurrentState(*robot_state.get());

//         // Display the goal state
//         visual_tools.publishRobotState(planning_scene->getCurrentStateNonConst(), rviz_visual_tools::GREEN);
//         visual_tools.publishAxisLabeled(pose.pose, "goal_3");
//         visual_tools.publishText(text_pose, "Orientation Constrained Motion Plan (3)", rvt::WHITE, rvt::XLARGE);
//         visual_tools.trigger();

//         // END_TUTORIAL
//         /* Wait for user input */
//         visual_tools.prompt("Press 'next' in the RvizVisualToolsGui window to exit the demo");
//         planner_instance.reset();

//     }



    void manipulationClass::kdl_test()
    {
        //Definition of a kinematic chain & add segments to the chain
        KDL::Chain chain;
        KDL::Joint joint1;
        KDL::Segment segment1;
        urdf::Model testmodel;
        KDL::Tree my_tree;
        if (!kdl_parser::treeFromFile("/home/zhang/tiagotest.urdf", my_tree))
        {
            ROS_ERROR("Failed to construct kdl tree");

        }
        else
        {
            ROS_INFO_STREAM("successfully load the robot model");
            unsigned int js=my_tree.getNrOfSegments();
            ROS_INFO_STREAM("my tree "<<js);

        }

        bool exit_value;
        exit_value = my_tree.getChain("base_link","arm_7_link",chain);
        ROS_INFO_STREAM("num of joints"<<chain.getNrOfJoints());
        ROS_INFO_STREAM("num of seg:"<<chain.getNrOfSegments());
        KDL::ChainFkSolverPos_recursive fksolver = KDL::ChainFkSolverPos_recursive(chain);
        unsigned int nj = chain.getNrOfJoints();
        KDL::JntArray jointpositions = KDL::JntArray(nj);
        for(unsigned int i=0;i<nj;i++)
        {
            float myinput;
            ROS_INFO_STREAM("Enter the position of joint %i: "<<i<<"with name "<<chain.getSegment(i+1).getJoint().getName());
            //scanf("%e",&myinput);
            std::cin>>myinput;
            jointpositions(i)=(double)myinput;

        }
        KDL::Frame cartpos;
        bool kinematics_status;
        kinematics_status = fksolver.JntToCart(jointpositions,cartpos);
        if(kinematics_status>=0)
        {
            ROS_INFO_STREAM("sucess calculated position in cartesian space");
            std::cout << cartpos << std::endl;

        }
        else
        {
            printf("%s \n","Error:could not calculate forward kinematics : ");
        }
    }

    void manipulationClass::kdl_ik_c2j()
    {
        KDL::Chain chain;
        KDL::Joint joint1;
        KDL::Segment segment1;
        urdf::Model testmodel;
        KDL::Tree my_tree;
        if (!kdl_parser::treeFromFile("/home/zhang/tiagotest.urdf", my_tree))
        {
            ROS_ERROR("Failed to construct kdl tree");

        }
        else
        {
            ROS_INFO_STREAM("successfully load the robot model");


        }
        bool exit_value;
        exit_value = my_tree.getChain("base_link","arm_7_link",chain);
        unsigned int js=chain.getNrOfSegments();
        unsigned int nj=chain.getNrOfJoints();
        ROS_INFO_STREAM("my tree has "<<js<<" segments, "<<nj<<" joints!");
        for(int i=0;i<js;i++)
        {
            ROS_INFO_STREAM(chain.getSegment(i).getJoint().getName());
        }

        double eps=1E-5;
        int maxiter=500;
        double eps_joints=1E-15;
        KDL::ChainIkSolverPos_LMA iksolver = KDL::ChainIkSolverPos_LMA(chain,eps,maxiter,eps_joints);


        KDL::JntArray jointGuesspositions = KDL::JntArray(nj);
        for(unsigned int i=0;i<nj;i++)
        {
            float myinput;
            ROS_INFO_STREAM("Please input the initial guess position of joint "<<chain.getSegment(i).getJoint().getName());
            std::cin>>myinput;

        }

        float x,y,z,roll,pitch,yaw;
        ROS_INFO_STREAM("please input six number represent x,y,z,roll,pitch,yaw:");
        std::cin>>x;
        std::cin>>y;
        std::cin>>z;
        std::cin>>roll;
        std::cin>>pitch;
        std::cin>>yaw;

        ROS_INFO_STREAM("x"<<x);
        ROS_INFO_STREAM("y"<<y);
        ROS_INFO_STREAM("z"<<z);
        ROS_INFO_STREAM("roll"<<roll);
        ROS_INFO_STREAM("pitch"<<pitch);
        ROS_INFO_STREAM("yaw"<<yaw);

        KDL::Vector vector=KDL::Vector(x,y,z);
        float cy=std::cos(yaw);
        float sy=std::sin(yaw);
        float cp=std::cos(pitch);
        float sp=std::sin(pitch);
        float cr=std::cos(roll);
        float sr=std::sin(roll);

        double rot0=cy*cp;
        double rot1=cy*sp*sr-sy*cr;
        double rot2=cy*sp*cr+sy*sr;
        double rot3=sy*cp;
        double rot4=sy*sp*sr+cy*cr;
        double rot5=sy*sp*cr-cy*sr;
        double rot6=-sp;
        double rot7=cp*sr;
        double rot8=cp*cr;

        KDL::Rotation rot=KDL::Rotation(rot0,rot1,rot2,rot3,rot4,rot5,rot6,rot7,rot8);
        KDL::Rotation rotRPY=KDL::Rotation::RPY(roll,pitch,yaw);
        ROS_INFO_STREAM("rot:"<<rot);
        ROS_INFO_STREAM("rotRPY:"<<rotRPY);

        KDL::Frame cartpos=KDL::Frame(rot,vector);
        ROS_INFO_STREAM("cartpos:"<<cartpos);
        KDL::JntArray jointpositions=KDL::JntArray(nj);
        bool kinematics_status;
        kinematics_status = iksolver.CartToJnt(jointGuesspositions,cartpos,jointpositions);
        if(kinematics_status>=0)
        {
            for(int i=0;i<nj;i++)
            {
                ROS_INFO_STREAM("joint "<<chain.getSegment(i).getJoint().getName()<<" with position"<<jointpositions(i));
            }
            ROS_INFO("Success, thanks KDL!");
        }
        else
        {
            ROS_ERROR("could not calculate backwart kinematics!");
        }
    }

    void manipulationClass::speak(std::string text)
{


  ros::Publisher text_pub = nh_.advertise<pal_interaction_msgs::TtsActionGoal>("/tts/goal",100);
  ROS_INFO_STREAM(" text to speech : ");
  ros::Rate r_text(10);
  ros::Time t_torso=ros::Time::now();

  pal_interaction_msgs::TtsActionGoal Text;
  Text.goal.rawtext.text = text;
  Text.goal.rawtext.lang_id.push_back('en_GB');

 // while((ros::Time::now().toSec()-t_torso.toSec())<3)
 // {
      text_pub.publish(Text);
    //  ros::spinOnce();
    //  r_text.sleep();
  //}
  ROS_INFO_STREAM(" text to speech finished ! ");
  ROS_INFO_STREAM(" raw_text is: "<<Text.goal.rawtext.text);

}


}
