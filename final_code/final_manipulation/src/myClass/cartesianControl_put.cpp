#include <final_manipulation/final_manipulation_inc.h>//include the headfile
namespace final_manipulation
{
    bool manipulationClass::cartesian_control_put_Cb(flowerfanggirl::PoseStamped::Request &req,flowerfanggirl::PoseStamped::Response &res)
    {
        ROS_INFO_STREAM("in cartesian control put  callback");
        service_count++;
        low_torso(0.14);
        objectinfo current_pose_initial,current_pose_old,current_pose,target_pose,target_pose_rotate;
        double rotate_angle = 1.5708;
        rotate_angle = rotate_angle - service_count*0.3927;
 //----- save the initial position -----//


        for (int j = 0; j < nj; j++)
        {
            initialjointpositions(j) = joint_states[j];
        }

        current_pose_initial = get_realtime_pose(initialjointpositions,chain);
        gripper_pick(0.02);
        arm3_adjust();

 //----- save the position after arm3_adjust() -----//

        for (int j = 0; j < nj; j++)
        {
            initialjointpositions(j) = joint_states[j];
        }

        current_pose_old = get_realtime_pose(initialjointpositions,chain);

 //----- compute the gesture for target_new -----//

        objectinfo target_new;
        Eigen::Matrix<double, 7, 1> joint_states_new;
        joint_states_new[0] = 0.20;
        joint_states_new[1] = 0.24;
        joint_states_new[2] = -1.6;
        joint_states_new[3] = 1.94;
        joint_states_new[4] = -1.57;
        joint_states_new[5] = 1.37;
        joint_states_new[6] = 0.0;
         for (int j = 0; j < nj; j++)
        {
            initialjointpositions(j) = joint_states_new[j];
        }

        ROS_INFO(" gesture change finished~ ");
        target_new = get_realtime_pose( initialjointpositions,chain);
        ROS_INFO_STREAM(" new target pose in torso lift link:x: "<<target_new.rx_<<" y: "<<target_new.ry_<< " z: "<<target_new.rz_);
        target_new.rz_ = 1.57; target_new.ry_ = 0.0; 

 //----- change the gesture -----//

        desired_joint test_put = calculate_joint(  current_pose_old, target_new, num_subpath);
        control_msgs::FollowJointTrajectoryGoal arm_goal_put;
        cub_waypoints_arm_goal(arm_goal_put, test_put.desired_joint_pose , num_subpath);

        // Sends the command to start the given trajectory 1s from now
        arm_goal_put.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.2);
        //ROS_INFO_STREAM(" here we adjust the gestures: " << arm_goal_put);
        ArmClient->sendGoal(arm_goal_put);
        ROS_INFO(" adjust movements finished~ ");
         while(!(ArmClient->getState().isDone()) && ros::ok())
        {
        ros::Duration(1).sleep(); // sleep for four seconds
        ROS_INFO("SUCCESS!");}

 //----- rotate and adjust the torso position -----//

         low_torso(0.03);
         rotate_movebase(1.57);
         low_torso(0.30);

 //----- compute the gesture to put the object -----//

        for (int j = 0; j < nj; j++)
        {
            initialjointpositions(j) = joint_states[j];
        }
        current_pose = get_realtime_pose(initialjointpositions,chain);
        geometry_msgs::PoseStamped target_pose_stamped;
        target_pose_stamped.header.frame_id="base_link";
        target_pose_stamped.pose.position=req.position;
        //target_pose_stamped.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(req.RPY.x,req.RPY.y,req.RPY.z);
        target_pose_stamped.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(1.57,0,0);
        target_pose = tf_transport("torso_lift_link",target_pose_stamped);

 //----- change the gesture -----//

        desired_joint test = calculate_joint(  current_pose ,  target_pose, num_subpath);
        control_msgs::FollowJointTrajectoryGoal arm_goal;
        cub_waypoints_arm_goal(arm_goal, test.desired_joint_pose , num_subpath);
        arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.2);

        ArmClient->sendGoal(arm_goal);

        // Wait for trajectory execution
        while(!(ArmClient->getState().isDone()) && ros::ok())
        {
            ros::Duration(1).sleep(); // sleep for four seconds
            ROS_INFO("SUCCESS!");}

        target_pose_stamped.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(1.57,0, rotate_angle);
        target_pose_rotate = tf_transport("torso_lift_link",target_pose_stamped);
        desired_joint rotate = calculate_joint(   target_pose,target_pose_rotate, num_subpath);
        control_msgs::FollowJointTrajectoryGoal arm_goal_rotate;
        cub_waypoints_arm_goal(arm_goal_rotate, rotate.desired_joint_pose , num_subpath);
        arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.2);

        ArmClient->sendGoal(arm_goal_rotate);

        // Wait for trajectory execution
        while(!(ArmClient->getState().isDone()) && ros::ok())
        {
            ros::Duration(1).sleep(); // sleep for four seconds
            ROS_INFO("SUCCESS!");}

 //----- put the object on the kitchen -----//
        gripper_pick(0.04);

 //----- gesture reset -----//

        control_msgs::FollowJointTrajectoryGoal arm_goal_back;
        desired_joint test_back= calculate_joint( target_pose_rotate, current_pose , num_subpath);
        cub_waypoints_arm_goal(arm_goal_back, test_back.desired_joint_pose , num_subpath);

        arm_goal_back.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.2);
        //ROS_INFO_STREAM(" this is the initial joint_positions " << arm_goal_back);
        ArmClient->sendGoal(arm_goal_back);
        ROS_INFO(" go back to the initial positions ");

        while(!(ArmClient->getState().isDone()) && ros::ok())
        {
            ros::Duration(1).sleep(); // sleep for four seconds
                ROS_INFO("SUCCESS!");}

 //----- rotate and adjust the torso position -----//

        low_torso(0.03);
        rotate_movebase(-1.57);
        low_torso(0.14);

 //----- gesture reset -----//

        objectinfo target_reset;
        for (int j = 0; j < nj; j++)
        {
            initialjointpositions(j) = joint_states[j];
        }
        target_reset = get_realtime_pose(initialjointpositions,chain);
        desired_joint test_put_back = calculate_joint(   target_reset,current_pose_initial , num_subpath);
        control_msgs::FollowJointTrajectoryGoal arm_goal_put_back;
        cub_waypoints_arm_goal(arm_goal_put_back, test_put_back.desired_joint_pose , num_subpath);

        // Sends the command to start the given trajectory 1s from now
        arm_goal_put_back.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.2);
        //ROS_INFO_STREAM(" here we adjust the gestures: " << arm_goal_put_back);
        ArmClient->sendGoal(arm_goal_put_back);
        ROS_INFO(" adjust movements finished~ ");
         while(!(ArmClient->getState().isDone()) && ros::ok())
        {
        ros::Duration(1).sleep(); // sleep for four seconds
        ROS_INFO("SUCCESS!");}

        arm3_adjust();

        if (service_count == 6)
        { service_count = 0; }
        ROS_INFO_STREAM(" pick movements finished~ "<< service_count);




        
        return true;
    }
}

