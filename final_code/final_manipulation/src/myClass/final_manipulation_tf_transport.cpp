#include <final_manipulation/final_manipulation_inc.h>//include the headfile
namespace final_manipulation
{
    objectinfo manipulationClass::tf_transport(std::string desired_frame, geometry_msgs::PoseStamped pt_in )
    {
        objectinfo pt_out1;
       
        geometry_msgs::PoseStamped pt_out;
        //pt_in.header.frame_id = "base_link";

        listener_.transformPose(desired_frame,pt_in,pt_out);
        pt_out1.x_=pt_out.pose.position.x;
        pt_out1.y_=pt_out.pose.position.y;
        pt_out1.z_=pt_out.pose.position.z;
        double R,P,Y;
        tf::Quaternion tmp(pt_out.pose.orientation.x,pt_out.pose.orientation.y,pt_out.pose.orientation.z,pt_out.pose.orientation.w);
        // tmp.setX(pt_out.pose.orientation.x);
        // tmp.setY(pt_out.pose.orientation.y);
        // tmp.setZ(pt_out.pose.orientation.z);
        // tmp.setW(pt_out.pose.orientation.w);
        tf::Matrix3x3 m(tmp);
        m.getRPY(R,P,Y);
        //tf::Matrix3x3(tmp).getRPY(R,P,Y);
        pt_out1.rx_ = R;
        pt_out1.ry_ = P;
        pt_out1.rz_ = Y;
  
        return pt_out1;
    }
    objectinfo manipulationClass::tf_transport1(std::string desired_frame, objectinfo pt_in, std::string current_frame)
    {

        geometry_msgs::PoseStamped p_in;
        p_in.header.frame_id = current_frame;
        p_in.pose.position.x = pt_in.x_;
        p_in.pose.position.y = pt_in.y_;
        p_in.pose.position.z = pt_in.z_;
        p_in.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(pt_in.rx_,pt_in.ry_,pt_in.rz_);

        objectinfo tmp = tf_transport(desired_frame,p_in);
        return tmp;

    }
    geometry_msgs::PoseStamped manipulationClass::tf_transport2(std::string desired_frame, geometry_msgs::PoseStamped pt_in)
    {
        geometry_msgs::PoseStamped pt_out;
        //pt_in.header.frame_id = "base_link";

        listener_.transformPose(desired_frame,pt_in,pt_out);
        return pt_out;
    }
}