#include<final_manipulation/final_manipulation_inc.h>
namespace final_manipulation
{
    void manipulationClass::createArmClient(arm_control_client_Ptr& actionClient)
    {
        ROS_INFO("Creating action client to arm controller ...");

        //actionClient.reset( new arm_control_client("/safe_arm_controller/follow_joint_trajectory") );
        actionClient.reset( new arm_control_client("/arm_controller/follow_joint_trajectory") );

        int iterations = 0, max_iterations = 3;
        // Wait for arm controller action server to come up
        while( !actionClient->waitForServer(ros::Duration(2.0)) && ros::ok() && iterations < max_iterations )
        {
            ROS_DEBUG("Waiting for the arm_controller_action server to come up");
            ++iterations;
        }

        if ( iterations == max_iterations )
            throw std::runtime_error("Error in createArmClient: arm controller action server not available");
    }

    void manipulationClass::jointStateCb(const sensor_msgs::JointState::ConstPtr& msg)
    {
        /* read the 7 arm joint value */
        std::vector<std::string> joint_name = msg->name;
        std::vector<double> joint_position = msg->position;
        std::vector<double> joint_velocity = msg->velocity;
        unsigned int num_joint = joint_name.size();
        for (int i = 0; i < num_joint; i ++)
        {
            if (joint_name[i] == "arm_1_joint"){
                joint_states[0] = joint_position[i];
                joint_vel[0] = joint_velocity[i];
            }
            if (joint_name[i] == "arm_2_joint"){
                joint_states[1] = joint_position[i];
                joint_vel[1] = joint_velocity[i];
            }
            if (joint_name[i] == "arm_3_joint"){
                joint_states[2] = joint_position[i];
                joint_vel[2] = joint_velocity[i];
            }
            if (joint_name[i] == "arm_4_joint"){
                joint_states[3] = joint_position[i];
                joint_vel[3] = joint_velocity[i];
            }
            if (joint_name[i] == "arm_5_joint"){
                joint_states[4] = joint_position[i];
                joint_vel[4] = joint_velocity[i];
            }
            if (joint_name[i] == "arm_6_joint"){
                joint_states[5] = joint_position[i];
                joint_vel[5] = joint_velocity[i];
            }
            if (joint_name[i] == "arm_7_joint"){
                joint_states[6] = joint_position[i];
                joint_vel[6] = joint_velocity[i];
            }
        }
        jointState_ready = true;
        //ROS_INFO_STREAM("joint state:"<<joint_states);
    }
}
