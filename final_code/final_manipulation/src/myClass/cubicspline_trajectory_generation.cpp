#include <final_manipulation/final_manipulation_inc.h>//include the headfile
namespace final_manipulation
{

 
 

 cubicspline manipulationClass::getYbyX(double *x_data, double *y_data,double &x_in, double &y_out)
    {
        double *x_sample_, *y_sample_;

        double *M_;

        int sample_count_;
        int count = 2;

        //load data:
        x_sample_ = new double[count];
        y_sample_ = new double[count];
        M_        = new double[count];
        sample_count_ = count;
    
        memcpy(x_sample_, x_data, sample_count_*sizeof(double));
        memcpy(y_sample_, y_data, sample_count_*sizeof(double));    


        //spline:
        double f1=0,f2=0;    
        double *a=new double[sample_count_];                //  a:稀疏矩阵最下边一串数
        double *b=new double[sample_count_];                //  b:稀疏矩阵最中间一串数
        double *c=new double[sample_count_];                //  c:稀疏矩阵最上边一串数
        double *d=new double[sample_count_];

        double *f=new double[sample_count_];   
        double *bt=new double[sample_count_];
        double *gm=new double[sample_count_];
        double *h=new double[sample_count_];
     
        for(int i=0;i<sample_count_;i++)
            b[i]=2;                                //  中间一串数为2
        for(int i=0;i<sample_count_-1;i++)
            h[i]=x_sample_[i+1]-x_sample_[i];      // 各段步长
        for(int i=1;i<sample_count_-1;i++)
            a[i]=h[i-1]/(h[i-1]+h[i]);
        a[sample_count_-1]=1;
    
        c[0]=1;
        for(int i=1;i<sample_count_-1;i++)
            c[i]=h[i]/(h[i-1]+h[i]);  

        for(int i=0;i<sample_count_-1;i++)
            f[i]=(y_sample_[i+1]-y_sample_[i])/(x_sample_[i+1]-x_sample_[i]);

        for(int i=1;i<sample_count_-1;i++)
            d[i]=6*(f[i]-f[i-1])/(h[i-1]+h[i]);

        //count == 1: 
         {
            d[0]=6*(f[0]-f1)/h[0];
            d[sample_count_-1]=6*(f2-f[sample_count_-2])/h[sample_count_-2];
     
            bt[0]=c[0]/b[0];
            for(int i=1;i<sample_count_-1;i++)
                bt[i]=c[i]/(b[i]-a[i]*bt[i-1]);
            gm[0]=d[0]/b[0];

            for(int i=1;i<=sample_count_-1;i++)
                gm[i]=(d[i]-a[i]*gm[i-1])/(b[i]-a[i]*bt[i-1]);  
            M_[sample_count_-1]=gm[sample_count_-1];

            for(int i=sample_count_-2;i>=0;i--)
                M_[i]=gm[i]-bt[i]*M_[i+1];
        }
        //get cubicspline_y by x:
        int klo,khi,k;
        klo=0;
        khi=sample_count_-1;
        double hh,bb,aa;    
        //  二分法查找x所在区间段

        while(khi-klo>1)
        {
           k=(khi+klo)>>1;
            if(x_sample_[k]>x_in)
                khi=k;
            else
                klo=k;
        }

        hh=x_sample_[khi]-x_sample_[klo];    
        aa=(x_sample_[khi]-x_in)/hh;
        bb=(x_in-x_sample_[klo])/hh;   
        y_out=aa*y_sample_[klo]+bb*y_sample_[khi]+((aa*aa*aa-aa)*M_[klo]+(bb*bb*bb-bb)*M_[khi])*hh*hh/6.0;
        //////test

        double acc = 0, vel = 0;
        acc = (M_[klo]*(x_sample_[khi]-x_in) + M_[khi]*(x_in - x_sample_[klo])) / hh;
       
        vel = M_[khi]*(x_in - x_sample_[klo]) * (x_in - x_sample_[klo]) / (2 * hh)
            - M_[klo]*(x_sample_[khi]-x_in) * (x_sample_[khi]-x_in) / (2 * hh)
            + (y_sample_[khi] - y_sample_[klo])/hh
            - hh*(M_[khi] - M_[klo])/6;
        //printf("%0.9f, %0.9f, %0.9f\n",y_out, vel, acc);
        cubicspline y_v_a;
        y_v_a.y_out = y_out;
        y_v_a.vel = vel;
        y_v_a.acc = acc;
   
        //return trajectory;
        return y_v_a;
    }

     Traj manipulationClass::generate_cubicspLine_trajectory( double pose_diff,int num_interpolation)
    {
        // generate a cubicSpline motion path
        Traj trajectory;
        Eigen::VectorXd traj(num_interpolation);
        Eigen::VectorXd Vel(num_interpolation);
        Eigen::VectorXd Acc(num_interpolation);
        //Eigen::Matrix<double, 3, num_interpolation> d_matrix;
        cubicspline y_v_a;

        double T = ceil(fabs(pose_diff)/0.2);
        double x_data[2] ={0, T};
        double y_data[2] ={0,pose_diff};
        double x_out = 0;
        double y_out = 0;
        int i;
        double T_delta = T/num_interpolation;
        //x_out = -T_delta ;
        for( i=0; i<num_interpolation; i++)
       {
        x_out = x_out + T_delta;
        //d_pose(i)=getYbyX(x_data, y_data, x_out, y_out);;
        y_v_a = getYbyX(x_data, y_data, x_out, y_out);
        traj(i) = y_v_a.y_out;
        Vel(i)  = y_v_a.vel;
        Acc(i)  = y_v_a.acc;
        //d_matrix(1,i)=y_v_a.y_out;
        //d_matrix(1,i)=y_v_a.y_out
        //d_matrix(1,i)=y_v_a.y_out
        
        //d_pose(i,1)=y_v_a.y_out;
        //spline.getYbyX(x_out, y_out);
        //printf("%f, %0.9f \n", x_out, y_out);
       }
        //double t_end = 4 * fabs(pose_diff) / 3 / vmax;
//        double delta_t = (t_end - t_from_start) / num_interpolation;
//        Eigen::VectorXd d_pose(num_interpolation);
//        for (int index = 0; index < num_interpolation; index++)
//        {
//            t_from_start = delta_t * (index + 1);
//            //*(d_pose + index) = Cartesian::create_trajectory(t_from_start, vmax, pose_diff);
//            d_pose(index) = create_trajectory(t_from_start, vmax, pose_diff);
//        }

        trajectory.traj = traj;
        trajectory.vel  = Vel;
        trajectory.acc  = Acc;
        return trajectory;
    }
    
    void manipulationClass::cub_waypoints_arm_goal(control_msgs::FollowJointTrajectoryGoal& goal, Eigen::MatrixXd desired_joint_pose, int num_sub_path)
    {
        // The joint names, which apply to all waypoints
        goal.trajectory.joint_names.push_back("arm_1_joint");
        goal.trajectory.joint_names.push_back("arm_2_joint");
        goal.trajectory.joint_names.push_back("arm_3_joint");
        goal.trajectory.joint_names.push_back("arm_4_joint");
        goal.trajectory.joint_names.push_back("arm_5_joint");
        goal.trajectory.joint_names.push_back("arm_6_joint");
        goal.trajectory.joint_names.push_back("arm_7_joint");

        // num_sub_path waypoints in this goal trajectory
        goal.trajectory.points.resize(num_sub_path);
        
        int index = 0;
        for (int i = 0; i < num_sub_path; i++)
        {
            // Positions
            goal.trajectory.points[index].positions.resize(7);
            goal.trajectory.points[index].positions[0] = desired_joint_pose(0, index);//   desired_joint_pose[0][index];
            goal.trajectory.points[index].positions[1] = desired_joint_pose(1, index);// [1][index];
            goal.trajectory.points[index].positions[2] = desired_joint_pose(2, index);// [2][index];
            goal.trajectory.points[index].positions[3] = desired_joint_pose(3, index);// [3][index];
            goal.trajectory.points[index].positions[4] = desired_joint_pose(4, index);// [4][index];
            goal.trajectory.points[index].positions[5] = desired_joint_pose(5, index);// [5][index];
            goal.trajectory.points[index].positions[6] = desired_joint_pose(6, index);// [6][index];

            ROS_INFO("joint_pose is: %f", goal.trajectory.points[index].positions[0]);
            
            // Velocities
            goal.trajectory.points[index].velocities.resize(7);
            for (int j = 0; j < 7; ++j)
            {
            goal.trajectory.points[index].velocities[j] = 0.0;
            }
            // To be reached specialized seconds after starting along the trajectory
            goal.trajectory.points[index].time_from_start = ros::Duration(2.0 * (index + 1));

            index += 1;
        }
    }


         void manipulationClass::cubs_waypoints_arm_goal(control_msgs::FollowJointTrajectoryGoal& goal, KDL::JntArray joint_d, int num_sub_path)
    {
        // The joint names, which apply to all waypoints
        goal.trajectory.joint_names.push_back("arm_1_joint");
        goal.trajectory.joint_names.push_back("arm_2_joint");
        goal.trajectory.joint_names.push_back("arm_3_joint");    
        goal.trajectory.joint_names.push_back("arm_4_joint");
        goal.trajectory.joint_names.push_back("arm_5_joint");
        goal.trajectory.joint_names.push_back("arm_6_joint");
        goal.trajectory.joint_names.push_back("arm_7_joint");

        // Two waypoints in this goal trajectory
        goal.trajectory.points.resize(1);

        // First trajectory point
        // Positions
        int index = 0;int count = 0;
         for (int i = 0; i < num_sub_path; i++)
        {
        count = index+1; 
        goal.trajectory.points[index].positions.resize(7);
        goal.trajectory.points[index].positions[0] = joint_d(0)*count/num_sub_path;
        goal.trajectory.points[index].positions[1] = joint_d(1)*count/num_sub_path;
        goal.trajectory.points[index].positions[2] = joint_d(2)*count/num_sub_path;  
        goal.trajectory.points[index].positions[3] = joint_d(3)*count/num_sub_path;
        goal.trajectory.points[index].positions[4] = joint_d(4)*count/num_sub_path;
        goal.trajectory.points[index].positions[5] = joint_d(5)*count/num_sub_path;
        goal.trajectory.points[index].positions[6] = joint_d(6)*count/num_sub_path;
         
        // Velocities
        goal.trajectory.points[index].velocities.resize(7);
        for (int j = 0; j < 7; ++j)
        {
            goal.trajectory.points[index].velocities[j] = 0.0;
        }
        
            index += 1;
        }

        // To be reached 0.1 second after starting along the trajectory
        goal.trajectory.points[index].time_from_start = ros::Duration(0.02);
    }

        desired_joint  manipulationClass::calculate_joint( objectinfo current_pose , objectinfo target_pose, int num_sub_path)
        {
         Eigen::Matrix<double, 7, Eigen::Dynamic> desired_joint_pose;
         desired_joint Desired_joint_pose;

        double delta_x,delta_y,delta_z;
        delta_x = target_pose.x_ - current_pose.x_;
        delta_y = target_pose.y_ - current_pose.y_;
        delta_z = target_pose.z_ - current_pose.z_;
        ROS_INFO_STREAM(" delta x: "<<delta_x<<" delta y: "<<delta_y<<" delta z: "<<delta_z);
        //double delta_1,delta_2,delta_3,delta_4,delta_5,delta_6,delta_7;
        

        double max_p_diff = (fabs(delta_x) > fabs(delta_y)) ? (delta_z) : (delta_y);
        int max_index = (fabs(delta_x) > fabs(delta_y)) ? 0 : 1;
        max_p_diff = ( fabs(max_p_diff) > fabs(delta_z)) ? max_p_diff : (delta_z);
        max_index = (fabs(max_p_diff) > fabs(delta_z)) ? max_index : 2;
        ROS_INFO_STREAM("max_p_diff:"<<max_p_diff);
        ROS_INFO_STREAM("max_index:"<<max_index);


        Eigen::VectorXd x_traj = my_x_array;
        Eigen::VectorXd y_traj = my_y_array;
        Eigen::VectorXd z_traj = my_z_array;
        Traj a,b,c;
        Eigen::VectorXd x_test_traj = my_x_array;
        double t_duration = 4 * fabs(max_p_diff) / 3 / cartesian_vmax;
        ROS_INFO_STREAM("t_duration:"<<t_duration);
        ROS_INFO_STREAM("cartesian_vmax:"<<cartesian_vmax);
        ROS_INFO_STREAM("num_subpath:"<<num_subpath);
        switch (max_index)
        {
            case 0: // x_error largest
            {
               // x_traj = generate_trajectory(cartesian_vmax, max_p_diff, num_subpath);
               // y_traj = generate_trajectory(4 * fabs(delta_y) / 3 / t_duration, delta_y + 0.0001, num_subpath);
              //  z_traj = generate_trajectory(4 * fabs(delta_z) / 3 / t_duration, delta_z + 0.0001, num_subpath);
               // x_traj = generate_cubicspLine_trajectory( max_p_diff, num_subpath);
                //y_traj = generate_cubicspLine_trajectory( delta_y + 0.0001, num_subpath);
               // z_traj = generate_cubicspLine_trajectory( delta_z + 0.0001, num_subpath);
                  a = generate_cubicspLine_trajectory( max_p_diff, num_subpath);       x_traj = a.traj;
                  b = generate_cubicspLine_trajectory( delta_y + 0.0001, num_subpath); y_traj = b.traj;
                  c = generate_cubicspLine_trajectory( delta_z + 0.0001, num_subpath); z_traj = c.traj;
            }
            break;
            case 1: // y_error largest
            {
                //y_traj = generate_trajectory(cartesian_vmax, max_p_diff, num_subpath);
                //x_traj = generate_trajectory(4 * fabs(delta_x) / 3 / t_duration, delta_x + 0.0001, num_subpath);
                //z_traj = generate_trajectory(4 * fabs(delta_z) / 3 / t_duration, delta_z + 0.0001, num_subpath);
               // y_traj = generate_cubicspLine_trajectory(max_p_diff, num_subpath);
               // x_traj = generate_cubicspLine_trajectory(delta_x + 0.0001, num_subpath);
               // z_traj = generate_cubicspLine_trajectory(delta_z + 0.0001, num_subpath);
                  a = generate_cubicspLine_trajectory(max_p_diff, num_subpath);       y_traj = a.traj;
                  b = generate_cubicspLine_trajectory(delta_x + 0.0001, num_subpath); x_traj = b.traj;
                  c = generate_cubicspLine_trajectory(delta_z + 0.0001, num_subpath); z_traj = c.traj;
            }
            break;
            case 2: // z_error largest
            {
                //z_traj = generate_trajectory(cartesian_vmax, max_p_diff, num_subpath);
                //y_traj = generate_trajectory(4 * fabs(delta_y) / 3 / t_duration, delta_y + 0.0001, num_subpath);
                //x_traj = generate_trajectory(4 * fabs(delta_x) / 3 / t_duration, delta_x + 0.0001, num_subpath);
               // z_traj = generate_cubicspLine_trajectory(max_p_diff, num_subpath);
               // y_traj = generate_cubicspLine_trajectory( delta_y + 0.0001, num_subpath);
               // x_traj = generate_cubicspLine_trajectory( delta_x + 0.0001, num_subpath);
                  a = generate_cubicspLine_trajectory(max_p_diff, num_subpath);        z_traj = a.traj;  
                  b = generate_cubicspLine_trajectory( delta_y + 0.0001, num_subpath); y_traj = b.traj;
                  c = generate_cubicspLine_trajectory( delta_x + 0.0001, num_subpath); x_traj = c.traj;
            }
            break;
        }
        ROS_INFO_STREAM(" x_traj_function: "<<x_traj);
        ROS_INFO_STREAM(" y_traj:_function "<<y_traj);
        ROS_INFO_STREAM(" z_traj:_function "<<z_traj);

         for(int i=0;i<num_subpath;i++)
        {
            KDL::Vector traj_vec = KDL::Vector(x_traj[i] + current_pose.x_, y_traj[i] + current_pose.y_, z_traj[i] + current_pose.z_);
            double roll = target_pose.rx_;
            double pitch = target_pose.ry_;//ry_d;
            double yaw = target_pose.rz_;//rz_d;
            KDL::Rotation traj_rot = KDL::Rotation::RPY(roll,pitch,yaw);
            KDL::Frame cartesian_pose;
            cartesian_pose = KDL::Frame(traj_rot,traj_vec);
            KDL::JntArray jointpositions = KDL::JntArray(nj);
            KDL::ChainIkSolverPos_LMA iksolver = KDL::ChainIkSolverPos_LMA(chain, eps, maxiter, eps_joints);
            bool kinematics_status;
            // calculate desired joint positions from desired catesian pose
            kinematics_status = iksolver.CartToJnt(initialjointpositions, cartesian_pose, jointpositions);
            if (kinematics_status >= 0)
            {
                //ROS_INFO("Joint positions should be: %f, %f, %f, %f, %f, %f, %f", jointpositions(0), jointpositions(1),
                //          jointpositions(2), jointpositions(3), jointpositions(4), jointpositions(5), jointpositions(6));
                //ROS_INFO("Successfully obtain the desired joint positions!");
            }
            else
            {
                ROS_WARN("Error: Could not find corresponding inverse kinematics!");
               // return -1;
            }
            desired_joint_pose.resize(7, num_subpath);
            for (int j = 0; j < 7; j++)
            {
                desired_joint_pose(j, i) = jointpositions(j);
            }
        }
         ROS_INFO_STREAM(" desired_joint_pose finished! ");
         Desired_joint_pose.desired_joint_pose = desired_joint_pose;

         return Desired_joint_pose;
         }


         void manipulationClass::rotate_movebase(double angle)
    {
        ros::Publisher pub_rotate = nh_.advertise<geometry_msgs::Twist>("/mobile_base_controller/cmd_vel",100);
        ROS_INFO_STREAM(" rotate movebase : ");
        ros::Duration(3).sleep();
        ros::Rate r_rotate(10);
        geometry_msgs::Twist initial;
        initial.linear.x = 0.0;initial.linear.y=0.0;initial.linear.z=0.0;
        initial.angular.z = 0.0;initial.angular.z = 0.0;initial.angular.z = 0.3925;
        int t;t = floor(fabs(angle)/0.3925);
        if (angle < 0)
        {initial.angular.z = -0.3925;}
        ros::Time t_totate=ros::Time::now();

         while((ros::Time::now().toSec()-t_totate.toSec())<t)
     {
         
         pub_rotate.publish(initial);
         ros::spinOnce();
         r_rotate.sleep();
     }
      
        ROS_INFO_STREAM(" rotate finished ! ");
    }
   
        
        


}
