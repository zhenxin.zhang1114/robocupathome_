#include <final_manipulation/final_manipulation_inc.h>//include the headfile
namespace final_manipulation
{
    bool manipulationClass::cartesian_control_Cb(flowerfanggirl::PoseStamped::Request &req,flowerfanggirl::PoseStamped::Response &res)
    {
      ROS_INFO_STREAM("in cartesian control callback");
//----- save the initial position -----//
       speak("I am super intelligent , and I find the object");
       ros::Duration(2).sleep();
       objectinfo current_pose,current_pose_old,target_pose,target_pose_new;
       for (int j = 0; j < nj; j++)
       {
           initialjointpositions(j) = joint_states[j];
       }
       current_pose_old=get_realtime_pose(initialjointpositions,chain);

//----- adjust the torso position-----//
      
       low_torso(0.15);
       speak("I adjust my torso");
       arm3_adjust();
//----- compute the gripper position-----//
       speak("I pick the object right now");
       for (int j = 0; j < nj; j++)
       {
           initialjointpositions(j) = joint_states[j];
       }
       current_pose=get_realtime_pose(initialjointpositions,chain);
       geometry_msgs::PoseStamped target_pose_stamped;
       target_pose_stamped.header.frame_id="base_link";
       target_pose_stamped.pose.position=req.position;
       target_pose_stamped.pose.position.z = target_pose_stamped.pose.position.z +0.02;
       //target_pose_stamped.pose.position.y = target_pose_stamped.pose.position.y + 0.02;
       //target_pose_stamped.pose.position.y = target_pose_stamped.pose.position.y;
       //target_pose_stamped.pose.position.x = target_pose_stamped.pose.position.x - 0.02;
    // target_pose_stamped.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(req.RPY.x,req.RPY.y,req.RPY.z);
       target_pose_stamped.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(1.57,1.57,0.0);
       target_pose = tf_transport("torso_lift_link",target_pose_stamped);
       ROS_INFO_STREAM("target pose in torso lift link:"<<target_pose.x_<<target_pose.y_<<target_pose.z_);

       ROS_INFO_STREAM("current pose:"<<current_pose.x_<<current_pose.y_<<current_pose.z_);

       desired_joint test = calculate_joint(  current_pose ,  target_pose, num_subpath);
       control_msgs::FollowJointTrajectoryGoal arm_goal;
       cub_waypoints_arm_goal(arm_goal, test.desired_joint_pose , num_subpath);
       //waypoints_arm_goal(arm_goal, desired_joint_pose, num_subpath);
       //cub_waypoints_arm_goal(arm_goal, desired_joint_pose, c.vel,num_subpath);
       // Sends the command to start the given trajectory 1s from now
       arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.2);
       //ROS_INFO_STREAM("" << arm_goal);
       ArmClient->sendGoal(arm_goal);

       // Wait for trajectory execution
       while(!(ArmClient->getState().isDone()) && ros::ok())
       {
       ros::Duration(1).sleep(); // sleep for four seconds
       ROS_INFO("SUCCESS!");}

//----- compute the gripper position-----//
       target_pose_stamped.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(1.57,1.57,req.RPY.z-1.5707963);
       target_pose_stamped.pose.position=req.position;
       //target_pose_stamped.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(req.RPY.x,req.RPY.y,req.RPY.z);
      // target_pose_stamped.pose.position.y = target_pose_stamped.pose.position.y - 0.02;
       //target_pose_stamped.pose.position.x = target_pose_stamped.pose.position.x + 0.02;
       target_pose_new = tf_transport("torso_lift_link",target_pose_stamped);
       desired_joint test_angle = calculate_joint(  target_pose ,  target_pose_new, num_subpath);
       control_msgs::FollowJointTrajectoryGoal arm_goal_angle;
       cub_waypoints_arm_goal(arm_goal_angle, test_angle.desired_joint_pose , num_subpath);
       arm_goal_angle.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.2);
       //ROS_INFO_STREAM("" << arm_goal_angle);
       ArmClient->sendGoal(arm_goal_angle);
       while(!(ArmClient->getState().isDone()) && ros::ok())
       {
       ros::Duration(1).sleep(); // sleep for four seconds
       ROS_INFO(" Rotate SUCCESS! ");}
       speak("pick up finished");
//----- adjust the torso position-----//
       // set the shared_point "sharedTorso" to get the actuall states of the torso
       boost::shared_ptr<control_msgs::JointTrajectoryControllerState const> sharedTorso;
       control_msgs::JointTrajectoryControllerState sharedtorso;
       sharedTorso = ros::topic::waitForMessage< control_msgs::JointTrajectoryControllerState >("/torso_controller/state",nh_);
       if (sharedTorso !=NULL)
      { sharedtorso = *sharedTorso;}

      //target_gripper_control["torso_lift_joint"] = sharedtorso.desired.positions[0] ;
       double height;
       height = sharedtorso.desired.positions[0];
       low_torso(height-0.12);
       gripper_pick(0.02);
       low_torso(height);
       //gripper_pick(0.04);
//----- go back to the initial position-----//
       control_msgs::FollowJointTrajectoryGoal arm_goal_back;
       desired_joint test_back= calculate_joint( target_pose_new, current_pose_old , num_subpath);
       cub_waypoints_arm_goal(arm_goal_back, test_back.desired_joint_pose , num_subpath);

       arm_goal_back.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.2);
       //ROS_INFO_STREAM(" this is the initial joint_positions " << arm_goal_back);
       ArmClient->sendGoal(arm_goal_back);
       ROS_INFO(" go back to the initial positions ");

       while(!(ArmClient->getState().isDone()) && ros::ok())
       {
       ros::Duration(1).sleep(); // sleep for four seconds
       ROS_INFO("SUCCESS!");}
       speak("movement finished,thank you");

        return true;
    }
}
