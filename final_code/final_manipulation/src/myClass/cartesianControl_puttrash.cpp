#include <final_manipulation/final_manipulation_inc.h>//include the headfile
namespace final_manipulation
{
    bool manipulationClass::cartesian_control_puttrash_Cb(flowerfanggirl::PoseStamped::Request &req,flowerfanggirl::PoseStamped::Response &res)
    {
        ROS_INFO_STREAM("in cartesian control put  callback");
        speak("I am super intelligent , and I find the object");
        ros::Duration(3).sleep();
        low_torso(0.30);
        objectinfo current_pose_initial,current_pose_old,current_pose,target_pose,target_pose_rotate;

 //----- save the initial position -----//


        for (int j = 0; j < nj; j++)
        {
            initialjointpositions(j) = joint_states[j];
        }

        current_pose_initial = get_realtime_pose(initialjointpositions,chain);

        arm3_adjust();

 //----- save the position after arm3_adjust() -----//

        for (int j = 0; j < nj; j++)
        {
            initialjointpositions(j) = joint_states[j];
        }

        current_pose_old = get_realtime_pose(initialjointpositions,chain);

 //----- compute the gesture for target_new -----//

        objectinfo target_new;
        Eigen::Matrix<double, 7, 1> joint_states_new;
        joint_states_new[0] = 0.40;
        joint_states_new[1] = 0.24;
        joint_states_new[2] = -1.6;
        joint_states_new[3] = 1.94;
        joint_states_new[4] = -1.57;
        joint_states_new[5] = 1.37;
        joint_states_new[6] = 0.0;

         for (int j = 0; j < nj; j++)
        {
            initialjointpositions(j) = joint_states_new[j];
        }

        ROS_INFO(" gesture change finished~ ");
        target_new = get_realtime_pose( initialjointpositions,chain);

        target_new.rz_ = 1.57; target_new.ry_ = 0.0; 

 //----- change the gesture -----//

        desired_joint test_put = calculate_joint(  current_pose_old, target_new, num_subpath);
        control_msgs::FollowJointTrajectoryGoal arm_goal_put;
        cub_waypoints_arm_goal(arm_goal_put, test_put.desired_joint_pose , num_subpath);

        arm_goal_put.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.2);
        ArmClient->sendGoal(arm_goal_put);
        ROS_INFO(" adjust movements finished~ ");
         while(!(ArmClient->getState().isDone()) && ros::ok())
        {
        ros::Duration(1).sleep(); // sleep for four seconds
        ROS_INFO("SUCCESS!");}
        speak("hello bitch, I adjust my gesture");
        ros::Duration(2).sleep();
 //----- compute the gesture to put the object -----//

        for (int j = 0; j < nj; j++)
        {
            initialjointpositions(j) = joint_states[j];
        }
        current_pose = get_realtime_pose(initialjointpositions,chain);
        geometry_msgs::PoseStamped target_pose_stamped;
        target_pose_stamped.header.frame_id="base_link";
        target_pose_stamped.pose.position=req.position;
        target_pose_stamped.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(1.57,0,0);
        target_pose = tf_transport("torso_lift_link",target_pose_stamped);

 //----- change the gesture -----//

        desired_joint test = calculate_joint(  target_new ,  target_pose, num_subpath);
        control_msgs::FollowJointTrajectoryGoal arm_goal;
        cub_waypoints_arm_goal(arm_goal, test.desired_joint_pose , num_subpath);
        arm_goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.2);

        ArmClient->sendGoal(arm_goal);

        // Wait for trajectory execution
        while(!(ArmClient->getState().isDone()) && ros::ok())
        {
            ros::Duration(1).sleep(); //
            ROS_INFO("SUCCESS!");}

 //----- put the object on the kitchen -----//
        gripper_pick(0.04);
        speak("hello, I throw the trash ");
        ros::Duration(2).sleep();
 //----- gesture reset -----//

        control_msgs::FollowJointTrajectoryGoal arm_goal_back;
        desired_joint test_back= calculate_joint( target_pose, current_pose_initial , num_subpath);
        cub_waypoints_arm_goal(arm_goal_back, test_back.desired_joint_pose , num_subpath);

        arm_goal_back.trajectory.header.stamp = ros::Time::now() + ros::Duration(0.2);
        ArmClient->sendGoal(arm_goal_back);
        ROS_INFO(" go back to the initial positions ");

        while(!(ArmClient->getState().isDone()) && ros::ok())
        {
            ros::Duration(1).sleep(); // sleep for four seconds
                ROS_INFO("SUCCESS!");}

         low_torso(0.05);
         speak("hello, Big DJ!I want to pick another one ");
         ros::Duration(3).sleep();
        return true;
    }
}

