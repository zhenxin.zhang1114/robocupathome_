#include<final_manipulation/final_manipulation_inc.h>
namespace final_manipulation
{
    bool manipulationClass::testservicecb(std_srvs::Empty::Request &req,std_srvs::Empty::Response &res)
    {
        ROS_INFO_STREAM("!test tf");
        geometry_msgs::PoseStamped pose_in;
        objectinfo pose_out;

        return true;
    }

    bool manipulationClass::test_tfcb(flowerfanggirl::PoseStamped::Request &req,flowerfanggirl::PoseStamped::Response &res)
    {
        geometry_msgs::PoseStamped pose_in;
        pose_in.header.frame_id = "base_link";
        pose_in.pose.position = req.position;
        pose_in.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(req.RPY.x,req.RPY.y,req.RPY.z);
        objectinfo p_out = tf_transport("torso_lift_link",pose_in);
        ROS_INFO_STREAM("pose in baselink:"<<req.position<<"RPY:"<<req.RPY.x<<req.RPY.y<<req.RPY.z);
        ROS_INFO_STREAM("pose in torso lift link:"<<p_out.x_<<p_out.y_<<p_out.z_<<p_out.rx_<<p_out.ry_<<p_out.rz_);
        return true;
    }
    
    bool manipulationClass::currentpossrvcb(std_srvs::Empty::Request &req,std_srvs::Empty::Response &res)
    {
        objectinfo current_pos;
        ROS_INFO_STREAM("in current pose callback");
        /* get the initial position */
        for (int j = 0; j < nj; j++)
        {
            //ROS_INFO_STREAM("current"<<j<<":"<<joint_states[j]);
            current_jointpositions(j) = joint_states[j];
        }
        ROS_INFO_STREAM("joint states, initial positions:"<<current_jointpositions.data);
        objectinfo current_pose,current_pose_base,pose_error;
        current_pose=get_realtime_pose(current_jointpositions,chain);
        
        current_pose_base = tf_transport1("base_link",current_pose,"torso_lift_link");
        ROS_INFO_STREAM("current pose base on torsolift:"<<current_pose.x_<<","<<current_pose.y_<<","<<current_pose.z_<<","<<current_pose.rx_<<","<<current_pose.ry_<<","<<current_pose.z_);
        ROS_INFO_STREAM("current pose base on base_link:"<<current_pose_base.x_<<","<<current_pose_base.y_<<","<<current_pose_base.z_<<","<<current_pose_base.rx_<<","<<current_pose_base.ry_<<","<<current_pose_base.z_);
        return true;
    }
}
 
 
    