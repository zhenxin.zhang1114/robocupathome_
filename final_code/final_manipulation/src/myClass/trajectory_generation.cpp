#include <final_manipulation/final_manipulation_inc.h>//include the headfile
namespace final_manipulation
{
    double manipulationClass::create_trajectory(double t, double vmax, double d)
    {
        double trajectory = 0;
        double fabs_d =fabs(d);
        // first phase: acceleration
        if (t < fabs(d) / 3 / vmax)
            trajectory = 3 * pow(vmax * t, 2) / 2 / d;  
        // second phase: constant speed
        else if (t < fabs(d) / vmax)
            trajectory = d / fabs_d * (t * vmax - fabs_d / 6);
        // third phase: deceleration
        else
            trajectory = d / fabs_d *(fabs_d - 3 / 2 / fabs_d * pow(vmax * (4 * fabs_d / 3 / vmax - t), 2));
        return trajectory;
    }

    double manipulationClass::generate_velocity(double t, double vmax, double d)
    {
        double fabs_d = fabs(d);
        double v_now = 0;
        // first phase: acceleration
        if (t < fabs(d) / 3 / vmax)
            v_now = 3 * pow(vmax, 2) / d * t;
        // second phase: constant speed
        else if (t < fabs(d) / vmax)
            v_now = d / fabs_d * vmax;
        // third phase: deceleration
        else if (t < 4 * fabs(d) / 3 / vmax)
            v_now = (4 * fabs_d / 3 / vmax - t) * 3 * pow(vmax, 2) / d * (4 * fabs(d) / 3 / vmax - t);
        else
            v_now = 0;
        return v_now;
    }

    Eigen::VectorXd manipulationClass::generate_trajectory(double vmax, double pose_diff, int num_interpolation)
    {
        // generate a trapezoidal motion path
        double t_from_start = 0;
        double t_end = 4 * fabs(pose_diff) / 3 / vmax;
        double delta_t = (t_end - t_from_start) / num_interpolation;
        Eigen::VectorXd d_pose(num_interpolation);
        for (int index = 0; index < num_interpolation; index++)
        {
            t_from_start = delta_t * (index + 1);
            //*(d_pose + index) = Cartesian::create_trajectory(t_from_start, vmax, pose_diff);
            d_pose(index) = create_trajectory(t_from_start, vmax, pose_diff);
        }
        return d_pose;
    }

 

    void manipulationClass::cartesian_initial()
    {
        cartesian_vmax = 0.05;
        dt = 0.03;
        num_subpath = 5;
        my_x_array.setZero();
        my_y_array.setZero();
        my_z_array.setZero();
        jointState_ready = false;


    }
    

    void manipulationClass::waypoints_arm_goal(control_msgs::FollowJointTrajectoryGoal& goal, Eigen::MatrixXd desired_joint_pose, int num_sub_path)
    {
        // The joint names, which apply to all waypoints
        goal.trajectory.joint_names.push_back("arm_1_joint");
        goal.trajectory.joint_names.push_back("arm_2_joint");
        goal.trajectory.joint_names.push_back("arm_3_joint");
        goal.trajectory.joint_names.push_back("arm_4_joint");
        goal.trajectory.joint_names.push_back("arm_5_joint");
        goal.trajectory.joint_names.push_back("arm_6_joint");
        goal.trajectory.joint_names.push_back("arm_7_joint");

        // num_sub_path waypoints in this goal trajectory
        goal.trajectory.points.resize(num_sub_path);
        
        int index = 0;
        for (int i = 0; i < num_sub_path; i++)
        {
            // Positions
            goal.trajectory.points[index].positions.resize(7);
            goal.trajectory.points[index].positions[0] = desired_joint_pose(0, index);//   desired_joint_pose[0][index];
            goal.trajectory.points[index].positions[1] = desired_joint_pose(1, index);// [1][index];
            goal.trajectory.points[index].positions[2] = desired_joint_pose(2, index);// [2][index];
            goal.trajectory.points[index].positions[3] = desired_joint_pose(3, index);// [3][index];
            goal.trajectory.points[index].positions[4] = desired_joint_pose(4, index);// [4][index];
            goal.trajectory.points[index].positions[5] = desired_joint_pose(5, index);// [5][index];
            goal.trajectory.points[index].positions[6] = desired_joint_pose(6, index);// [6][index];

            ROS_INFO("joint_pose is: %f", goal.trajectory.points[index].positions[0]);
            
            // Velocities
            goal.trajectory.points[index].velocities.resize(7);
            for (int j = 0; j < 7; ++j)
            {
            goal.trajectory.points[index].velocities[j] = 0.0;
            }
            // To be reached specialized seconds after starting along the trajectory
            goal.trajectory.points[index].time_from_start = ros::Duration(2.0 * (index + 1));

            index += 1;
        }

    }

    void manipulationClass::waypoints_arm_goal(control_msgs::FollowJointTrajectoryGoal& goal, KDL::JntArray joint_d)
    {
        // The joint names, which apply to all waypoints
        goal.trajectory.joint_names.push_back("arm_1_joint");
        goal.trajectory.joint_names.push_back("arm_2_joint");
        goal.trajectory.joint_names.push_back("arm_3_joint");    //tf::Transform base_to_gripper;
            //tf::pointMsgToTF(p_in, p_in_tf);
            //ros::Time now = ros::Time::now();
            //tf::StampedTransform time_transform;
            //listener_.waitForTransform("gripper_grasping_frame", "base_link", now, ros::Duration(3.0));
            //listener_.lookupTransform("gripper_grasping_frame", "base_link", now, base_to_gripper);
        goal.trajectory.joint_names.push_back("arm_4_joint");
        goal.trajectory.joint_names.push_back("arm_5_joint");
        goal.trajectory.joint_names.push_back("arm_6_joint");
        goal.trajectory.joint_names.push_back("arm_7_joint");

        // Two waypoints in this goal trajectory
        goal.trajectory.points.resize(1);

        // First trajectory point
        // Positions
        int index = 0;
        goal.trajectory.points[index].positions.resize(7);
        goal.trajectory.points[index].positions[0] = joint_d(0);
        goal.trajectory.points[index].positions[1] = joint_d(1);
        goal.trajectory.points[index].positions[2] = joint_d(2);
        goal.trajectory.points[index].positions[3] = joint_d(3);
        goal.trajectory.points[index].positions[4] = joint_d(4);
        goal.trajectory.points[index].positions[5] = joint_d(5);
        goal.trajectory.points[index].positions[6] = joint_d(6);

        // Velocities
        goal.trajectory.points[index].velocities.resize(7);
        for (int j = 0; j < 7; ++j)
        {
            goal.trajectory.points[index].velocities[j] = 0.0;
        }
        // To be reached 0.1 second after starting along the trajectory
        goal.trajectory.points[index].time_from_start = ros::Duration(0.02);
    }
}