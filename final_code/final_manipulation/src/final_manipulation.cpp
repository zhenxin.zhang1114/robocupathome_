#include <final_manipulation/final_manipulation_inc.h>//include the headfile
int main( int argc, char** argv )
{

    ros::init(argc, argv, "final_integration",ros::init_options::AnonymousName);//initial node name, AnonymousName can

    ROS_INFO_STREAM("**final manipulation initial");
    ros::AsyncSpinner spinner_main(1);
    spinner_main.start();
    ros::NodeHandle n;//build a nodehandle
    
                ROS_INFO_STREAM("file: " << __FILE__ << " line: " << __LINE__);

    final_manipulation::manipulationClass node(n);
    ros::Rate r(60);
    //ros::spin();
    ros::waitForShutdown();
    return 0;
}