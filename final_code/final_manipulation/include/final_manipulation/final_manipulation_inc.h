#include <final_manipulation/includefiles.h>
namespace final_manipulation{
  struct objectinfo
  {
    double x_;
    double y_;
    double z_;
    double rx_;
    double ry_;
    double rz_;
  };
  struct desired_joint
  {
  //Eigen::Matrix<double, 7, Eigen::Dynamic> desired_joint_pose;
  Eigen::Matrix<double, 7, 5> desired_joint_pose;
  };
  struct cubicspline
  {
    double y_out;
    double vel;
    double acc;
  };
  struct Traj
  {
    Eigen::VectorXd traj;
    Eigen::VectorXd vel;
    Eigen::VectorXd acc;
  };
  class manipulationClass
  {
  private:
    //! The node handle
    ros::NodeHandle nh_;
    //! Node handle in the private namespace
    ros::NodeHandle priv_nh_;
    ros::Publisher display_publisher;
    //action interface for arm controller
    ros::ServiceServer grasp_server1,grasp_serverRPY1,head_look_around,test_server;
    ros::ServiceServer grasp_put_server;
    ros::ServiceServer trash_put_server;

    //state machine
    // srv and msg
    flowerfanggirl::Localization loc_srv;
    flowerfanggirl::ObjectRecognition objRec_msg;
    flowerfanggirl::ObjectRecognitionArray objRec_srv;
    flowerfanggirl::Manipulation manipulation_srv;
    geometry_msgs::PointStamped test_msg;
    std_srvs::Empty empty_srv;
    geometry_msgs::Twist rotate_vel;
    moveit_msgs::DisplayTrajectory display_trajectory;


    //tf for coordinate transform
    tf::Transformer obj_in_cam,obj_in_base;

    //moveit
    moveit::planning_interface::MoveGroupInterface group_arm_torso;
    moveit::planning_interface::MoveGroupInterface group_arm;
    //moveit::planning_interface::MoveGroupInterface group_torso;
    moveit::planning_interface::MoveGroupInterface group_gripper;

    void low_torso(double height);
    void gripper_pick(double distance);
    void low_head();
    void head_move_around();
    void movetotarget_arm(geometry_msgs::PoseStamped targetpos);
    void movetotarget_armtorso(geometry_msgs::PoseStamped targetpos);
    void moveto_randomtarget();
    void getRobotModel(const geometry_msgs::PoseStamped& targetpos);
    //void getRobotModel_trajectary_replay();
    //moveit::planning_interface::MoveGroupInterface *test;
    bool pick1(flowerfanggirl::Targetpoint::Request &req,flowerfanggirl::Targetpoint::Response &res);
    bool pickRPY1(flowerfanggirl::Targetpoint1::Request &req,flowerfanggirl::Targetpoint1::Response &res);
    bool headaround(std_srvs::Empty::Request &req,std_srvs::Empty::Response &res);
    bool testservicecb(std_srvs::Empty::Request &req,std_srvs::Empty::Response &res);
    void kdl_test();
    void kdl_ik_c2j();
    void kdl_arm();

    /* arm control */
    typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;
    typedef actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> arm_control_client;
    typedef boost::shared_ptr< arm_control_client>  arm_control_client_Ptr;
    arm_control_client_Ptr ArmClient;
    ros::Subscriber joint_state_sub;
    ros::Publisher joint_position_pub;
    tf::TransformListener listener_;
    void jointStateCb(const sensor_msgs::JointState::ConstPtr& msg);//读取当前关节值
    void createArmClient(arm_control_client_Ptr& actionClient);



    /* variables */
    int service_count = 0;
    double cartesian_vmax,dt;
    int num_subpath;
    Eigen::Matrix<double, 6, Eigen::Dynamic> desired_pose;
    Eigen::Matrix<double, 7, Eigen::Dynamic> desired_joint_pose;
    Eigen::Matrix<double, 7, 1> joint_states;
    Eigen::Matrix<double, 7, 1> joint_vel;
    Eigen::Matrix<double, Eigen::Dynamic, 1> my_x_array;
    Eigen::Matrix<double, Eigen::Dynamic, 1> my_y_array;
    Eigen::Matrix<double, Eigen::Dynamic, 1> my_z_array;
    objectinfo target_object,vor_target_object;

    Traj generate_cubicspLine_trajectory( double pose_diff,int num_interpolation);
    cubicspline getYbyX(double *x_data, double *y_data, double &x_in, double &y_out);
    void cub_waypoints_arm_goal(control_msgs::FollowJointTrajectoryGoal& goal, Eigen::MatrixXd desired_joint_pose, int num_sub_path);
    void cubs_waypoints_arm_goal(control_msgs::FollowJointTrajectoryGoal& goal, KDL::JntArray joint_d,int num_sub_path);
    desired_joint  calculate_joint(objectinfo current_pose , objectinfo target_pose, int num_sub_path);
    void rotate_movebase(double angle);
    void arm3_adjust();
    bool cartesian_control_put_Cb(flowerfanggirl::PoseStamped::Request &req,flowerfanggirl::PoseStamped::Response &res);
    bool cartesian_control_puttrash_Cb(flowerfanggirl::PoseStamped::Request &req,flowerfanggirl::PoseStamped::Response &res);
    void speak(std::string text);
    //double torque_Callback(const geometry_msgs::WrenchStamped::ConstPtr& msg)

    double create_trajectory(double t, double vmax, double d);
    Eigen::VectorXd generate_trajectory(double vmax, double pose_diff, int num_interpolation);
    double generate_velocity(double t, double vmax, double d);
    ros::ServiceServer cartesian_controller_srv;
    ros::ServiceServer test_tf,get_current_pos_srv;
    bool test_tfcb(flowerfanggirl::PoseStamped::Request &req,flowerfanggirl::PoseStamped::Response &res);
    bool currentpossrvcb(std_srvs::Empty::Request &req,std_srvs::Empty::Response &res);
    bool cartesian_control_Cb(flowerfanggirl::PoseStamped::Request &req,flowerfanggirl::PoseStamped::Response &res);
    void cartesian_initial();
    void waypoints_arm_goal(control_msgs::FollowJointTrajectoryGoal& goal, KDL::JntArray joint_d);
    void waypoints_arm_goal(control_msgs::FollowJointTrajectoryGoal& goal, Eigen::MatrixXd desired_joint_pose, int num_sub_path);


    objectinfo tf_transport(std::string desired_frame, geometry_msgs::PoseStamped pt_in );
    objectinfo tf_transport1(std::string desired_frame,  objectinfo pt_in, std::string current_frame);
    geometry_msgs::PoseStamped tf_transport2(std::string desired_frame, geometry_msgs::PoseStamped pt_in);


    objectinfo get_realtime_pose(KDL::JntArray joint_pos, KDL::Chain chain);
    KDL::Jacobian jacob;
    KDL::Tree my_tree;
    bool exit_value, jointState_ready, force_ready, admittance_x_ready, admittance_y_ready;
    KDL::Chain chain;
    // solver for the inverse position kinematics that uses Levenberg-Marquardt
    double eps, eps_joints;
    int maxiter;
    unsigned int nj,num_segments,num_joints;
    double x_init, y_init, z_init, rx_init, ry_init, rz_init;
    KDL::JntArray initialjointpositions,current_jointpositions;
    ros::Time control_begin;

  public:
    manipulationClass(ros::NodeHandle nh) : nh_(nh), priv_nh_("~"), group_arm_torso("arm_torso"), group_arm("arm"),
    group_gripper("gripper")
    {            ROS_INFO_STREAM("file: " << __FILE__ << " line: " << __LINE__);

      display_publisher=nh_.advertise<moveit_msgs::DisplayTrajectory>("/move_group/display_planned_path", 1);

      grasp_server1=nh_.advertiseService("/final/pcik1",&final_manipulation::manipulationClass::pick1,this);

      grasp_serverRPY1=nh_.advertiseService("/final/pcikRPY1",&final_manipulation::manipulationClass::pickRPY1,this);

      head_look_around=nh_.advertiseService("/final/head_look_around",&final_manipulation::manipulationClass::headaround,this);

      test_server=nh_.advertiseService("/final/testservice",&final_manipulation::manipulationClass::testservicecb,this);
      joint_state_sub=nh_.subscribe("/joint_states", 1, &final_manipulation::manipulationClass::jointStateCb, this);
      cartesian_controller_srv=nh_.advertiseService("/final/cartesianControl",&final_manipulation::manipulationClass::cartesian_control_Cb,this);
      test_tf = nh_.advertiseService("/final/testtf",&final_manipulation::manipulationClass::test_tfcb,this);
      get_current_pos_srv = nh_.advertiseService("/final/current_pos_srv",&final_manipulation::manipulationClass::currentpossrvcb,this);
      grasp_put_server = nh_.advertiseService("/final/cartesianControl_put",&final_manipulation::manipulationClass::cartesian_control_put_Cb,this);
      trash_put_server = nh_.advertiseService("/final/cartesianControl_put_trash",&final_manipulation::manipulationClass::cartesian_control_puttrash_Cb,this);
      ROS_INFO_STREAM("---start to move hand to target position---");
      cartesian_initial();
      eps = 1E-5;
      maxiter = 500;
      eps_joints = 1E-15;
      kdl_parser::treeFromFile("/home/flowerfanggirl/final_ffg/src/final_manipulation/configs/tiagotest.urdf", my_tree);
      exit_value = my_tree.getChain("torso_lift_link", "arm_tool_link", chain);


      // initialize the joint positions
      nj = chain.getNrOfJoints();
      num_segments=chain.getNrOfSegments();
      ROS_INFO_STREAM("num of segments:"<<num_segments<<"num of joint:"<<nj);
      for(int i=0;i<num_segments;i++)
      {
        ROS_INFO_STREAM(chain.getSegment(i).getName());
        ROS_INFO_STREAM(chain.getSegment(i).getJoint().getName());
      }
      initialjointpositions = KDL::JntArray(nj);
      current_jointpositions = KDL::JntArray(nj);
      createArmClient(ArmClient);
      if (!ros::Time::waitForValid(ros::WallDuration(10.0))) // NOTE: Important when using simulated clock
      {
        ROS_FATAL("Timed-out waiting for valid time.");
      }

      while( !jointState_ready)
      {
          ROS_INFO("Joint_sub or Force_sub is not ready");
      }
      ROS_INFO("Services are ready to be called");
      //ros::Duration(15).sleep();
      speak("thank you");


    }
    ~manipulationClass() {}
  };




}
