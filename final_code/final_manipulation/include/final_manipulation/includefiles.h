#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <iostream>
#include <fstream>
#include <pthread.h>
#include <std_msgs/String.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <tf_conversions/tf_eigen.h>
#include <tf/tf.h>
#include <nav_msgs/Odometry.h>
#include <std_srvs/Empty.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_state/robot_state.h>
#include <moveit/move_group_interface/move_group_interface.h>


#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/Twist.h>
#include <trajectory_msgs/JointTrajectory.h>
#include <trajectory_msgs/JointTrajectoryPoint.h>
#include <pal_navigation_msgs/Acknowledgment.h>
#include <pal_interaction_msgs/TtsAction.h>
#include <control_msgs/JointTrajectoryControllerState.h>
#include <control_msgs/FollowJointTrajectoryActionGoal.h>
// #include <moveit/planning_interface/move_group_interface.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <map>
#include <pluginlib/class_loader.h>
#include <ros/ros.h>

// MoveIt
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/planning_interface/planning_interface.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <moveit/kinematic_constraints/utils.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/PlanningScene.h>
#include <boost/scoped_ptr.hpp>
#include <boost/shared_ptr.hpp>

#include <flowerfanggirl/Targetpoint.h>
#include <flowerfanggirl/Targetpoint1.h>
#include <XmlRpc.h>
/*kdl library */
#include <kdl/chain.hpp>
#include <kdl/chainfksolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/frames_io.hpp>
#include <kdl_parser/kdl_parser.hpp>
#include <kdl/kdl.hpp>
#include <kdl/tree.hpp>
#include <kdl/segment.hpp>
#include <kdl/chainfksolver.hpp>
#include <kdl_parser/kdl_parser.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainfksolver.hpp> 
#include <kdl/chainiksolver.hpp> 
#include <kdl/chainiksolverpos_lma.hpp> 
#include <kdl/chainiksolverpos_nr.hpp> 
#include <kdl/chainiksolverpos_nr_jl.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>

#include <actionlib/client/simple_action_client.h>
#include <boost/shared_ptr.hpp>
#include <control_msgs/FollowJointTrajectoryAction.h>

#include <flowerfanggirl/ObjectRecognition.h>
#include <flowerfanggirl/ObjectRecognitionArray.h>
#include <flowerfanggirl/Manipulation.h>
#include <flowerfanggirl/Localization.h>
#include <flowerfanggirl/PoseStamped.h>
#include <flowerfanggirl/PoseStamped.h>
#include <flowerfanggirl/AllObjInfoObtain.h>
#include <flowerfanggirl/GraspTarget.h>
#include <flowerfanggirl/ObjectInfo.h>
#include <flowerfanggirl/ObjectInfoStamped.h>
#include <flowerfanggirl/ObjectRecognitionArray.h>
