#include <final_navigation_integration/includefiles.h>
#define PI 3.141592653589793
namespace final_navigation_integration
{
  struct pose_2d
  {
    double x_;
    double y_;
  };
  class navigation_integrationClass
  {

  private:
    //! The node handle
    ros::NodeHandle nh_;
    //! Node handle in the private namespace
    ros::NodeHandle priv_nh_;
    //action
    typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;
    // target positions
    move_base_msgs::MoveBaseGoal goal_table1;
    move_base_msgs::MoveBaseGoal goal_table2;
    move_base_msgs::MoveBaseGoal goal_initial;

    move_base_msgs::MoveBaseGoal goal_objects;
    move_base_msgs::MoveBaseGoal goal_kitchen;
    move_base_msgs::MoveBaseGoal goal_trash;


    flowerfanggirl::targetpose_2d goal_objstack;
    
    // flowerfanggirl::targetpose_2d goal_kitchen_;
    // flowerfanggirl::targetpose_2d goal_trash_;

    geometry_msgs::Pose tiago_current_pose_odom;

    double current_theta,current_x,current_y;
    pal_navigation_msgs::Acknowledgment localModeSrv;
    pal_navigation_msgs::Acknowledgment localMapSrv;

    //service and clients for communication between nodes
    ros::ServiceClient objRecognitionClient;
    ros::ServiceClient manipulationClient;
    ros::ServiceClient putClient, throwClient;
    ros::ServiceClient global_localization_client;
    ros::ServiceClient clear_costmaps_client;//client will call service "/move_base/clear_costmaps"
    ros::ServiceClient localmodeClient;
    ros::ServiceClient localMapClient;
    ros::ServiceServer movebaseserver;
    ros::Publisher localization_pub;
    ros::Publisher pub_head;
    ros::Subscriber cov_localization_sub;
    ros::Subscriber currentposesub;
    ros::Subscriber voice_sub;
    ros::Publisher cmd_velpub;
    ros::Publisher talk_pub;
    ros::ServiceClient movebase_forward;
    ros::ServiceClient movebase_rotate;
    //state set
    enum robot_states {gotoobjstack,objdetection,selectobj,calculate_ref_location,movebasecontroller,graspobj,putobj1,putobj2,judgeobj,gotokitchen,gototrash};
    robot_states robot_state;
    enum state_set {objDetecting,navigating_table1,navigating_table2,navigating_initial,grip_obj,put_obj,head_down};
    state_set tiago_state;
    void move2goal(move_base_msgs::MoveBaseGoal goal);
    void low_torso();
    void low_head();
    void getLocCov(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr &msg);
    pose_2d get_ref_position(geometry_msgs::Point input);
    double loc_acc=1.0;
    //state machine
    void stateMachine();
    void movebasecontrol();
    void statemachine();
    void move2goal1(double dis, double theta);
    double calculate_2ddistance(geometry_msgs::Point input);
    bool movebasecontrolcb(flowerfanggirl::targetpose_2d::Request &req,flowerfanggirl::targetpose_2d::Response &res );
    void rotate_movebase(double angle);
    double move_distance(double x,double y);
    double return_radiens(double x, double y);
    flowerfanggirl::ObjectInfo get_nearestobj(flowerfanggirl::ObjectInfoArray inputarray);
    // srv and msg
    flowerfanggirl::Localization loc_srv;
    flowerfanggirl::ObjectRecognition objRec_msg;
    flowerfanggirl::ObjectRecognitionArray objRec_srv;
    flowerfanggirl::Manipulation manipulation_srv;
    flowerfanggirl::AllObjInfoObtain allobjinfo,allobjinfo1;
    flowerfanggirl::ObjectInfoArray allobjinfoarray,allobjinfoarray1;
    geometry_msgs::Point targetposition;

    geometry_msgs::PointStamped test_msg;
    std_srvs::Empty empty_srv;
    geometry_msgs::Twist rotate_vel;
    
    geometry_msgs::Point target_point,target_table;
    std::string target_class;
    std::string target_class_Id;
    std::vector<flowerfanggirl::ObjectRecognition> allObjectInfo;
    std::vector<int> allObjectId;
    std::string loc_input = "LOC";
    std::string map_input = "final_map";
    std::string current_voice1,current_voice2;

    flowerfanggirl::forward forward_srv;
    flowerfanggirl::rotation rotation_srv;
    void localization1();

    ros::ServiceClient movecli;
    void localization_();

    void initialization();
    void getTiagoPosecb(const nav_msgs::Odometry::ConstPtr &msg);
    
    void move2goal_odom(geometry_msgs::Pose2D target);
    void move2goal2(std::string frame_id, geometry_msgs::Pose2D target);
    void voice_cb(const std_msgs::String::ConstPtr &msg);
    //tf for coordinate transform
    tf::Transformer obj_in_cam,obj_in_base;  

    geometry_msgs::Pose2D goal_stack; 
    geometry_msgs::Pose2D goal_trash_; 
    geometry_msgs::Pose2D goal_kitchen_;
flowerfanggirl::PoseStamped grasp_tar;
flowerfanggirl::PoseStamped grasp_tar_put, throw_tar;
   std::string objclass;
flowerfanggirl::ObjectInfo target_tmp;
                  
    pal_interaction_msgs::TtsActionGoal textgoal;
    void speak(std::string text);
    void rotate_and_go(double angular,double dis);
    void move_head(double head_1_joint_value,double head_2_joint_value);
  

    
  public:
    navigation_integrationClass(ros::NodeHandle nh) : nh_(nh), priv_nh_("~")
    {
      //waiting for move base server
      MoveBaseClient ac_initial("move_base", true);
  

      // while(!ac_initial.waitForServer(ros::Duration(5.0)))
      // {
      //   ROS_INFO("Waiting for the move_base action server to come up");
      // }
      ROS_INFO_STREAM("successful initial move base ");
      talk_pub=nh_.advertise<pal_interaction_msgs::TtsActionGoal>("/tts/goal",100);
      global_localization_client=nh_.serviceClient<std_srvs::Empty>("/global_localization");//client will call service "/global_localization"
      localmodeClient=nh_.serviceClient<pal_navigation_msgs::Acknowledgment>("/pal_navigation_sm");
      localMapClient=nh_.serviceClient<pal_navigation_msgs::Acknowledgment>("/pal_map_manager/change_map");
      localization_pub=nh_.advertise<geometry_msgs::Twist>("/mobile_base_controller/cmd_vel",100);
      pub_head = nh_.advertise<trajectory_msgs::JointTrajectory>("/head_controller/command",100);
      movebaseserver = nh_.advertiseService("/final/movebasecontroller",&final_navigation_integration::navigation_integrationClass::movebasecontrolcb,this);
      clear_costmaps_client=nh_.serviceClient<std_srvs::Empty>("/move_base/clear_costmaps");
      manipulationClient = nh_.serviceClient<flowerfanggirl::PoseStamped>("/final/cartesianControl");
      putClient = nh_.serviceClient<flowerfanggirl::PoseStamped>("/final/cartesianControl_put");
      throwClient = nh_.serviceClient<flowerfanggirl::PoseStamped>("/final/cartesianControl_put_trash");
      objRecognitionClient = nh_.serviceClient<flowerfanggirl::AllObjInfoObtain>("/final/objectrecognition");
      cov_localization_sub = nh_.subscribe("/amcl_pose",100,&navigation_integrationClass::getLocCov,this);
      currentposesub = nh_.subscribe("/mobile_base_controller/odom",1,&navigation_integrationClass::getTiagoPosecb,this);
      std::string mystring;
      cmd_velpub=nh_.advertise<geometry_msgs::Twist>("/mobile_base_controller/cmd_vel",100);
      movecli = nh_.serviceClient<flowerfanggirl::targetpose_2d>("/final/movebasecontroller");
      voice_sub = nh_.subscribe("/recognizer/output",1,&navigation_integrationClass::voice_cb,this);

      movebase_forward = nh_.serviceClient<flowerfanggirl::forward>("/final/forward");
      movebase_rotate = nh_.serviceClient<flowerfanggirl::rotation>("/final/rotation");
      initialization();
      speak("initialization finished");
      ros::Duration(2).sleep();
      // while(1)
      // {
      //   global_localization_client.call(empty_srv);
      //   ROS_INFO_STREAM("localization!");
       
      //   localization_();
      //    ROS_INFO_STREAM("Please check if the location right, if right, input continue ");
      //   std::cin>>mystring;
      //   ROS_INFO_STREAM("mystring.compare:"<<mystring.compare("continue"));
      //   if(mystring.compare("continue"))
      //   { 
          
      //     clear_costmaps_client.call(empty_srv);
      //     break;
      //   }
      // }
     
      low_torso();
      speak("flower fang is ready");
      ros::Duration(2).sleep();
      ros::Rate r_voice(10.0);
   
    
      while(ros::ok())
      {
        current_voice2 = current_voice1;
        ROS_INFO_STREAM(current_voice1<<"   "<<current_voice2);
        ros::Duration(1).sleep();
        if(current_voice2=="begin")
        {
          ROS_INFO_STREAM("I heard "<<current_voice2);
          break;
        }
        ros::spinOnce();
        r_voice.sleep();


      }

      ROS_INFO_STREAM("Localization start");
      speak("Im going to localization");
      ros::Duration(2).sleep();

    ros::Rate r_local(10.0);
       
      while(ros::ok())
      {
        
        localization1();
        ROS_INFO_STREAM("localization finished, is it right?");
        speak("localization finished is it right?");
        ros::Duration(2).sleep();

        
       
        ROS_INFO_STREAM("current voice 2: "<<current_voice2);
        current_voice1 ="novoice";
        while(current_voice1=="novoice")
        {

        }
        current_voice2 = current_voice1;
        if(current_voice2=="yes")
        {
          ROS_INFO_STREAM("localization finished");
          speak("localization finished");
          break;
        }
        else if(current_voice2 == "no")
        {
          continue;
          ROS_INFO_STREAM(__LINE__);

        }
        
        //ros::Duration(10).sleep();
    
        //if()


      }
      
      ROS_INFO_STREAM("after clear");
      ROS_INFO_STREAM("localization finished ");
      speak("localization finished");
      ros::Duration(2).sleep();

      statemachine();


   
   
    }
  };

}

