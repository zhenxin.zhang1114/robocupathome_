#include <final_navigation_integration/final_nav_integration.h>
namespace final_navigation_integration
{
    void navigation_integrationClass::move2goal(move_base_msgs::MoveBaseGoal goal)
    {   
        //move to target 
       MoveBaseClient ac("move_base", true);
       
        //wait for the action server to come up
        while(!ac.waitForServer(ros::Duration(5.0)))
        {
            ROS_INFO("Waiting for the move_base action server to come up");
        }
       // rotate_movebase(req.target.theta);
        //move_base_msgs::MoveBaseGoal goal;

       ROS_INFO_STREAM("move to goal");
        //ROS_INFO("Sending goal");
        ac.sendGoal(goal);
        //ROS_INFO_STREAM("turn "<<theta);
        ac.waitForResult();

      
 ros::Duration(2).sleep();
        

    }

    void navigation_integrationClass::move2goal1(double dis,double theta)
    {
        MoveBaseClient ac("move_base", true);
       
        //wait for the action server to come up
        while(!ac.waitForServer(ros::Duration(5.0)))
        {
            ROS_INFO("Waiting for the move_base action server to come up");
        }
       // rotate_movebase(req.target.theta);
        move_base_msgs::MoveBaseGoal goal;

        goal.target_pose.header.frame_id = "base_link";
        goal.target_pose.header.stamp = ros::Time::now();

        goal.target_pose.pose.position.x = 0;
        goal.target_pose.pose.position.y = 0;
        //goal.target_pose.pose.orientation=tf::createQuaternionMsgFromRollPitchYaw(0,0,req.target.theta);
        goal.target_pose.pose.orientation=tf::createQuaternionMsgFromRollPitchYaw(0,0,theta);
        //ROS_INFO("Sending goal");
        ac.sendGoal(goal);
        ROS_INFO_STREAM("turn "<<theta);
        ac.waitForResult();

        // if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
        //     ROS_INFO("Hooray, the base moved 1 meter forward");
        // else
        //     ROS_INFO("The base failed to move forward 1 meter for some reason");
        ros::Duration(2).sleep();
        goal.target_pose.pose.position.x = dis;
         goal.target_pose.pose.orientation=tf::createQuaternionMsgFromRollPitchYaw(0,0,0);
        ROS_INFO_STREAM("go ahead "<<dis);
        ac.sendGoal(goal);

        ac.waitForResult();
 ros::Duration(2).sleep();
        // if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
        //     ROS_INFO("Hooray, the base moved 1 meter forward");
        // else
        //     ROS_INFO("The base failed to move forward 1 meter for some reason");
            
    }

    void navigation_integrationClass::move2goal2(std::string frame_id,geometry_msgs::Pose2D target)
    {
        MoveBaseClient ac("move_base", true);
       
        //wait for the action server to come up
        while(!ac.waitForServer(ros::Duration(5.0)))
        {
            ROS_INFO("Waiting for the move_base action server to come up");
        }
       // rotate_movebase(req.target.theta);
        move_base_msgs::MoveBaseGoal goal;

        goal.target_pose.header.frame_id = frame_id;
        goal.target_pose.header.stamp = ros::Time::now();

        goal.target_pose.pose.position.x = target.x;
        goal.target_pose.pose.position.y = target.y;
        //goal.target_pose.pose.orientation=tf::createQuaternionMsgFromRollPitchYaw(0,0,req.target.theta);
        goal.target_pose.pose.orientation=tf::createQuaternionMsgFromRollPitchYaw(0,0,target.theta);
        ROS_INFO("Sending goal");
        ac.sendGoal(goal);

        ac.waitForResult();

        if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
            ROS_INFO("Hooray, the base moved 1 meter forward");
        else
            ROS_INFO("The base failed to move forward 1 meter for some reason");
         ros::Duration(2).sleep();
    }


    void navigation_integrationClass::getTiagoPosecb(const nav_msgs::Odometry::ConstPtr &msg)
    {
        //ROS_INFO_STREAM("tiago_pose based on baselink:"<<msg->pose.pose);
        tiago_current_pose_odom = msg->pose.pose;
        //ROS_INFO_STREAM("tiago_current:"<<tiago_current_pose_odom.orientation);
        double R_,P_,Y_ ;
        current_x = msg->pose.pose.position.x;
        current_y = msg->pose.pose.position.y;
        tf::Quaternion q(tiago_current_pose_odom.orientation.x,tiago_current_pose_odom.orientation.y,tiago_current_pose_odom.orientation.z,tiago_current_pose_odom.orientation.w);
        //tf::Matrix3x3().getRPY(R_,P_,Y_);
        tf::Matrix3x3 m(q);
        m.getRPY(R_,P_,current_theta);
        //ROS_INFO_STREAM("R P Y:"<<R_<<P_<<Y_);
    
    }

    void navigation_integrationClass::rotate_movebase(double angle)
    {
        ros::Publisher pub_rotate = nh_.advertise<geometry_msgs::Twist>("/mobile_base_controller/cmd_vel",100);
        ROS_INFO_STREAM(" rotate movebase : ");
        //ros::Duration(1).sleep();
        ros::Rate r_rotate(10);
        geometry_msgs::Twist initial;
        initial.linear.x = 0.0;initial.linear.y=0.0;initial.linear.z=0.0;
        initial.angular.z = 0.0;initial.angular.z = 0.0;initial.angular.z = 0.785;
        double t;t = fabs(angle)/0.785;
        if (angle < 0)
        {initial.angular.z = -0.785;}
        ros::Time t_totate=ros::Time::now();

         while((ros::Time::now().toSec()-t_totate.toSec())<t)
     {
         
         pub_rotate.publish(initial);
         ros::spinOnce();
         r_rotate.sleep();
     }
      
        ROS_INFO_STREAM(" rotate finished ! ");
    }

    double navigation_integrationClass::move_distance(double x,double y)
    {
        double d,alpha,d_move;
        d = sqrt(pow(x,2)+pow(y,2));
        alpha = 0.5/d;
        d_move = d*(1-alpha);
        return d_move;
        

    }

    bool navigation_integrationClass::movebasecontrolcb(flowerfanggirl::targetpose_2d::Request &req,flowerfanggirl::targetpose_2d::Response &res )
    {
        
        MoveBaseClient ac("move_base", true);
       
        //wait for the action server to come up
        while(!ac.waitForServer(ros::Duration(5.0)))
        {
            ROS_INFO("Waiting for the move_base action server to come up");
        }
       // rotate_movebase(req.target.theta);
        move_base_msgs::MoveBaseGoal goal;

        goal.target_pose.header.frame_id = req.frame_id;
        goal.target_pose.header.stamp = ros::Time::now();

        goal.target_pose.pose.position.x = req.target.x;
        goal.target_pose.pose.position.y = req.target.y;
        //goal.target_pose.pose.orientation=tf::createQuaternionMsgFromRollPitchYaw(0,0,req.target.theta);
        goal.target_pose.pose.orientation=tf::createQuaternionMsgFromRollPitchYaw(0,0,req.target.theta);
        ROS_INFO("Sending goal");
        ac.sendGoal(goal);

        ac.waitForResult();

        if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
            ROS_INFO("Hooray, the base moved 1 meter forward");
        else
            ROS_INFO("The base failed to move forward 1 meter for some reason");
            
        // geometry_msgs::Pose2D targettmp;
        // targettmp = req.target;
        // move2goal_odom(targettmp);
        return true;
    }


    

    
    void navigation_integrationClass::move2goal_odom(geometry_msgs::Pose2D target)
    {
       
        ros::Publisher vel_pub = nh_.advertise<geometry_msgs::Twist>("/mobile_base_controller/cmd_vel",100);
        geometry_msgs::Twist vel_tmp;
        ros::Rate r_(60);
        double target_theta = target.theta+current_theta;
        target_theta = fmod(target_theta,PI);
        ROS_INFO_STREAM("target theta:"<<target_theta);


        //double theta_1 = atan(target.y/target.x)*3.1415926/180;

        //ROS_INFO_STREAM("theta1:"<<theta_1);


        // while(fabs(target_theta-current_theta)>0.01)
        // {
        //     //ROS_INFO_STREAM("delta theta:"<<fabs(theta_1-current_Y));
        //     vel_tmp.angular.z = fabs(target_theta-current_theta);
        //     vel_pub.publish(vel_tmp);
            
        //     r_.sleep();
        //     ros::spinOnce();
        // }
        // double tmp1=current_x;
        // double tmp2=current_y;

        // double dis = sqrt(pow(target.x,2)+pow(target.y,2));
        // while(sqrt(pow(tmp1-current_x,2)+pow(tmp2-current_y,2))>0.01)
        // {
        //     vel_tmp.linear.x = sqrt(pow(tmp1-current_x,2)+pow(tmp2-current_y,2));
        //     vel_pub.publish(vel_tmp);
        //     r_.sleep();
        //     ros::spinOnce();
        // }


    }


    double navigation_integrationClass::return_radiens(double x,double y)
    {
        double theta_radians = atan(y/x);
        if(theta_radians>0)
        {
            if(x<0&&y<0)
            {
                
            }
        }
    }



    void navigation_integrationClass::rotate_and_go(double angular,double dis)
    {
        ros::Publisher pub_rotate = nh_.advertise<geometry_msgs::Twist>("/mobile_base_controller/cmd_vel",100);
        ROS_INFO_STREAM(" rotate movebase : ");
        //ros::Duration(1).sleep();
        ros::Rate r_rotate(10);
        geometry_msgs::Twist initial;
        initial.linear.x = 0.0;initial.linear.y=0.0;initial.linear.z=0.0;
        initial.angular.z = 0.0;initial.angular.z = 0.0;initial.angular.z = 0.5;
        double t;t = fabs(angular)/0.5;
        if (angular < 0)
        {initial.angular.z = -0.785;}
        ros::Time t_totate=ros::Time::now();

         while((ros::Time::now().toSec()-t_totate.toSec())<t)
     {
         
         pub_rotate.publish(initial);
         ros::spinOnce();
         r_rotate.sleep();
     }

    initial.linear.x = 0.2;initial.linear.y=0.0;initial.linear.z=0.0;
        initial.angular.z = 0.0;initial.angular.z = 0.0;initial.angular.z = 0.0;
t=fabs(dis)/0.2;
ros::Time t_goahead=ros::Time::now();

         while((ros::Time::now().toSec()-t_goahead.toSec())<t)
     {
         
         pub_rotate.publish(initial);
         ros::spinOnce();
         r_rotate.sleep();
     }
      
        ROS_INFO_STREAM(" rotate and go ahead finished ! ");
    }

   
}