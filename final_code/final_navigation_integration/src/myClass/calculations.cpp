#include <final_navigation_integration/final_nav_integration.h>
namespace final_navigation_integration
{
    flowerfanggirl::ObjectInfo navigation_integrationClass::get_nearestobj(flowerfanggirl::ObjectInfoArray inputarray)
    {
        flowerfanggirl::ObjectInfo output;
        int tar_idx = 0;
        double min_dis = 20;
        for(int i=0;i<inputarray.allObjects.size();i++)
        {
            double dis_tmp = calculate_2ddistance(inputarray.allObjects[i].position);
            if(dis_tmp<min_dis)
            {
                tar_idx = i;
                min_dis = dis_tmp;
            }
        }
        output = inputarray.allObjects[tar_idx];

        return output;
    }
    
    double navigation_integrationClass::calculate_2ddistance(geometry_msgs::Point input)
    {
        double dis = sqrt(input.x*input.x + input.y*input.y);
        return dis;
    }

    pose_2d navigation_integrationClass::get_ref_position(geometry_msgs::Point input)
    {
        pose_2d output;

        return output;
    }


    
}