#include <final_navigation_integration/final_nav_integration.h>
namespace final_navigation_integration
{
    void navigation_integrationClass::initialization()
    {
        goal_objects.target_pose.header.frame_id="map";
        goal_objects.target_pose.pose.position.x = 2.193;
        goal_objects.target_pose.pose.position.y = 0.0;
        goal_objects.target_pose.pose.position.z = 0.0;
        goal_objects.target_pose.pose.orientation.x = 0.0;
        goal_objects.target_pose.pose.orientation.y = 0.0;
        goal_objects.target_pose.pose.orientation.z = -0.845;
        goal_objects.target_pose.pose.orientation.w = 0.535; 

        goal_kitchen.target_pose.header.frame_id="map";
        goal_kitchen.target_pose.pose.position.x = 1.858;
        goal_kitchen.target_pose.pose.position.y = 0.858;
        goal_kitchen.target_pose.pose.position.z = 0.0;
        goal_kitchen.target_pose.pose.orientation.x = 0.0;
        goal_kitchen.target_pose.pose.orientation.y = 0.0;
        goal_kitchen.target_pose.pose.orientation.z = -0.246;
        goal_kitchen.target_pose.pose.orientation.w = 0.969;

        goal_trash.target_pose.header.frame_id="map";
        goal_trash.target_pose.pose.position.x = 3.208;
        goal_trash.target_pose.pose.position.y = 0.124;
        goal_trash.target_pose.pose.position.z = 0.0;
        goal_trash.target_pose.pose.orientation.x = 0.0;
        goal_trash.target_pose.pose.orientation.y = 0.0;
        goal_trash.target_pose.pose.orientation.z = -0.243;
        goal_trash.target_pose.pose.orientation.w = 0.970;
        

    }
    
}