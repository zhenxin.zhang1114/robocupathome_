#include <final_navigation_integration/final_nav_integration.h>
namespace final_navigation_integration
{
    void navigation_integrationClass::speak(std::string text)
    {


        ros::Publisher text_pub = nh_.advertise<pal_interaction_msgs::TtsActionGoal>("/tts/goal",100);
        //ROS_INFO_STREAM(" text to speech : ");
        ros::Rate r_text(10);
        ros::Time t_torso=ros::Time::now();

        pal_interaction_msgs::TtsActionGoal Text;
        Text.goal.rawtext.text = text;
        Text.goal.rawtext.lang_id.push_back('en_GB');
         
        //while((ros::Time::now().toSec()-t_torso.toSec())<3)
        //{        
            text_pub.publish(Text);
            //ros::spinOnce();
            //r_text.sleep();
        //}
        // ROS_INFO_STREAM(" text to speech finished ! ");
        // ROS_INFO_STREAM(" raw_text is: "<<Text.goal.rawtext.text);
        
    }

    void navigation_integrationClass::voice_cb(const std_msgs::String::ConstPtr &msg)
    {
        //ROS_INFO_STREAM("I heared "<<msg->data);

        current_voice1 = msg->data;
        ROS_INFO_STREAM("current voice 1: "<<current_voice1);
    }
}