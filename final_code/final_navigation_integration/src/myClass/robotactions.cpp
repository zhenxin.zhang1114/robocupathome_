#include <final_navigation_integration/final_nav_integration.h>
namespace final_navigation_integration
{
    void navigation_integrationClass::low_head()
    {
        ROS_INFO_STREAM("head_down");
        ros::Duration(3).sleep();
        ros::Rate r(10);
        ros::Time t2=ros::Time::now();
        while((ros::Time::now().toSec()-t2.toSec())<7)
        {
            trajectory_msgs::JointTrajectory Head_move;
            trajectory_msgs::JointTrajectoryPoint Head_set;

            Head_move.joint_names.push_back("head_1_joint");
            Head_move.joint_names.push_back( "head_2_joint");

            Head_set.positions.push_back(0.0);
            Head_set.positions.push_back(-0.3);
            Head_set.time_from_start = ros::Duration(2);
            Head_move.points.push_back(Head_set);

            pub_head.publish(Head_move);
            ros::spinOnce();
            r.sleep();
        }
    }

    void navigation_integrationClass::low_torso()
    {
        ros::Publisher pub_torso_down = nh_.advertise<trajectory_msgs::JointTrajectory>("/torso_controller/command",100);
        ROS_INFO_STREAM("torso down");
        ros::Duration(3).sleep();
        ros::Rate r_torso(10);

        ros::Time t_torso=ros::Time::now();
        while((ros::Time::now().toSec()-t_torso.toSec())<7)
        {
            trajectory_msgs::JointTrajectory Torso_down;
            trajectory_msgs::JointTrajectoryPoint Torso_set;

            Torso_down.joint_names.push_back("torso_lift_joint");
            Torso_set.positions.push_back(0.05);
            

            Torso_set.time_from_start = ros::Duration(2);
            Torso_down.points.push_back(Torso_set);

            pub_torso_down.publish(Torso_down);
            ros::spinOnce();
            r_torso.sleep();
        }
    }

    void navigation_integrationClass::move_head(double head_1_joint_value,double head_2_joint_value)
    {

        // head2joint value between -0.98 0.79
        ros::Duration(3).sleep();
        ros::Rate r(10);
        ros::Time t2=ros::Time::now();
        while((ros::Time::now().toSec()-t2.toSec())<7)
        {
            trajectory_msgs::JointTrajectory Head_move;
            trajectory_msgs::JointTrajectoryPoint Head_set;

            Head_move.joint_names.push_back("head_1_joint");
            Head_move.joint_names.push_back( "head_2_joint");

            Head_set.positions.push_back(head_1_joint_value);
            Head_set.positions.push_back(head_2_joint_value);
            Head_set.time_from_start = ros::Duration(2);
            Head_move.points.push_back(Head_set);

            pub_head.publish(Head_move);
            ros::spinOnce();
            r.sleep();
        }
    }
    
}