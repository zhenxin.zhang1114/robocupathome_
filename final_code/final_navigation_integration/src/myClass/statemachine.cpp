#include <final_navigation_integration/final_nav_integration.h>
namespace final_navigation_integration
{
    void navigation_integrationClass::statemachine()
    {
        robot_state = gotoobjstack;
        ROS_INFO_STREAM("in state machine ");
        int grasp_idx=0;
        while(true)
        {
            switch(robot_state)
            {
                case gotoobjstack:
                {

                  ROS_INFO_STREAM("in goto stack");
                  speak("Im moving to objects");
                  ros::Duration(2).sleep();
                    move2goal(goal_objects);
                    ros::Duration(4).sleep();
                    if(grasp_idx==4){
                      speak("I finished my work!");
                      ros::Duration(3).sleep();
                      std::string stopstring;
                      std::cin>>stopstring;
                    }
                    robot_state = objdetection;
                    

                    
                }
                break;
                case objdetection:
                {
                  ROS_INFO_STREAM("in objdetection");
                  speak("I will recognize objects");
                  ros::Duration(3);
                  move_head(0,-0.98);//low head
                  ros::Duration(20).sleep();
                  allobjinfo.request.require = true;
                  objRecognitionClient.call(allobjinfo);
                  allobjinfoarray = allobjinfo.response.allobj;
                  if(allobjinfoarray.allObjects.size()>0)
                  {
                    speak("I detected");
                    ros::Duration(1).sleep();
                    for(int i=0;i<allobjinfoarray.allObjects.size();i++)
                    {
                  ROS_INFO_STREAM("detected following objects:"<<allobjinfoarray.allObjects[i].classname);
                  speak(allobjinfoarray.allObjects[i].classname);
                                      ros::Duration(1).sleep();

                    }
                  }
                  ros::Duration(5).sleep();

                  robot_state = selectobj;
                }

                break;

                case selectobj:
                {

                  speak("Im selecting object ");
                  ros::Duration(1).sleep();
                  
                  flowerfanggirl::ObjectInfo selected_obj;
                  std::string sel_obj;
                  if(allobjinfoarray.allObjects.size()>1)
                  {
                     selected_obj = get_nearestobj(allobjinfoarray);
                  }
                  else
                  {
                    selected_obj = allobjinfoarray.allObjects[0];
                  }
                  sel_obj = selected_obj.classname;
                  std::cerr << "selected object: " << sel_obj << std::endl;
                  if(selected_obj.position.x>0.63|| fabs(selected_obj.position.y)>0.23)
                  {
                      double theta_ = atan(selected_obj.position.y/selected_obj.position.x);
                    ROS_INFO_STREAM("y:"<<selected_obj.position.y<<"x "<<selected_obj.position.x  <<"theta_:  "<<theta_);
                    double dis = sqrt(pow(selected_obj.position.x,2)+pow(selected_obj.position.y,2));
                    //rotate_movebase(theta_);
                    //double theta_rad = theta_*3.1415926/180;
                    if(dis>=0.6)
                    {
                      dis=dis-0.6;
                    }
                    else
                    {
                      dis=0;
                    }

                    //speak(std::to_string(theta_));
                    //ros::Duration(2).sleep();
                    std::string judge1;
                    ROS_INFO_STREAM("theta_:"<<theta_<<"dis:"<<dis);
                    // std::cin>>judge1;
                    // if(judge1=="yes")
                    // {
                      rotate_and_go(theta_,dis);
                      // rotate_and_go(-theta_,0);
                    // }
                    rotation_srv.request.angle = theta_;
                  forward_srv.request.distance = dis;

                  // movebase_rotate.call(rotation_srv);

                  // ros::Duration(2).sleep();
                  // movebase_forward.call(forward_srv);
                  // ros::Duration(2).sleep();


                    

                    ros::Duration(20).sleep();
                    allobjinfo1.request.require = true;
                    objRecognitionClient.call(allobjinfo1);
                    allobjinfoarray1 = allobjinfo1.response.allobj;
                    int idx;
                    float miny=10;
                    float min = 10;

                    if(allobjinfoarray1.allObjects.size()>0)
                    {
                      speak("I detected");
                      ros::Duration(1).sleep();
                      for(int i=0;i<allobjinfoarray1.allObjects.size();i++)
                      {
                        ROS_INFO_STREAM("detected following objects:"<<allobjinfoarray1.allObjects[i].classname);
                        speak(allobjinfoarray1.allObjects[i].classname);
                        ROS_INFO_STREAM("position:"<<allobjinfoarray1.allObjects[i].position.x<<" "<<allobjinfoarray1.allObjects[i].position.y);
                        ros::Duration(1).sleep();

                      }
                    }
                    int index_obj;
                    int idx_tmp;
                      double mindis_tmp=10;
                    for(int i=0;i<allobjinfoarray1.allObjects.size();i++)
                    {
                      if ( allobjinfoarray1.allObjects[i].classname == sel_obj)
                      {
                        index_obj = i;
                      }
                      
                      // if(pow(allobjinfoarray1.allObjects[i].position.y,2)<mindis_tmp)
                      // {
                      //   mindis_tmp = pow(allobjinfoarray1.allObjects[i].position.y,2);
                      //   idx_tmp=i;
                      // }
                    }
                  //index_obj = idx_tmp;

                    target_tmp = allobjinfoarray1.allObjects[index_obj];
                  }
                  else
                  {
                    target_tmp = selected_obj;
                  }
                  
                  speak("I will pick up ");
                  ros::Duration(1.5).sleep();
                  speak(target_tmp.classname);
                  ros::Duration(1).sleep();
                  std::string judge;
                  if(target_tmp.classname == "cola can")
                  {
                    grasp_tar.request.position.x = target_tmp.position.x+0.01;
                    grasp_tar.request.position.y = target_tmp.position.y-0.015;
                    grasp_tar.request.position.z = 0.4;
                    ROS_INFO_STREAM("cola can");
                    ROS_INFO_STREAM("x:"<<grasp_tar.request.position.x<<"   y:"<<grasp_tar.request.position.y);
                  
                    grasp_tar.request.RPY.x = 1.57;
                    grasp_tar.request.RPY.y = 1.57;
                    grasp_tar.request.RPY.z = 1.57;
                  }
                  else if(target_tmp.classname == "peach")
                  {
                    grasp_tar.request.position.x = target_tmp.position.x+0.025;
                    grasp_tar.request.position.y = target_tmp.position.y;
                    grasp_tar.request.position.z = 0.31;
                    ROS_INFO_STREAM("peach");
                    ROS_INFO_STREAM("x:"<<grasp_tar.request.position.x<<"   y:"<<grasp_tar.request.position.y);
                    
                    grasp_tar.request.RPY.x = 1.57;
                    grasp_tar.request.RPY.y = 1.57;
                    grasp_tar.request.RPY.z = 1.57;
                    
                  }
                  else
                  {

                    grasp_tar.request.position.x = target_tmp.position.x+0.025;
                    grasp_tar.request.position.y = target_tmp.position.y;
                    grasp_tar.request.position.z = 0.31;
                    ROS_INFO_STREAM("x:"<<grasp_tar.request.position.x<<"   y:"<<grasp_tar.request.position.y);
                    grasp_tar.request.RPY.x = 1.57;
                    grasp_tar.request.RPY.y = 1.57;
                    grasp_tar.request.RPY.z = target_tmp.RPY.z;
                  }
                  
                  
                  
                  


                  //manipulationClient.call(grasp_tar);


                  

                  
                  
                  //robot_state = graspobj;
                  //std::cin>>judge;
                  if(grasp_tar.request.position.x<0.65&&fabs(grasp_tar.request.position.y)<0.25)
                  {
                    judge="yes";
                  }
                  else
                  {
                    judge="no";
                  }
                  if(judge=="yes")
                  {
                    speak("I can pick up objects in this distance");
                    ros::Duration(4).sleep();
                    robot_state = graspobj;
                  }
                  else
                  {
                    speak("sorry,I can't grasp object in this distance ");
                    ros::Duration(3).sleep();
                    robot_state = gotoobjstack;
                  }
                  
                }
                                break;

              
                case graspobj:
                {
                  ROS_INFO_STREAM("graspobj");
                  speak("Im grasping object");
                  ros::Duration(2).sleep();
                    manipulationClient.call(grasp_tar);
                    grasp_idx= grasp_idx+1;
                  robot_state = judgeobj;
                }
                                break;



                                

                case judgeobj:
                {
 //ROS_INFO_STREAM("judgeobj");

                  if(target_tmp.classname=="cola can")
                  {
                    speak(" I picked up cola can, is it trash?");
                    ros::Duration(2);
                    // std::string judge2;
                    // std::cin>>judge2;

                    current_voice1 ="novoice";
                    while(current_voice1=="novoice")
                    {

                    } 
                    if(current_voice1=="yes")
                    {
                      robot_state = gototrash;
                    }
                    else{
                      robot_state = gotokitchen;
                    }
                    // if(judge2=="yes")
                    // {
                    //   robot_state = gototrash;
                    // }
                    // else{
                    //   robot_state = gotokitchen;
                    // }
                    
                  }
                  else
                  {
                      //ask human
                    robot_state = gotokitchen;
                  }
                    
                }
                                break;

                case gotokitchen:
                {
                   ROS_INFO_STREAM("gotokitchen");
                   speak("I'm moving to Kitchen");
                   ros::Duration(2).sleep();
                   move2goal(goal_kitchen);
                   ros::Duration(4).sleep();
                   speak("I arrived at kitchen");
                   ros::Duration(2).sleep();
                  ROS_INFO_STREAM("move to kitchen finish");
                  robot_state = putobj1;
                  
                }
                                break;

                case gototrash:
                {
                  ROS_INFO_STREAM("move to trash");
                  speak("I'm moving to trash");
                  move2goal(goal_trash);
                  ROS_INFO_STREAM("move to trash finish");
                  speak("I arrived at trash");
                  robot_state = putobj2;
                }
                                break;

                case putobj1:
                {
                  grasp_tar_put.request.position.x = 0.8;
                  grasp_tar_put.request.position.y = 0;
                  grasp_tar_put.request.position.z = 0.8;
                  grasp_tar_put.request.RPY.x=0;
                  grasp_tar_put.request.RPY.y = 0;
                  grasp_tar_put.request.RPY.z = 0;
                  putClient.call(grasp_tar_put);
                  
                  robot_state = gotoobjstack;
                }
                                break;

                case putobj2:
                {

                  throw_tar.request.position.x = 0.6;
                  throw_tar.request.position.y = 0.0;
                  throw_tar.request.position.z = 0.55;
                  throw_tar.request.RPY.x = 0.0;
                  throw_tar.request.RPY.y = 0.0;
                  throw_tar.request.RPY.z = 0.0;
                  throwClient.call(throw_tar);
                  robot_state = gotoobjstack;
                }
                                break;



            }
        }

    }















    void navigation_integrationClass::stateMachine(){
    tiago_state = navigating_table1;
    while (true)
    {
      
    switch (tiago_state)
    {
      /*** 
    case navigating_table1:
      ROS_INFO_STREAM("localizing");
      ros::Duration(3).sleep();
      loc_srv.request.start = true;
      localizationClient.call(loc_srv);
      if(loc_srv.response.finish)
      {
        ROS_INFO_STREAM("localization finish,now object recognition");
        tiago_state = navigating_table1;
        
      }
      else
      {
        tiago_state = localizing;
        break;
      }
      ***/
    case navigating_table1:
    {
      ROS_INFO_STREAM("navigating to table 1");
      ros::Duration(3).sleep();
      goal_table1.target_pose.header.frame_id = "map";
      goal_table1.target_pose.header.stamp = ros::Time::now();
      /*table 1 in real world*/
      goal_table1.target_pose.pose.position.x = -1.480;
      goal_table1.target_pose.pose.position.y = 0.879;
      goal_table1.target_pose.pose.orientation.z = 0.515;
      goal_table1.target_pose.pose.orientation.w = 0.857;
      /*table 1 in gazebo*/
  //    goal_table1.target_pose.pose.position.x = 2.8;
  //    goal_table1.target_pose.pose.position.y = 0.0;
  //    goal_table1.target_pose.pose.orientation.z = 0.71;
  //    goal_table1.target_pose.pose.orientation.w = 0.7;
      move2goal(goal_table1);
      tiago_state = head_down;
    }
    break;


    // head down.
    case head_down:
    {

      low_head();
      tiago_state = objDetecting;
    }
    break;
      
    case objDetecting:
    {
      ROS_INFO_STREAM("objDetecting");
      ros::Duration(3).sleep();
      objRec_srv.request.start = true;
      objRecognitionClient.call(objRec_srv);
      bool flag;
      int idx;
      flag = false;
      //target_class = "cup";
      if(objRec_srv.response.finish == true && objRec_srv.response.objectInfo.size()>0){
        ROS_INFO_STREAM("Detected the following objects with id:");
        for(int k=0;k<objRec_srv.response.objectInfo.size();k++){
          allObjectInfo.push_back(objRec_srv.response.objectInfo[k]);
          allObjectId.push_back(k);
          std::cout<<objRec_srv.response.objectInfo[k].Class<<", Id:"<<k<<std::endl;
        }
        ROS_INFO_STREAM("Please input the Id of the object that you want to grip:");

        
        std::cin>>target_class_Id;
        while(1){  
          for(int j=0;j<allObjectId.size();j++){
            if(atoi(target_class_Id.c_str()) == j){
              idx=j;
              flag = true;
            }
          }
          if(flag){
            flag = false;
            break;
          }
          else{
            ROS_ERROR("the input id don't matched to the detected objects,please input once more!");
            std::cin>>target_class_Id;
          }
        }
        ROS_INFO_STREAM("You want to know the position of "<< allObjectInfo[idx].Class<<" with Id:"<<idx<<"!");
        ROS_INFO_STREAM("The 3d coordinate of "<<allObjectInfo[idx].Class<<" with id " << idx<<"is: "<<allObjectInfo[idx].x<<","<<allObjectInfo[idx].y<<","<<allObjectInfo[idx].z);
        manipulation_srv.request.x = allObjectInfo[idx].x;
        manipulation_srv.request.y = allObjectInfo[idx].y;
        manipulation_srv.request.z = allObjectInfo[idx].z;
        manipulation_srv.request.start = true;
        manipulation_srv.request.action = "grasp";
        tiago_state = grip_obj;
        break;
      }
      else{
        tiago_state = objDetecting;
      }
    }
    break;
    
    case grip_obj:
    {
      ROS_INFO_STREAM("grip_obj");
      ros::Duration(3).sleep();
      manipulationClient.call(manipulation_srv);
      if(manipulation_srv.response.finish){
        tiago_state = navigating_table2;
        break;
      }
      else{
        tiago_state = grip_obj;
      }
      //tiago_state = navigating_table2;
    }
    break;
    case navigating_table2:
    {
      ROS_INFO_STREAM("navigating_table2");
      ros::Duration(3).sleep();
      goal_table2.target_pose.header.frame_id = "map";
      goal_table2.target_pose.header.stamp = ros::Time::now();
      /*table 2 in real world*/
      goal_table2.target_pose.pose.position.x = -2.335;
      goal_table2.target_pose.pose.position.y = 1.438;
      goal_table2.target_pose.pose.orientation.z = 0.519;
      goal_table2.target_pose.pose.orientation.w = 0.854;
      /*table 2 in gazebo*/
  //    goal_table2.target_pose.pose.position.x = 2.8;
  //    goal_table2.target_pose.pose.position.y = -0.2;
  //    goal_table2.target_pose.pose.orientation.z = -0.71;
  //    goal_table2.target_pose.pose.orientation.w = 0.70;
      move2goal(goal_table2);
      tiago_state = put_obj;
    }
    break;
    case put_obj:
    {
      ROS_INFO_STREAM("put_obj");
      ros::Duration(3).sleep();
      manipulation_srv.request.start = true;
      manipulation_srv.response.finish=false;
      manipulation_srv.request.action = "put";
      manipulation_srv.request.x = 1.4;
      manipulation_srv.request.y = -0.056;
      manipulation_srv.request.z = 0.698;
  //    manipulation_srv.request.x = 2.8;
  //    manipulation_srv.request.y = -1.2;
  //    manipulation_srv.request.z = 1.5;
      manipulationClient.call(manipulation_srv);
      if(manipulation_srv.response.finish){
        //tiago_state = navigating_initial;
        break;
      }
      else{
        tiago_state = put_obj;
      }
      //tiago_state = navigating_initial;
    }
    break;
        
    case navigating_initial: 
    {
      ROS_INFO_STREAM("navigating_initial");
      ros::Duration(3).sleep();
      goal_initial.target_pose.header.frame_id = "map";
      goal_initial.target_pose.header.stamp = ros::Time::now();
      goal_initial.target_pose.pose.position.x = -0;
      goal_initial.target_pose.pose.position.y = -0;
      goal_initial.target_pose.pose.orientation.z = -0.2;
      goal_initial.target_pose.pose.orientation.w = 0.9;
      //move2goal(goal_initial);
    }
    break;

    default:
    {
    }
    break;
    }
    }
    

  }
}