#include <final_navigation_integration/final_nav_integration.h>//include the headfile
namespace final_navigation_integration
{
  
   
  void navigation_integrationClass::localization_()
  {
    rotate_vel.angular.z=1;
    //rotate_vel.linear.x=0;

    localModeSrv.request.input = loc_input;
    localMapSrv.request.input = map_input;
    localmodeClient.call(localModeSrv);
    localMapClient.call(localMapSrv);
    global_localization_client.call(empty_srv);
    //std::vector<std::make_pair(flowerfanggirl::ObjectRecognition,int)> allobj_info;
    
    
    ros::Time t_start=ros::Time::now();
    ros::Rate rate_(50);
    while(loc_acc>0.015){
    //while((ros::Time::now().toSec()-t_start.toSec())<10.0){
      
      localization_pub.publish(rotate_vel);
    //ROS_INFO_STREAM("loc acc:"<<loc_acc);
      // ROS_INFO_STREAM("localizing!");
      // ROS_INFO_STREAM("accuracy:"<<loc_acc);
      ros::spinOnce();
      rate_.sleep();
    }
    clear_costmaps_client.call(empty_srv);
    ros::Duration(3).sleep();
    ROS_INFO_STREAM("localization finished!");
      
  }
  

  void navigation_integrationClass::getLocCov(const geometry_msgs::PoseWithCovarianceStampedConstPtr &msg){
    loc_acc = msg->pose.covariance[0]+msg->pose.covariance[7];
   // ROS_INFO_STREAM("loc_acc"<<loc_acc);
  }
  

  void navigation_integrationClass::localization1()
  {
    global_localization_client.call(empty_srv);
      ROS_INFO_STREAM("localization!!!!!!");
         localization_();
      clear_costmaps_client.call(empty_srv);
  }
}