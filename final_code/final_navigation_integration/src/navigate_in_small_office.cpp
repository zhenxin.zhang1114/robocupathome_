#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <iostream>
#include <fstream>
#include <pthread.h>
#include <std_msgs/String.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <tf_conversions/tf_eigen.h>
#include <nav_msgs/Odometry.h>
#include <std_srvs/Empty.h>
#include <flowerfanggirl/Localization.h>


class Localization{
private:
  ros::ServiceClient global_localization_client=n.serviceClient<std_srvs::Empty>("/global_localization");//client will call service "/global_localization"
  ros::ServiceClient clear_costmaps_client=n.serviceClient<std_srvs::Empty>("/move_base/clear_costmaps");//client will call service "/move_base/clear_costmaps"
  ros::Publisher localization_pub=n.advertise<geometry_msgs::Twist>("/mobile_base_controller/cmd_vel",100);

public:



};



int main(int argc, char** argv){
  ros::init(argc, argv, "localization");

  ros::NodeHandle n;//build a nodehandle
  ros::Rate r(60);

  ros::ServiceClient global_localization_client=n.serviceClient<std_srvs::Empty>("/global_localization");//client will call service "/global_localization"
  ros::ServiceClient clear_costmaps_client=n.serviceClient<std_srvs::Empty>("/move_base/clear_costmaps");//client will call service "/move_base/clear_costmaps"
  ros::Publisher localization_pub=n.advertise<geometry_msgs::Twist>("/mobile_base_controller/cmd_vel",100);


  ros::ServiceServer localization_server = n.advertiseService("/tutorial5/localization",localizationsrv_cb);
  std_srvs::Empty empty_srv;
  geometry_msgs::Twist rotate_vel;
  rotate_vel.angular.z=1;
  return 0;
}



bool localizationsrv_cb(flowerfanggirl::Localization::Request &req,
                        flowerfanggirl::Localization::Response &res)
{
  if(req.start==true){

  }
}