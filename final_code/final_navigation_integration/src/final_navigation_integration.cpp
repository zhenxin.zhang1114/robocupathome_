#include <final_navigation_integration/final_nav_integration.h>


/*********************************************************************
 * CUSTOM CLASS
 * ******************************************************************/

int main( int argc, char** argv )
{

    ros::init(argc, argv, "navigation_integration",ros::init_options::AnonymousName);//initial node name, AnonymousName can

    ROS_INFO_STREAM("**tutorial 5 initial");
ros::NodeHandle n;//build a nodehandle

     ros::AsyncSpinner spinner(4); // Use 4 threads
    spinner.start();
    final_navigation_integration::navigation_integrationClass node(n);
    ros::waitForShutdown();

    
    
   

    return 0;
}
