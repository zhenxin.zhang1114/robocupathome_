#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <std_msgs/Bool.h>

#include <nav_msgs/Odometry.h>
//include srv header
#include <vector>
#include <math.h>
#include <flowerfanggirl/forward.h>
#include <flowerfanggirl/rotation.h>
using namespace std;

class Controller
{
private:
  ros::NodeHandle nh_, priv_nh_;
  ros::Rate r();
  ros::Publisher vel_pub;
  ros::Subscriber odom_sub;
  ros::ServiceServer forward_srv, rotation_test;

  geometry_msgs::Twist vel, vel_msg;
  geometry_msgs::Pose odom_pose, origin_pose;
  float z;
  int x, y, x_, y_;
  double phi, goal_theta, goal_theta_, deta_theta, sin_phi2, dis;
  string team_color;
  double a, b, x_t, y_t, theta_t;
  //------------------callback function---------------------//
  void odom_callback(const nav_msgs::OdometryConstPtr& odom);
  bool forward_callback(flowerfanggirl::forward::Request &req, flowerfanggirl::forward::Response &res);
  bool rotation_test_callback(flowerfanggirl::rotation::Request &req, flowerfanggirl::rotation::Response &res);
public:
  Controller(ros::NodeHandle nh) : nh_(nh), priv_nh_("~")
  {
    odom_sub = nh_.subscribe("/mobile_base_controller/odom", 1, &Controller::odom_callback, this);
    vel_pub = nh_.advertise<geometry_msgs::Twist>("/mobile_base_controller/cmd_vel", 1);//turtle1/cmd_vel
    forward_srv =nh_.advertiseService("/final/forward", &Controller::forward_callback, this);
    rotation_test = nh_.advertiseService("/final/rotation", &Controller::rotation_test_callback, this);
  }
  ~Controller(){}
};


bool Controller::rotation_test_callback(flowerfanggirl::rotation::Request &req, flowerfanggirl::rotation::Response &res)
{

    deta_theta = req.angle;
    goal_theta = phi + deta_theta;
    ros::Rate rate(20);
    double v_max;
    v_max = 0.5;


    if(goal_theta > 3.1416)
    {
        goal_theta_ = goal_theta - (2 * 3.1416);
    }
    else if(goal_theta < - 3.1416)
    {
        goal_theta_ = goal_theta + (2 * 3.1416);
    }
    else {
        goal_theta_ = goal_theta;
    }

    if(goal_theta > 3.1416 || goal_theta < - 3.1416)
    {
        while ( fabs(goal_theta_ - phi) >= 0.3)
        {
            if(goal_theta > 3.1416)
            {
                vel_msg.angular.z = v_max;
            }
            else if(goal_theta < - 3.1416)
            {
                vel_msg.angular.z = -v_max;

            }
            vel_pub.publish(vel_msg);
            //ROS_INFO_STREAM(" phi : " << (goal_theta_ - phi) << ",  " << phi << "goal :" << goal_theta);
            rate.sleep();
            ros::spinOnce();

        }
        while (fabs(goal_theta_ - phi) > 0.0)
        {

            vel_msg.angular.z = (goal_theta_ - phi)/fabs(goal_theta_ - phi) * max(0.2, min(2 * sin(fabs(goal_theta_-phi)), v_max));
            vel_pub.publish(vel_msg);
            //ROS_INFO_STREAM(" z: " << vel_msg.angular.z);
            //ROS_INFO_STREAM(" phi : " << (goal_theta_ - phi) << ",  " << phi);
            rate.sleep();
            ros::spinOnce();
        }
    }
    else {
        while (fabs(goal_theta_ - phi) > 0.04)
        {
            if(fabs(goal_theta_ - phi) > 0.3)
            {
                vel_msg.angular.z = (goal_theta_ - phi) / fabs(goal_theta_ - phi) * v_max;
            }
            else {
                vel_msg.angular.z = (goal_theta_ - phi)/fabs(goal_theta_ - phi) * max(0.2, min(2 * sin(fabs(goal_theta_-phi)), v_max));
            }
            vel_pub.publish(vel_msg);
            //ROS_INFO_STREAM(" phi : " << phi);
            //ROS_INFO_STREAM(" z: " << vel_msg.angular.z);
            //ROS_INFO_STREAM(" phi : " << ((goal_theta_ / 3.1416 * 180) - req.angle) << ",  " << phi);
            rate.sleep();
            ros::spinOnce();
        }
    }
/*
    int k = 0;
    while (k < 1) {
        vel_msg.angular.z = 0;
        vel_pub.publish(vel_msg);
        k++;
        rate.sleep();
    }*/
    vel_msg.angular.z = 0;
    vel_pub.publish(vel_msg);
    res.done = true;
    return true;

}

void Controller::odom_callback(const nav_msgs::OdometryConstPtr& odom)
{
    sin_phi2 = odom->pose.pose.orientation.z;
    phi = 2 * asin(sin_phi2);//   -pi, +pi
    odom_pose.position.x = odom->pose.pose.position.x;
    odom_pose.position.y = odom->pose.pose.position.y;
    odom_pose.orientation.z = odom->pose.pose.orientation.z;
    odom_pose.orientation.w = odom->pose.pose.orientation.w;

}

bool Controller::forward_callback(flowerfanggirl::forward::Request &req, flowerfanggirl::forward::Response &res)
{
    ros::Rate rate(10);
    vel.angular.z = 0;
    vel.linear.x = 0;
    double way = 0;
    origin_pose = odom_pose;
    way = sqrt( pow(origin_pose.position.x - odom_pose.position.x, 2) + pow(origin_pose.position.y - odom_pose.position.y, 2));
    ros::spinOnce();
    while( fabs(req.distance)-way >= 0.008)
    {

        if ( fabs(req.distance)-way > 0.2)
        {
            vel.linear.x = min(0.2, way + 0.1);
        }
        else
        {
            vel.linear.x = max(0.0001,  min(0.2, (fabs(req.distance)-way)*2)) ;

        }
        if(req.distance < 0)
        {
            vel.linear.x = - vel.linear.x;
        }

        vel_pub.publish(vel);
        rate.sleep();
        ros::spinOnce();
        way = sqrt( pow(origin_pose.position.x - odom_pose.position.x, 2) + pow(origin_pose.position.y - odom_pose.position.y, 2));

    }
    if ( fabs(req.distance)-way <= 0.01)
    {
        res.done = true;

    }
    else{
        res.done = false;
    }
    //ROS_INFO_STREAM("way = " << way);

    return true;
}

int main(int argc, char**argv)
{
  ros::init(argc, argv, "controller");
  //ros::AsyncSpinner spinner(3);
  //spinner.start();
  ros::NodeHandle nh;
  Controller node(nh);
  //ros::waitForShutdown();

  ros::spin();
  return 0;
}
