#ifndef OBJECTPOSE_H
#define OBJECTPOSE_H
#include <ros/ros.h>
#include <ros/console.h>
#include <std_msgs/Char.h>
#include <math.h>

#include <Eigen/Dense>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <vector>
#include <thread>


#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>


#include <sensor_msgs/Image.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>


// PCL specific includes
#include <pcl_ros/point_cloud.h> // enable pcl publishing
#include <pcl_ros/transforms.h>
#include <pcl_conversions/pcl_conversions.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/point_cloud.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/common/centroid.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/features/moment_of_inertia_estimation.h>
#include <pcl/visualization/cloud_viewer.h>

#include <image_geometry/pinhole_camera_model.h>

// Visualization
#include <visualization_msgs/MarkerArray.h>
#include <visualization_msgs/Marker.h>

#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Vector3.h>
#include <darknet_ros_msgs/BoundingBoxes.h>
#include <darknet_ros_msgs/BoundingBox.h>

// Self-defined msgs and srvs.
#include <flowerfanggirl/ObjectRecognition.h>
#include <flowerfanggirl/ObjectRecognitionArray.h>
#include <flowerfanggirl/ObjectInfo.h>
#include <flowerfanggirl/ObjectInfoArray.h>
#include <flowerfanggirl/AllObjInfoObtain.h>


using namespace std;
using namespace cv;

typedef pcl::PointXYZRGB PointT;
const double PI = 3.14159265359;

class ObjectPose
{
private:
    //! The node handle
    ros::NodeHandle nh_;
    //! image_transport
    // image_transport::ImageTransport it_
    //! Node handle in the private namespace
    // ros::NodeHandle priv_nh_;

    //! Subscribers to the PointCloud data
    // Optional: MESSAGE FILTERS COULD BE A GOOD IDEA FOR GRABBING MULTIPLE TOPICS SYNCRONIZED, NOT NEEDED THOUGH
    ros::Subscriber pc_sub;
    //! Image Subcriber
    image_transport::Subscriber image_sub;
    //! Subcriber for boundingbox
    ros::Subscriber boundingbox_sub;

    //! Publishers for pointclouds
    ros::Publisher pub_plane_pc_;
    ros::Publisher pub_clusters_pc_;

    //! Publisher for visualization_msgs.
    ros::Publisher vis_pub;

    // ! tf listener
    tf::TransformListener tf_listener;
    tf::TransformListener pc_listener;

    //! ServiceServer for the pose of object.
    ros::ServiceServer pose_server;

    //! Define messages
    pcl::PointCloud<PointT> pc;
    pcl::PointCloud<PointT> plane_point;
    pcl::PointCloud<PointT> curr_plane_pc;
    pcl::PointCloud<PointT> curr_clusters_pc;
    darknet_ros_msgs::BoundingBoxes boundingBox;
    flowerfanggirl::ObjectInfoArray object_msg_array;

    visualization_msgs::MarkerArray MarkerArray;

    int nrof_labels;
    int flag;
    int id;
    float radius,plane_x,plane_y,plane_z;
    int centroid_x,centroid_y,x_min,x_max,y_min,y_max;
    string object_pose;
    string label;
    double P,Y;

    //! Get RGB pics from the camera.
    Mat img;
    Eigen::Vector4d point_in_camera;   // Position in camera frame.
    Eigen::Vector4d point_in_base;     // Position in base_link frame.
    Eigen::Vector4f centroid;
    //------------------ Callbacks -------------------

    //! Callback for service calls
    bool get_ObjPose(flowerfanggirl::AllObjInfoObtain::Request &req, flowerfanggirl::AllObjInfoObtain::Response &res);

    //! Callback for subscribers
    //! Complete processing of new point cloud
    void processCloud(const sensor_msgs::PointCloud2ConstPtr &var);
    void processBBox(const darknet_ros_msgs::BoundingBoxes& Bbox);

    pcl::PointCloud<PointT> passthrough(const pcl::PointCloud<PointT>& input_pc);
    pcl::ModelCoefficients::Ptr plane_segmentation_normal(const pcl::PointCloud<PointT>& input_pc);
    pcl::PointCloud<PointT> extract_object(const pcl::PointCloud<PointT>& input_pc);
    pcl::PointCloud<PointT> pc_transformation(const pcl::PointCloud<PointT>& input_pc);
    pcl::PointCloud<PointT> pc_filter(const pcl::PointCloud<PointT>& input_pc, const pcl::ModelCoefficients::Ptr &input_coeff);

    void model_segmentation(const pcl::PointCloud<PointT> &input_pc);
    void Cylinderical_PC_Marker(const Eigen::Vector4f& input_centroid, const pcl::ModelCoefficients::Ptr &coeff_cylinder);
    void Circular_PC_Marker(const Eigen::Vector4f& input_centroid);

    // void plane_segmentation(const pcl::PointCloud<PointT>& origin_pc);
    // void pc_coord(const pcl::PointCloud<PointT>& input_pc);


public:
    //! Subscribes to and advertises topics
    ObjectPose(ros::NodeHandle nh) : nh_(nh)// it_(nh) //,priv_nh_("~"),
        //sub(nh, "topic", 5) // constructor initialization form for the subscriber if needed
    {
        flag = 0;

        boundingbox_sub = nh_.subscribe("/darknet_ros/bounding_boxes", 1, &ObjectPose::processBBox,this);
        pc_sub = nh_.subscribe("/xtion/depth_registered/points", 1, &ObjectPose::processCloud, this);
        pose_server = nh_.advertiseService("final/objectrecognition",&ObjectPose::get_ObjPose,this);

        pub_plane_pc_ = nh_.advertise< pcl::PointCloud<PointT> >("/segmentation/plane_points", 10);
        pub_clusters_pc_ = nh_.advertise< pcl::PointCloud<PointT> >("/segmentation/clusters_points", 10);

        vis_pub = nh_.advertise<visualization_msgs::Marker>("/visualization_marker",0);

        cout << "-------------------- ObjectPose initialized --------------------------" << endl;

    }

    ~ObjectPose()
    {}
};

#endif // OBJECTPOSE_H
