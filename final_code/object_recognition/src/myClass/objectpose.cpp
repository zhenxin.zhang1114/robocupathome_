#include "object_recognition/myClass/objectpose.h"

// Transform the pc message from ros to pcl type.
void ObjectPose::processCloud(const sensor_msgs::PointCloud2ConstPtr &var)
{
  if (flag == 0)
  {
    ROS_ERROR("Warning, lightning storm is creating.");
    pcl::fromROSMsg(*var,pc); 
    while(!(pc.isOrganized()))
    {
      ROS_INFO("Waiting for organisation, please be patient.");
    }
  }
  flag = 1;
}

// Extract the information of boundingbox from yolo and process the message for service.
void ObjectPose::processBBox(const darknet_ros_msgs::BoundingBoxes& Bbox)
{
  if(flag == 1)
  {
    ROS_WARN("Warning, nuclear missile launched.");
    // ROS_ERROR_STREAM(__LINE__);
    flowerfanggirl::ObjectInfo object_msg;
    flowerfanggirl::ObjectInfoArray object_msg_array_backup;

    if(pc.isOrganized())
    {
      nrof_labels = Bbox.bounding_boxes.size();
      boundingBox = Bbox;

      for(int i = 0; i < nrof_labels; i++)
      {
        if (boundingBox.bounding_boxes[i].probability >= 0.1)
        {
          cout <<"---------------" << boundingBox.bounding_boxes[i].Class << " detected. ---------" << endl;
          x_min = boundingBox.bounding_boxes[i].xmin;
          y_min = boundingBox.bounding_boxes[i].ymin;
          x_max = boundingBox.bounding_boxes[i].xmax;
          y_max = boundingBox.bounding_boxes[i].ymax;
          label = boundingBox.bounding_boxes[i].Class;

          pcl::PointCloud<PointT> pc_extracted, pc_filtered;
          id = i;

          pcl::ModelCoefficients::Ptr huge_plane_coeff (new pcl::ModelCoefficients);
          huge_plane_coeff = ObjectPose::plane_segmentation_normal(pc);
          pc_extracted = ObjectPose::extract_object(pc);
          pc_filtered = ObjectPose::pc_filter(pc_extracted,huge_plane_coeff);
          ObjectPose::model_segmentation(pc_filtered);
          // pc_filtered = ObjectPose::passthrough(pc_extracted);
          // ObjectPose::model_segmentation(pc_filtered);
          // ROS_ERROR_STREAM(__LINE__);

          if (centroid[0] >= 0.4 && centroid[0] <= 1.2)
          {
            object_msg.classname = boundingBox.bounding_boxes[i].Class;
            object_msg.pose = object_pose;
            object_msg.position.x = centroid[0];
            object_msg.position.y = centroid[1];
            object_msg.position.z = centroid[2];
            object_msg.RPY.x = 0;
            object_msg.RPY.y = P;
            object_msg.RPY.z = Y;
            object_msg_array_backup.allObjects.push_back(object_msg);
            ROS_INFO_STREAM("The info of object is:" << object_msg);
          }
        }
      }
      object_msg_array = object_msg_array_backup;
    }
  }
  flag = 0; 
}

// Service callback
bool ObjectPose::get_ObjPose(flowerfanggirl::AllObjInfoObtain::Request &req, flowerfanggirl::AllObjInfoObtain::Response &res)
{
  if (req.require)
  {
    while(!(pc.isOrganized()))
    {}
    ROS_INFO("PointCLoud is already organised.");
    if (nrof_labels == 0)
    {
      cerr << "Nothing detected. " << endl;
    }
    else
    {
      cout << "The detected staffs from left to right, from top to bottom." << endl;
      if (object_msg_array.allObjects.size() < 1)
      {
        res.not_empty = false;
      }
      else
      {
        res.not_empty = true;
      }
      
      res.allobj = object_msg_array;
      cerr << res.allobj.allObjects[0] << endl;
    }
  }
  return true;
}

// Extract the pc area of the item in each boundingbox
pcl::PointCloud<PointT> ObjectPose::extract_object(const pcl::PointCloud<PointT>& input_pc)
{
   vector< int > indices;
   pcl::PointCloud<PointT> output_pc, obj_pc;
   int index_helper;
   centroid_x = 0.5*(x_min + x_max);
   centroid_y = 0.5*(y_min + y_max);
   for(int i = x_min; i < x_max; i ++ )
   {
     for(int j = y_min; j < y_max; j ++)
     {
       index_helper = (j-1)*640+i;
       indices.push_back(index_helper);
     }
   }
   pcl::copyPointCloud(input_pc, indices, obj_pc);

   // Transform the pointcloud with tf.
   output_pc = ObjectPose::pc_transformation(obj_pc);
   return output_pc;
}

// Transform the frame of pointcloud from camera to base_link.
pcl::PointCloud<PointT> ObjectPose::pc_transformation(const pcl::PointCloud<PointT>& input_pc)
{
  pcl::PointCloud<PointT> output_pc;
  pcl_conversions::toPCL(ros::Time(0),output_pc.header.stamp);
  // cerr << "Timestamp: " << output_pc.header.stamp << endl;
  pcl_ros::transformPointCloud("base_link", ros::Time(0), input_pc, "xtion_rgb_optical_frame", output_pc, pc_listener);
  // cerr << "Timestamp: " << output_pc.header.stamp << endl;

  return output_pc;
}

// Get the normal vector of the biggest plane from the original pointcloud.
pcl::ModelCoefficients::Ptr ObjectPose::plane_segmentation_normal(const pcl::PointCloud<PointT>& input_pc)
{
  // All the objects needed
  pcl::NormalEstimation<PointT, pcl::Normal> ne;
  pcl::SACSegmentationFromNormals<PointT, pcl::Normal> seg;
  pcl::ExtractIndices<PointT> extract;
  pcl::ExtractIndices<pcl::Normal> extract_normals;
  pcl::search::KdTree<PointT>::Ptr tree (new pcl::search::KdTree<PointT> ());

  // Datasets
  pcl::PointCloud<PointT> cloud;
  pcl::PointCloud<PointT>::Ptr cloud_filtered (new pcl::PointCloud<PointT>);
  pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
  pcl::PointCloud<PointT>::Ptr huge_plane_cloud (new pcl::PointCloud<PointT>);
  pcl::ModelCoefficients::Ptr coefficients_plane (new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers_plane (new pcl::PointIndices);

  cloud = ObjectPose::pc_transformation(input_pc);
  std::cerr << "PointCloud has: " << cloud.points.size () << " data points." << std::endl;

  cloud_filtered = cloud.makeShared();

  // Estimate point normals
  ne.setSearchMethod (tree);
  ne.setInputCloud (cloud_filtered);
  ne.setKSearch (50);
  ne.compute (*cloud_normals);

  // Create the segmentation object for the planar model and set all the parameters
  seg.setOptimizeCoefficients (true);
  seg.setModelType (pcl::SACMODEL_NORMAL_PLANE);
  seg.setNormalDistanceWeight (0.1);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setMaxIterations (100);
  seg.setDistanceThreshold (0.03);
  seg.setInputCloud (cloud_filtered);
  seg.setInputNormals (cloud_normals);

  // Obtain the plane inliers and coefficients
  seg.segment (*inliers_plane, *coefficients_plane);
  // std::cerr << "Plane coefficients: " << *coefficients_plane << std::endl;

  // Remove the planar inliers, extract the rest
  extract.setInputCloud (cloud_filtered);
  extract.setIndices (inliers_plane);
  extract.setNegative (false);
  extract.filter (*huge_plane_cloud);

  // curr_plane_pc = ObjectPose::pc_transformation(*huge_plane_cloud);

  // pcl::copyPointCloud(*huge_plane_cloud,curr_plane_pc);
  // curr_plane_pc.header.frame_id = "base_link";
  // curr_plane_pc.header.stamp = input_pc.header.stamp;
  // pub_plane_pc_.publish(curr_plane_pc.makeShared());


  plane_x = huge_plane_cloud->points[0].x;
  plane_y = huge_plane_cloud->points[0].y;
  plane_z = huge_plane_cloud->points[0].z;

  // ROS_ERROR_STREAM(__LINE__);

  return coefficients_plane;
}

// Filter the plane in the extracted pointcloud by using the normal vector.
pcl::PointCloud<PointT> ObjectPose::pc_filter(const pcl::PointCloud<PointT>& input_pc, const pcl::ModelCoefficients::Ptr &input_coeff)
{
  vector< int > object_indices;
  pcl::PointCloud<PointT> whole_pointcloud;
  pcl::PointCloud<PointT> object_pc;
  pcl::PointCloud<PointT>::Ptr whole_pc (new pcl::PointCloud<PointT>);
  pcl::copyPointCloud(input_pc, whole_pointcloud);
  whole_pc = whole_pointcloud.makeShared();

  // ROS_ERROR_STREAM(__LINE__);

  int pc_size;
  pc_size = input_pc.points.size();
  cerr << "Point cloud before filtering has " << pc_size << "data points." << endl;
  float filter_threshold;

  if (!(whole_pc->points.empty ()))
  {
    // ROS_ERROR_STREAM(__LINE__);
    for (int i = 0; i < pc_size; i++)
    {
      if (label == "cola can")
      {
        filter_threshold = 0.15;
      }
      else
      {
        filter_threshold = 0.08;
      }
      // cerr << "The threshold is: " << filter_threshold << endl;
      if ((whole_pc->points[i].x-plane_x)*(input_coeff->values[0]) + (whole_pc->points[i].y-plane_y)*(input_coeff->values[1]) + (whole_pc->points[i].z-plane_z)*(input_coeff->values[2]) + input_coeff->values[3] > filter_threshold)
      {
        object_indices.push_back(i);
      }
    }

    // ROS_ERROR_STREAM(__LINE__);
    pcl::copyPointCloud(input_pc, object_indices, object_pc);

    pcl::copyPointCloud(object_pc,curr_clusters_pc);
    curr_clusters_pc.header.frame_id = "base_link";
    curr_clusters_pc.header.stamp = input_pc.header.stamp;
    pub_clusters_pc_.publish(curr_clusters_pc.makeShared());
  }

  return object_pc;
}

// Filter the ground by using the passthrough of z axis.
pcl::PointCloud<PointT> ObjectPose::passthrough(const pcl::PointCloud<PointT>& input_pc)
{
  pcl::PointCloud<PointT>::Ptr cloud1( new pcl::PointCloud<PointT> ());

  cloud1 = input_pc.makeShared(); // cloud to operate
  // cerr << "pcl coordinate in base_link is: "<< cloud.at(290,329).x <<", "<< cloud.at(290,329).y <<", "<< cloud.at(290,329).z <<endl;
  pcl::PointCloud<PointT>::Ptr cloud_filtered( new pcl::PointCloud<PointT> ()); // cloud to store the filter the data
  // cout << "PointCloud before filtering has: " << cloud1 -> points.size() << " data points." << endl;

  // Create the filtering object
  pcl::PassThrough<PointT> pass;
  pass.setInputCloud (cloud1);
  pass.setFilterFieldName ("z");
  pass.setFilterLimits (-0.095, 2.0);
  pass.setFilterLimitsNegative (false);
  pass.filter (*cloud_filtered);
  // cout << "PointCloud after filtering has: " << cloud_filtered -> points.size() << " data points." << endl;

  pcl::copyPointCloud(*cloud_filtered,curr_clusters_pc);
  return curr_clusters_pc;
}

// Segment the cylinderical and circular components from the filtered pointcloud.
void ObjectPose::model_segmentation(const pcl::PointCloud<PointT> &input_pc)
{
  if (boundingBox.bounding_boxes[id].Class == "bottle"||"cola can"||"banana")
  {
    // All the objects needed
    pcl::NormalEstimation<PointT, pcl::Normal> ne;
    pcl::SACSegmentationFromNormals<PointT, pcl::Normal> seg;
    pcl::ExtractIndices<PointT> extract;
    pcl::ExtractIndices<pcl::Normal> extract_normals;
    pcl::search::KdTree<PointT>::Ptr tree (new pcl::search::KdTree<PointT> ());

    // Datasets
    pcl::PointCloud<PointT>::Ptr cloud (new pcl::PointCloud<PointT>);
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
    pcl::PointCloud<PointT>::Ptr cloud_filtered (new pcl::PointCloud<PointT>);
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals2 (new pcl::PointCloud<pcl::Normal>);
    pcl::ModelCoefficients::Ptr coefficients_plane (new pcl::ModelCoefficients), coefficients_cylinder (new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers_plane (new pcl::PointIndices), inliers_cylinder (new pcl::PointIndices);

    cloud = input_pc.makeShared();

    // Estimate point normals
    ne.setSearchMethod (tree);
    ne.setInputCloud (cloud);
    ne.setKSearch (50);
    ne.compute (*cloud_normals);

    // Create the segmentation object for cylinder segmentation and set all the parameters
    seg.setOptimizeCoefficients (true);
    seg.setModelType (pcl::SACMODEL_CYLINDER);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setNormalDistanceWeight (0.1);
    seg.setMaxIterations (10000);
    seg.setDistanceThreshold (0.05);
    seg.setRadiusLimits (0, 0.03);
    seg.setInputCloud (cloud);
    seg.setInputNormals (cloud_normals);

    // Obtain the cylinder inliers and coefficients
    seg.segment (*inliers_cylinder, *coefficients_cylinder);
    // cerr << "Cylinder coefficients: " << *coefficients_cylinder << endl;

    // Write the cylinder inliers to disk
    extract.setInputCloud (cloud);
    extract.setIndices (inliers_cylinder);
    extract.setNegative (false);
    pcl::PointCloud<PointT>::Ptr cloud_cylinder (new pcl::PointCloud<PointT> ());
    extract.filter (*cloud_cylinder);

    pcl::compute3DCentroid(*cloud_cylinder,centroid);
    cerr << "The centroid of the cylindrical pc is: "
         << centroid[0] << ", "
         << centroid[1] << ", "
         << centroid[2] << ", " << endl;

    if (cloud_cylinder->points.empty ())
      cerr << "Can't find the cylindrical component." << endl;
    else
    {
      cerr << "PointCloud representing the cylindrical component: " << cloud_cylinder->points.size () << " data points." << endl;

      // Get the centroid of the cylinder component.
      ObjectPose::Cylinderical_PC_Marker(centroid, coefficients_cylinder);
      double x_arrow,y_arrow,z_arrow;
      x_arrow = coefficients_cylinder->values[3];
      y_arrow = coefficients_cylinder->values[4];
      z_arrow = coefficients_cylinder->values[5];
      radius = coefficients_cylinder->values[6];
      cerr << "The radius of the cylindrical item is:" << radius << endl;
      // cerr << "The central axis of the chosen item is: " << x_arrow << ", " << y_arrow << ", " << z_arrow << endl;
      P = atan(z_arrow/sqrt(pow(x_arrow,2) + pow(y_arrow,2)));     // The pose of the item with respect to arm_link_7.
      Y = atan(y_arrow/x_arrow) + PI/2;        // The angle of the item.

      if ((fabs(P) <= PI/4) && (fabs(P) >= 0))
      {
        object_pose = "lying";                // The item is lying.
        cerr << "The chosen item is lying." << endl;
        cerr << "The pose of the item is: " << Y*180/PI << " degrees" << endl;
      }
      else
      {
        object_pose = "standing";               // The item is standing.
        cerr << "The chosen item is standing." << endl;
        // cerr << "The angle of the item is: " << P*180/PI << endl;
      }

      // Publish other clusters
      pcl::copyPointCloud(*cloud_cylinder,curr_plane_pc);
      curr_plane_pc.header.frame_id = "base_link";
      curr_plane_pc.header.stamp = input_pc.header.stamp;
      pub_plane_pc_.publish(curr_plane_pc.makeShared());

    }

  }
  else
  {
    // For the circular staffs.

    if (input_pc.points.empty ())
     cerr << "Can't find the circular component." << endl;
    else
    {
     cerr << "PointCloud representing the circular component: " << input_pc.points.size () << " data points." << endl;

     // Get the centroid of the circular component.
     pcl::compute3DCentroid(input_pc,centroid);
     object_pose = "standing";
     cerr << " The centroid of the circular item pc is: "
          << centroid[0] << ", "
          << centroid[1] << ", "
          << centroid[2] << ", " << endl;

      // Mark circle center in the rviz.
      ObjectPose::Circular_PC_Marker(centroid);

      // Publish other clusters
      pcl::copyPointCloud(input_pc,curr_clusters_pc);
      curr_clusters_pc.header.frame_id = "base_link";
      curr_clusters_pc.header.stamp = input_pc.header.stamp;
      pub_clusters_pc_.publish(curr_clusters_pc.makeShared());
    }
  }
}

// Mark the central axis and centroid of the cylinderical component in rviz.
void ObjectPose::Cylinderical_PC_Marker(const Eigen::Vector4f& input_centroid, const pcl::ModelCoefficients::Ptr &coeff_cylinder)
{
  // Initialization.
  visualization_msgs::Marker point1;
  visualization_msgs::Marker point2;
  visualization_msgs::Marker line;
  // Set time stamp and frame_id.
  point1.header.frame_id = point2.header.frame_id = line.header.frame_id = "base_link";
  point1.header.stamp = point2.header.stamp = line.header.stamp = ros::Time::now();

  point1.ns = point2.ns = line.ns = "PC_points";
  point1.id = 0;
  point2.id = 1;
  line.id = 2;

  point1.type = point2.type = visualization_msgs::Marker::POINTS;
  line.type = visualization_msgs::Marker::LINE_LIST;
  point1.action = point2.action = line.action = visualization_msgs::Marker::ADD;

  // points.pose.position.x = input_centroid[0];
  // points.pose.position.y = input_centroid[1];
  // points.pose.position.z = input_centroid[2];
  // points.pose.orientation.x = 0.0;
  // points.pose.orientation.y = 0.0;
  // points.pose.orientation.z = 0.0;
  point1.pose.orientation.w = point2.pose.orientation.w = line.pose.orientation.w = 1.0;

  point1.scale.x = 0.02;
  point1.scale.y = 0.02;
  point1.scale.z = 0.02;
  point2.scale.x = line.scale.x = 0.01;
  point2.scale.y = line.scale.y = 0.01;
  point2.scale.z = line.scale.z = 0.01;

  point1.color.r = 1.0;
  point1.color.g = 0.0;
  point1.color.b = 0.0;
  point1.color.a = 1.0;

  point2.color.r = 0.0;
  point2.color.g = 1.0;
  point2.color.b = 0.0;
  point2.color.a = 1.0;

  line.color.r = 0.0;
  line.color.g = 0.0;
  line.color.b = 1.0;
  line.color.a = 1.0;

  point1.lifetime = point2.lifetime = line.lifetime = ros::Duration();

  geometry_msgs::Point centroid_point;
  geometry_msgs::Point origin;
  geometry_msgs::Point cylinder_point1;
  geometry_msgs::Point cylinder_point2;

  centroid_point.x = input_centroid[0];
  centroid_point.y = input_centroid[1];
  centroid_point.z = input_centroid[2];

  origin.x = 0.0;
  origin.y = 0.0;
  origin.z = 0.0;

  cylinder_point1.x = coeff_cylinder->values[0];
  cylinder_point1.y = coeff_cylinder->values[1];
  cylinder_point1.z = coeff_cylinder->values[2];


  cylinder_point2.x = coeff_cylinder->values[0] + coeff_cylinder->values[3];
  cylinder_point2.y = coeff_cylinder->values[1] + coeff_cylinder->values[4];
  cylinder_point2.z = coeff_cylinder->values[2] + coeff_cylinder->values[5];

  point1.points.push_back(centroid_point);
  point2.points.push_back(cylinder_point1);
  line.points.push_back(cylinder_point1);
  line.points.push_back(cylinder_point2);

  vis_pub.publish(point1);
  vis_pub.publish(point2);
  vis_pub.publish(line);
}

// Mark the central axis and centroid of the circular component in rviz.
void ObjectPose::Circular_PC_Marker(const Eigen::Vector4f& input_centroid)
{
  // Initialization.
  visualization_msgs::Marker circle_centroid;
  // Set time stamp and frame_id.
  circle_centroid.header.frame_id = "base_link";
  circle_centroid.header.stamp = ros::Time::now();

  circle_centroid.ns = "Circular_PC_points";
  circle_centroid.id = 0;

  circle_centroid.type = visualization_msgs::Marker::POINTS;
  circle_centroid.action = visualization_msgs::Marker::ADD;

  circle_centroid.pose.orientation.w = 1.0;

  circle_centroid.scale.x = 0.02;
  circle_centroid.scale.y = 0.02;
  circle_centroid.scale.z = 0.02;

  circle_centroid.color.r = 1.0;
  circle_centroid.color.g = 0.0;
  circle_centroid.color.b = 0.0;
  circle_centroid.color.a = 1.0;

  circle_centroid.lifetime = ros::Duration();

  geometry_msgs::Point centroid_point;

  centroid_point.x = input_centroid[0];
  centroid_point.y = input_centroid[1];
  centroid_point.z = input_centroid[2];

  circle_centroid.points.push_back(centroid_point);

  vis_pub.publish(circle_centroid);
}
