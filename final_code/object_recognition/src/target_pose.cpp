#include <ros/ros.h>
#include <ros/console.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <std_msgs/Char.h>

#include <Eigen/Dense>
#include <vector>

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <tf/transform_listener.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>


// PCL specific includes
#include <pcl_ros/point_cloud.h> // enable pcl publishing
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/passthrough.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <image_geometry/pinhole_camera_model.h>

// Visualization
//#include <visualization_msgs/MarkerArray.h>
//#include <visualization_msgs/Marker.h>

#include <geometry_msgs/PoseStamped.h>
#include <darknet_ros_msgs/BoundingBoxes.h>
#include <darknet_ros_msgs/BoundingBox.h>

using namespace std;
using namespace cv;


//! Plane segmentation class
//! computes and split the big planes from the rest of the point cloud clusters
class PclTest
{

private:
    //! The node handle
    ros::NodeHandle nh_;
    //! Node handle in the private namespace
    ros::NodeHandle priv_nh_;

    //! Subscribers to the PointCloud data
    // Optional: MESSAGE FILTERS COULD BE A GOOD IDEA FOR GRABBING MULTIPLE TOPICS SYNCRONIZED, NOT NEEDED THOUGH
    ros::Subscriber pc_sub;

    //! Subcriber for boundingbox
    //ros::Subscriber boundingbox_sub;

    //! Publisher for pointclouds
    ros::Publisher pub_plane_pc_;
    ros::Publisher pub_clusters_pc_;

    //! Define messages
    pcl::PointCloud<pcl::PointXYZRGB> pc;
    pcl::PointCloud<pcl::PointXYZRGB> curr_table_pc;
    pcl::PointCloud<pcl::PointXYZRGB> curr_clusters_pc;
    //pcl::PointCloud<pcl::PointXYZRGB> objects_pc;

    //geometry_msgs::Point point_3d;

    int centroid_x,centroid_y;
    int bound_x,bound_y,height,width;

    //------------------ Callbacks -------------------

    //! Callback for service calls


    //! Callback for subscribers
    void processCloud(const sensor_msgs::PointCloud2ConstPtr& var);
    //void processbb(const darknet_ros_msgs::BoundingBoxes& Bbox);
    //void pc_coord();
public:
    //! Subscribes to and advertises topics
    PclTest(ros::NodeHandle nh) : nh_(nh), priv_nh_("~") //,
        //sub(nh, "topic", 5) // constructor initialization form for the subscriber if needed
    {

        //boundingbox_sub = nh_.subscribe("/darknet_ros/bounding_boxes", 1, &PclTest::processbb,this);
        pc_sub = nh_.subscribe("/xtion/depth_registered/points", 1, &PclTest::processCloud, this);
        pub_plane_pc_ = nh_.advertise< pcl::PointCloud<pcl::PointXYZRGB> >("/segmentation/plane_points", 10);
        pub_clusters_pc_ = nh_.advertise< pcl::PointCloud<pcl::PointXYZRGB> >("/segmentation/clusters_points", 10);

        ROS_INFO("PclTest initialized ...");

    }

    ~PclTest() {}
};

//!Callback for subscriber
//! Callback for processing the Point Cloud data
void PclTest::processCloud(const sensor_msgs::PointCloud2ConstPtr &var)
{
    pcl::PassThrough<pcl::PointXYZRGB> pass;
    pcl::NormalEstimation<pcl::PointXYZRGB, pcl::Normal> ne;
    pcl::SACSegmentationFromNormals<pcl::PointXYZRGB, pcl::Normal> seg;
    pcl::ExtractIndices<pcl::PointXYZRGB> extract;
    pcl::ExtractIndices<pcl::Normal> extract_normals;
    pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGB> ());

    // Datasets
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZRGB>);// cloud after pass through filtering
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>); // Normal vectors of the cloud_filtered
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_filtered2 (new pcl::PointCloud<pcl::PointXYZRGB>);//
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals2 (new pcl::PointCloud<pcl::Normal>);//
    pcl::ModelCoefficients::Ptr coefficients_plane (new pcl::ModelCoefficients), coefficients_cylinder (new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers_plane (new pcl::PointIndices), inliers_cylinder (new pcl::PointIndices);

    // Convert the data to the internal var (pc) using pcl function: fromROSMsg
    // TODO

    pcl::fromROSMsg(*var,pc);
    cloud = pc.makeShared();



//    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud = pc.makeShared(); // cloud to operate
//    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_filtered( new pcl::PointCloud<pcl::PointXYZRGB> ); // cloud to store the filter the data

    cout << "PointCloud before filtering has: " << pc.points.size() << " data points." << endl;

    pass.setInputCloud (cloud);
    pass.setFilterFieldName ("z");
    pass.setFilterLimits (0, 1.5);
    pass.filter (*cloud_filtered);
    cerr << "PointCloud after filtering has: " << cloud_filtered->points.size () << " data points." << endl;

    // Estimate point normals
    ne.setSearchMethod (tree);
    ne.setInputCloud (cloud_filtered);
    ne.setKSearch (50);
    ne.compute (*cloud_normals);
    cout << "cloud_normals: " << cloud_normals << endl;

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_plane (new pcl::PointCloud<pcl::PointXYZRGB> ());

    // Create the segmentation object for the planar model and set all the parameters
    seg.setOptimizeCoefficients (true);
    seg.setModelType (pcl::SACMODEL_NORMAL_PLANE);
    seg.setNormalDistanceWeight (0.1);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setMaxIterations (100);
    seg.setDistanceThreshold (0.03);
    seg.setInputCloud (cloud_filtered);
    seg.setInputNormals (cloud_normals);
    // Obtain the plane inliers and coefficients
    seg.segment (*inliers_plane, *coefficients_plane);
    cerr << "Plane coefficients: " << *coefficients_plane << endl;

    // Extract the planar inliers from the input cloud
    extract.setInputCloud (cloud_filtered);
    extract.setIndices (inliers_plane);
    extract.setNegative (false);

    // Write the planar inliers to disk
    extract.filter (*cloud_plane);
    cerr << "PointCloud representing the planar component: " << cloud_plane->points.size () << " data points." << endl;

    // Remove the planar inliers, extract the rest
    extract.setNegative (true);
    extract.filter (*cloud_filtered2);
    extract_normals.setNegative (true);
    extract_normals.setInputCloud (cloud_normals);
    extract_normals.setIndices (inliers_plane);
    extract_normals.filter (*cloud_normals2);
    cout << "cloud_normals2: " << cloud_normals2 << endl;


    // Create the segmentation object for cylinder segmentation and set all the parameters
    seg.setOptimizeCoefficients (true);
    seg.setModelType (pcl::SACMODEL_CYLINDER);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setNormalDistanceWeight (0.1);
    seg.setMaxIterations (10000);
    seg.setDistanceThreshold (0.05);
    seg.setRadiusLimits (0, 0.1);
    seg.setInputCloud (cloud_filtered2);
    seg.setInputNormals (cloud_normals2);

    // Obtain the cylinder inliers and coefficients
    seg.segment (*inliers_cylinder, *coefficients_cylinder);
    cerr << "Cylinder coefficients: " << *coefficients_cylinder << endl;

    // Write the cylinder inliers to disk
    extract.setInputCloud (cloud_filtered2);
    extract.setIndices (inliers_cylinder);
    extract.setNegative (false);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_cylinder (new pcl::PointCloud<pcl::PointXYZRGB> ());
    extract.filter (*cloud_cylinder);
    if (cloud_cylinder->points.empty ())
      cerr << "Can't find the cylindrical component." << endl;
    else
    {
            cerr << "PointCloud representing the cylindrical component: " << cloud_cylinder->points.size () << " data points." << endl;
    }

    // Create the filtering object
//    pcl::StatisticalOutlierRemoval<pcl::PointXYZRGB> sor;
//    sor.setInputCloud (cloud);
//    sor.setMeanK (50);
//    sor.setStddevMulThresh (1.0);
//    sor.filter (*cloud_filtered);

//    cerr <<  "PointCloud after filtering has: " << cloud_filtered->points.size () << " data points."<< endl;

//    sor.setNegative (false);
//    sor.filter (*cloud_filtered);
//    // Down sample the pointcloud using VoxelGrid

//    pcl::VoxelGrid<pcl::PointXYZRGB> pc_filter;
//    pc_filter.setInputCloud(cloud);
//    pc_filter.setLeafSize(0.01f,0.01f,0.01f);
//    pc_filter.filter(*cloud_filtered);

//    pcl::copyPointCloud(*cloud_filtered,objects_pc);
//    cout<<"\nThe width of objects_pc is: "<<objects_pc.width<<"\n"<<endl;
//    cout<<"\nThe height of objects_pc is: "<<objects_pc.height<<"\n"<<endl;

//    cout << "PointCloud after filtering has: " << cloud_filtered->points.size()  << " data points." << endl;

//    // Create the segmentation object for the plane model and set all the parameters using pcl::SACSegmentation<pcl::PointXYZ>
//    pcl::SACSegmentation<pcl::PointXYZRGB> seg;
//    pcl::PointIndices::Ptr inliers( new pcl::PointIndices );
//    pcl::ModelCoefficients::Ptr coefficients( new pcl::ModelCoefficients );
//    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_plane( new pcl::PointCloud<pcl::PointXYZRGB>() );
//    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_backup( new pcl::PointCloud<pcl::PointXYZRGB>() );

//    cloud_plane->width = 1;
//    cloud_plane->height = 1;

//    // set parameters of the SACS segmentation
//        // TODO
//    seg.setOptimizeCoefficients(true);
//    seg.setModelType(pcl::SACMODEL_PLANE);
//    seg.setMethodType(pcl::SAC_RANSAC);
//    seg.setMaxIterations(1000);
//    seg.setDistanceThreshold(0.01);


//    int nr_points = (int) cloud_filtered->points.size();

//    // Segment the planes using pcl::SACSegmentation::segment() function and pcl::ExtractIndices::filter() function
//        // TODO
//        // If you want to extract more than one plane you have to do a while
//    pcl::ExtractIndices<pcl::PointXYZRGB> extract;

//    while (cloud_filtered->points.size () > 0.15 * nr_points)
//    {
//        // Segment the largest planar compornent from the remaining cloud
//        seg.setInputCloud (cloud_filtered);
//        seg.segment (*inliers, *coefficients);
//        if (inliers->indices.size () == 0)
//        {
//          cerr << "Could not estimate a planar model for the given dataset." << endl;
//          //break;
//        }
////        cerr << "Model coefficients: " << coefficients->values[0] << " "
////                                       << coefficients->values[1] << " "
////                                       << coefficients->values[2] << " "
////                                       << coefficients->values[3] << endl;

//        // Extract the inliers
//        extract.setInputCloud (cloud_filtered);
//        extract.setIndices (inliers);
//        extract.setNegative (false);
//        extract.filter (*cloud_backup);
//        if ((cloud_backup->points.size()) > (cloud_plane->points.size()))
//        {
//            cloud_plane = cloud_backup->makeShared();
//        }

////        cout<<"---------------------------------------------------------------------"<<endl;
////        cout<<"---------------------------------------------------------------------"<<endl;
////        cout<<"points_amount: "<<cloud_backup->points.size()<<endl;
////        cout<<"---------------------------------------------------------------------"<<endl;
////        cout<<"---------------------------------------------------------------------"<<endl;

//        // Extract the objects on the table.
//        extract.setNegative (true);
//        extract.filter (*cloud_filtered);

//   }

    // Publish biggest plane
        // TODO
        // Tips:
        // - you can copy the pointcloud using pcl::copyPointCloud()
        // - Set the header frame_id to the pc2 header frame_id
        // - you can use pcl_conversions::toPCL() to convert the stamp from pc2 header to pointcloud header stamp
        // - to publish -> pub_plane_pc_.publish( pointcloud_variable.makeShared() )
        pcl::copyPointCloud(*cloud_plane,curr_table_pc);
        curr_table_pc.header.frame_id = "xtion_rgb_optical_frame";
        pcl_conversions::toPCL(var->header.stamp,curr_table_pc.header.stamp);
        pub_plane_pc_.publish(curr_table_pc.makeShared());


    // Publish other clusters
    // TODO similar to the previous publish
        pcl::copyPointCloud(*cloud_cylinder,curr_clusters_pc);
        curr_clusters_pc.header.frame_id = "xtion_rgb_optical_frame";
        pcl_conversions::toPCL(var->header.stamp,curr_clusters_pc.header.stamp);
        pub_clusters_pc_.publish(curr_clusters_pc.makeShared());

        return;
}
//void PclTest::processbb(const darknet_ros_msgs::BoundingBoxes& Bbox)
//{
//    if(pc.isOrganized())
//    {
//        ROS_INFO("The Cloud is ORGANIZED!!!");
//        ROS_INFO("Start reading the point cloud");

//        int nrof_labels;
//        nrof_labels = Bbox.bounding_boxes.size();
//        cout<<"The detected staffs from left to right, from top to bottom."<<endl;

//        vector< int > IDs;
//        vector< int > original_IDs;
//        int id = 0;
//        int original_id = 0;
//        for(int i = 0; i < nrof_labels; i++)
//        {
//            original_id++;
//            if (Bbox.bounding_boxes[i].probability >= 0.5)
//            {
//                id++;
//                cout<<Bbox.bounding_boxes[i].Class<<" detected. ID: "<<id<<endl;
//                IDs.push_back(id);
//                original_IDs.push_back(original_id);
//            }
//        }
//        // Input the label of the item.
//        ROS_INFO_STREAM("Please input the ID of the detected item.");
//        string num;
//        cin>>num;
//        for(int i = 0; i < nrof_labels; i++)
//        {
//            if(atoi(num.c_str()) == original_IDs[i])
//            {
//                centroid_x = 0.5*(Bbox.bounding_boxes[i].xmin+Bbox.bounding_boxes[i].xmax);
//                centroid_y = 0.5*(Bbox.bounding_boxes[i].ymin+Bbox.bounding_boxes[i].ymax);
//                bound_x = Bbox.bounding_boxes[i].xmin;
//                bound_y = Bbox.bounding_boxes[i].ymin;
//                height = Bbox.bounding_boxes[i].ymax-Bbox.bounding_boxes[i].ymin;
//                width = Bbox.bounding_boxes[i].xmax-Bbox.bounding_boxes[i].xmin;
//            }
//        }
//        cout<<"xmin="<<bound_x<<", ymin="<<bound_y<<", height="<<height<<", width="<<width<<endl;
//        cout<<"The centroid is: "<<centroid_x<<","<<centroid_y<<endl;

//        //PclTest::pc_coord();
//    }
//}
//void PclTest::pc_coord()
//{

//    float pcl_x,pcl_y,pcl_z;
//    pcl_x = objects_pc.at(centroid_x,centroid_y).x;
//    pcl_y = objects_pc.at(centroid_x,centroid_y).y;
//    pcl_z = objects_pc.at(centroid_x,centroid_y).z;

//    if (!isnan(pcl_x) && !isnan(pcl_y) && !isnan(pcl_z))
//    {
//        cout<<"pcl coordinate is: "<<pcl_x<<","<<pcl_y<<","<<pcl_z<<endl;
//        point_3d.x = pcl_x;
//        point_3d.y = pcl_y;
//        point_3d.z = pcl_z;
//        //point3d_pub.publish(point_3d);
//    }
//    else
//    {
//        ROS_INFO("PCL not found.");
//    }
//}


int main(int argc, char** argv)
{
    ros::init(argc, argv, "pcl_test");
    ros::NodeHandle nh;
    PclTest node(nh);
    ros::spin();
    return 0;
}



