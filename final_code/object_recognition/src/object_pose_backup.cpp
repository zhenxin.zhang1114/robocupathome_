#include "object_recognition/myClass/objectpose.h"

void ObjectPose::processCloud(const sensor_msgs::PointCloud2ConstPtr &var)
{
  ROS_ERROR("Warning, lightning storm is creating.");
  pcl::fromROSMsg(*var,pc);
  while(!(pc.isOrganized()))
  {
    ROS_INFO("Waiting for organisation, please be patient.");
  }
  // ROS_ERROR_STREAM(__LINE__);
}

void ObjectPose::processBBox(const darknet_ros_msgs::BoundingBoxes& Bbox)
{
  ROS_WARN("Warning, nuclear missile launched.");
  // ROS_ERROR_STREAM(__LINE__);
  flowerfanggirl::ObjectInfo object_msg;
  flowerfanggirl::ObjectInfoArray object_msg_array_backup;

  if(pc.isOrganized())
  {
    nrof_labels = Bbox.bounding_boxes.size();
    boundingBox = Bbox;

    for(int i = 0; i < nrof_labels; i++)
    {
      if (boundingBox.bounding_boxes[i].probability >= 0.1)
      {
        cout <<"---------------" << boundingBox.bounding_boxes[i].Class << " detected. ---------" << endl;
        x_min = boundingBox.bounding_boxes[i].xmin;
        y_min = boundingBox.bounding_boxes[i].ymin;
        x_max = boundingBox.bounding_boxes[i].xmax;
        y_max = boundingBox.bounding_boxes[i].ymax;

        pcl::PointCloud<PointT> pc_extracted, pc_filtered;
        id = i;

        pc_extracted = ObjectPose::extract_object(pc);
        pc_filtered = ObjectPose::passthrough(pc_extracted);
        ObjectPose::model_segmentation(pc_filtered);
        ROS_ERROR_STREAM(__LINE__);


        object_msg.classname = boundingBox.bounding_boxes[i].Class;
        object_msg.pose = object_pose;
        object_msg.position.x = centroid[0];
        object_msg.position.y = centroid[1];
        object_msg.position.z = centroid[2];
        object_msg.RPY.x = 0;
        object_msg.RPY.y = P;
        object_msg.RPY.z = Y;
        object_msg_array_backup.allObjects.push_back(object_msg);
        ROS_INFO_STREAM("The info of object is:"<<object_msg);
      }
    }
    object_msg_array = object_msg_array_backup;
  }
}

void ObjectPose::pc_coord(const pcl::PointCloud<PointT>& input_pc)
{
  float pcl_x,pcl_y,pcl_z;
  pcl_x = input_pc.at(299,336).x;
  pcl_y = input_pc.at(299,336).y;
  pcl_z = input_pc.at(299,336).z;

  if (!isnan(pcl_x) && !isnan(pcl_y) && !isnan(pcl_z))
  {
    cout << "pcl coordinate is: " << pcl_x << ", " << pcl_y << ", " << pcl_z << endl;

    ROS_INFO( "Transformation begins." );

    //ros::Time now = pcl_conversions::fromPCL(pc.header).stamp;

    tf::StampedTransform transform;
    try{
        tf_listener.waitForTransform("/base_link","/xtion_rgb_optical_frame",ros::Time(0),ros::Duration(10.0));

        tf_listener.lookupTransform("/base_link", "/xtion_rgb_optical_frame",ros::Time(0), transform);
    }
    catch (tf::TransformException ex){
        ROS_ERROR("%s",ex.what());
        ros::Duration(1.0).sleep();
    }
    //! Define the staffs for the tf_Transform.
    Eigen::Affine3d trans;

    tf::transformTFToEigen(transform,trans);

    point_in_camera[0] = pcl_x;
    point_in_camera[1] = pcl_y;
    point_in_camera[2] = pcl_z;
    point_in_camera[3] = 1;

    point_in_base = trans.matrix() * point_in_camera;
    cerr << "---------------------------------------------------------------------" << endl;
    cerr << "---------------------------------------------------------------------" << endl;
    cerr << "pcl coordinate in base_link is: "<<point_in_base[0]<<", "<<point_in_base[1]<<", "<<point_in_base[2]<<endl;
    cerr << "---------------------------------------------------------------------" << endl;
    cerr << "---------------------------------------------------------------------" << endl;
  }
  else
  {
    ROS_INFO("PCL not found.");
  }
}

bool ObjectPose::get_ObjPose(flowerfanggirl::AllObjInfoObtain::Request &req, flowerfanggirl::AllObjInfoObtain::Response &res)
{
  if (req.require)
  {
    while(!(pc.isOrganized()))
    {}
    ROS_INFO("PointCLoud is already organised.");
    if (nrof_labels == 0)
    {
      cerr << "Nothing detected. " << endl;
    }
    else
    {
      cout << "The detected staffs from left to right, from top to bottom." << endl;
      res.allobj = object_msg_array;
      cerr << res.allobj.allObjects[0] << endl;
    }
  }
  return true;
}

// void ObjectPose::imageCallback(const sensor_msgs::ImageConstPtr& msg)
// {
//   cv_bridge::CvImagePtr cv_ptr;
//   try
//   {
//     cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
//   }
//   catch (cv_bridge::Exception& e)
//   {
//     ROS_ERROR("cv_bridge exception: %s", e.what());
//     return;
//   }
//   // imshow("OBJECT_WINDOW", cv_ptr->image);
//   // waitKey(3);
//
//   img = Mat(cv_ptr->image.size(),CV_8UC3);
//   cv_ptr->image.copyTo(img);
//
//   imwrite("/home/flowerfanggirl/train_yolo/images/bottles.jpg", img);
//   // imshow("RECT_IMAGE", image_roi);
//   // waitKey(3);
//   // ObjectPose::getContours(img);
// }
//
// void ObjectPose::getContours(const Mat& src)
// {
//   Mat src_gray, canny_output;
//   int lowThreshold = 0;
//   const int max_lowThreshold = 100;
//   const int kernel_size = 3;
//   const char* window_name = "Canny";
//   //! RGB2GRAY and Filter
//   cvtColor( src, src_gray, CV_BGR2GRAY );
//   blur( src_gray, src_gray, Size(3,3) );
//
//   Canny( src_gray, canny_output, lowThreshold, max_lowThreshold, kernel_size );
//   namedWindow( window_name, CV_WINDOW_AUTOSIZE );
//   imshow( window_name, canny_output);
//   waitKey(3);
//
//   vector<vector<Point> > contours;
//   vector<Vec4i> hierarchy;
//
//   findContours( canny_output, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE, Point(1, 1) );
//
//   /// Draw contours
//   Mat drawing = Mat::zeros( canny_output.size(), CV_8U );
//
//   drawContours( drawing, contours, -1, Scalar(255,0,255));
//
//   // Show in a window
//   namedWindow( "Contours", CV_WINDOW_AUTOSIZE );
//   imshow( "Contours", drawing );
//   waitKey(0);
// }

pcl::PointCloud<PointT> ObjectPose::extract_object(const pcl::PointCloud<PointT>& input_pc)
{
   vector< int > indices;
   pcl::PointCloud<PointT> output_pc, obj_pc;
   int index_helper;
   centroid_x = 0.5*(x_min + x_max);
   centroid_y = 0.5*(y_min + y_max);
   for(int i = x_min; i < x_max; i ++ )
   {
     for(int j = y_min; j < y_max; j ++)
     {
       index_helper = (j-1)*640+i;
       indices.push_back(index_helper);
     }
   }
   pcl::copyPointCloud(input_pc, indices, obj_pc);

   // Transform the pointcloud with tf.
   output_pc = ObjectPose::pc_transformation(obj_pc);
   return output_pc;
}

pcl::PointCloud<PointT> ObjectPose::pc_transformation(const pcl::PointCloud<PointT>& input_pc)
{
  pcl::PointCloud<PointT> output_pc;
  pcl_conversions::toPCL(ros::Time(0),output_pc.header.stamp);
  // cerr << "Timestamp: " << output_pc.header.stamp << endl;
  pcl_ros::transformPointCloud("base_link", ros::Time(0), input_pc, "xtion_rgb_optical_frame", output_pc, pc_listener);
  // cerr << "Timestamp: " << output_pc.header.stamp << endl;

  return output_pc;
}

pcl::PointCloud<PointT> ObjectPose::passthrough(const pcl::PointCloud<PointT>& input_pc)
{
  pcl::PointCloud<PointT>::Ptr cloud1( new pcl::PointCloud<PointT> ());

  cloud1 = input_pc.makeShared(); // cloud to operate
  // cerr << "pcl coordinate in base_link is: "<< cloud.at(290,329).x <<", "<< cloud.at(290,329).y <<", "<< cloud.at(290,329).z <<endl;
  pcl::PointCloud<PointT>::Ptr cloud_filtered( new pcl::PointCloud<PointT> ()); // cloud to store the filter the data
  // cout << "PointCloud before filtering has: " << cloud1 -> points.size() << " data points." << endl;

  // Create the filtering object
  pcl::PassThrough<PointT> pass;
  pass.setInputCloud (cloud1);
  pass.setFilterFieldName ("z");
  pass.setFilterLimits (-0.095, 2.0);
  pass.setFilterLimitsNegative (false);
  pass.filter (*cloud_filtered);
  // cout << "PointCloud after filtering has: " << cloud_filtered -> points.size() << " data points." << endl;

  pcl::copyPointCloud(*cloud_filtered,curr_clusters_pc);
  return curr_clusters_pc;
}

void ObjectPose::model_segmentation(const pcl::PointCloud<PointT> &input_pc)
{
  if (boundingBox.bounding_boxes[id].Class == "bottle"||"cola can"||"banana")
  {
    // All the objects needed
    pcl::NormalEstimation<PointT, pcl::Normal> ne;
    pcl::SACSegmentationFromNormals<PointT, pcl::Normal> seg;
    pcl::ExtractIndices<PointT> extract;
    pcl::ExtractIndices<pcl::Normal> extract_normals;
    pcl::search::KdTree<PointT>::Ptr tree (new pcl::search::KdTree<PointT> ());

    // Datasets
    pcl::PointCloud<PointT>::Ptr cloud (new pcl::PointCloud<PointT>);
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
    pcl::PointCloud<PointT>::Ptr cloud_filtered (new pcl::PointCloud<PointT>);
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals2 (new pcl::PointCloud<pcl::Normal>);
    pcl::ModelCoefficients::Ptr coefficients_plane (new pcl::ModelCoefficients), coefficients_cylinder (new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers_plane (new pcl::PointIndices), inliers_cylinder (new pcl::PointIndices);

    cloud = input_pc.makeShared();

    // Estimate point normals
    ne.setSearchMethod (tree);
    ne.setInputCloud (cloud);
    ne.setKSearch (50);
    ne.compute (*cloud_normals);

    // Create the segmentation object for cylinder segmentation and set all the parameters
    seg.setOptimizeCoefficients (true);
    seg.setModelType (pcl::SACMODEL_CYLINDER);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setNormalDistanceWeight (0.1);
    seg.setMaxIterations (10000);
    seg.setDistanceThreshold (0.05);
    seg.setRadiusLimits (0, 0.03);
    seg.setInputCloud (cloud);
    seg.setInputNormals (cloud_normals);

    // Obtain the cylinder inliers and coefficients
    seg.segment (*inliers_cylinder, *coefficients_cylinder);
    // cerr << "Cylinder coefficients: " << *coefficients_cylinder << endl;

    // Write the cylinder inliers to disk
    extract.setInputCloud (cloud);
    extract.setIndices (inliers_cylinder);
    extract.setNegative (false);
    pcl::PointCloud<PointT>::Ptr cloud_cylinder (new pcl::PointCloud<PointT> ());
    extract.filter (*cloud_cylinder);

    pcl::compute3DCentroid(*cloud_cylinder,centroid);
    cerr << "The centroid of the cylindrical pc is: "
         << centroid[0] << ", "
         << centroid[1] << ", "
         << centroid[2] << ", " << endl;

    if (cloud_cylinder->points.empty ())
      cerr << "Can't find the cylindrical component." << endl;
    else
    {
      cerr << "PointCloud representing the cylindrical component: " << cloud_cylinder->points.size () << " data points." << endl;

      // Get the centroid of the cylinder component.
      ObjectPose::Cylinderical_PC_Marker(centroid, coefficients_cylinder);
      double x_arrow,y_arrow,z_arrow;
      x_arrow = coefficients_cylinder->values[3];
      y_arrow = coefficients_cylinder->values[4];
      z_arrow = coefficients_cylinder->values[5];
      radius = coefficients_cylinder->values[6];
      cerr << "The radius of the cylindrical item is:" << radius << endl;
      // cerr << "The central axis of the chosen item is: " << x_arrow << ", " << y_arrow << ", " << z_arrow << endl;
      P = atan(z_arrow/sqrt(pow(x_arrow,2) + pow(y_arrow,2)));     // The pose of the item with respect to arm_link_7.
      Y = atan(y_arrow/x_arrow) + PI/2;        // The angle of the item.

      if ((fabs(P) <= PI/4) && (fabs(P) >= 0))
      {
        object_pose = "lying";                // The item is lying.
        cerr << "The chosen item is lying." << endl;
        cerr << "The pose of the item is: " << Y*180/PI << " degrees" << endl;
      }
      else
      {
        object_pose = "standing";               // The item is standing.
        cerr << "The chosen item is standing." << endl;
        // cerr << "The angle of the item is: " << P*180/PI << endl;
      }

      // Publish other clusters
      pcl::copyPointCloud(*cloud_cylinder,curr_clusters_pc);
      curr_clusters_pc.header.frame_id = "base_link";
      curr_clusters_pc.header.stamp = input_pc.header.stamp;
      pub_clusters_pc_.publish(curr_clusters_pc.makeShared());

      // pcl::io::savePCDFileASCII("/home/flowerfanggirl/test_pcd.pcd",curr_clusters_pc);
    }

  }
  else
  {
    // For the circular staffs.

    if (input_pc.points.empty ())
     cerr << "Can't find the circular component." << endl;
    else
    {
     cerr << "PointCloud representing the circular component: " << input_pc.points.size () << " data points." << endl;

     // Get the centroid of the circular component.
     pcl::compute3DCentroid(input_pc,centroid);
     object_pose = "standing";
     cerr << " The centroid of the circular item pc is: "
          << centroid[0] << ", "
          << centroid[1] << ", "
          << centroid[2] << ", " << endl;

      // Mark circle center in the rviz.
      ObjectPose::Circular_PC_Marker(centroid);
      // Publish other clusters
      pcl::copyPointCloud(input_pc,curr_clusters_pc);
      curr_clusters_pc.header.frame_id = "base_link";
      curr_clusters_pc.header.stamp = input_pc.header.stamp;
      pub_clusters_pc_.publish(curr_clusters_pc.makeShared());
    }
  }
}

pcl::PointCloud<pcl::Normal> ObjectPose::plane_segmentation_normal(const pcl::PointCloud<PointT>& input_pc)
{
  // All the objects needed
  pcl::NormalEstimation<PointT, pcl::Normal> ne;
  pcl::SACSegmentationFromNormals<PointT, pcl::Normal> seg;
  pcl::ExtractIndices<PointT> extract;
  pcl::ExtractIndices<pcl::Normal> extract_normals;
  pcl::search::KdTree<PointT>::Ptr tree (new pcl::search::KdTree<PointT> ());

  // Datasets
  pcl::PointCloud<PointT>::Ptr cloud (new pcl::PointCloud<PointT>);
  pcl::PointCloud<PointT>::Ptr cloud_filtered (new pcl::PointCloud<PointT>);
  pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
  pcl::PointCloud<PointT>::Ptr huge_plane_cloud (new pcl::PointCloud<PointT>);
  pcl::PointCloud<pcl::Normal>::Ptr plane_cloud_normals (new pcl::PointCloud<pcl::Normal>);
  pcl::ModelCoefficients::Ptr coefficients_plane (new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers_plane (new pcl::PointIndices);

  std::cerr << "PointCloud has: " << cloud->points.size () << " data points." << std::endl;

  // Estimate point normals
  ne.setSearchMethod (tree);
  ne.setInputCloud (cloud_filtered);
  ne.setKSearch (50);
  ne.compute (*cloud_normals);

  // Create the segmentation object for the planar model and set all the parameters
  seg.setOptimizeCoefficients (true);
  seg.setModelType (pcl::SACMODEL_NORMAL_PLANE);
  seg.setNormalDistanceWeight (0.1);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setMaxIterations (100);
  seg.setDistanceThreshold (0.03);
  seg.setInputCloud (cloud_filtered);
  seg.setInputNormals (cloud_normals);

  // Obtain the plane inliers and coefficients
  seg.segment (*inliers_plane, *coefficients_plane);
  std::cerr << "Plane coefficients: " << *coefficients_plane << std::endl;

  // Remove the planar inliers, extract the rest
  extract.setNegative (false);
  extract.filter (*huge_plane_cloud);
  extract_normals.setNegative (true);
  extract_normals.setInputCloud (cloud_normals);
  extract_normals.setIndices (inliers_plane);
  extract_normals.filter (*plane_cloud_normals);

  pcl::copyPointCloud(*huge_plane_cloud,curr_clusters_pc);
  curr_clusters_pc.header.frame_id = "base_link";
  curr_clusters_pc.header.stamp = input_pc.header.stamp;
  pub_clusters_pc_.publish(curr_clusters_pc.makeShared());

  return *plane_cloud_normals;
}

void ObjectPose::Cylinderical_PC_Marker(const Eigen::Vector4f& input_centroid, const pcl::ModelCoefficients::Ptr &coeff_cylinder)
{
  // Initialization.
  visualization_msgs::Marker point1;
  visualization_msgs::Marker point2;
  visualization_msgs::Marker line;
  // Set time stamp and frame_id.
  point1.header.frame_id = point2.header.frame_id = line.header.frame_id = "base_link";
  point1.header.stamp = point2.header.stamp = line.header.stamp = ros::Time::now();

  point1.ns = point2.ns = line.ns = "PC_points";
  point1.id = 0;
  point2.id = 1;
  line.id = 2;

  point1.type = point2.type = visualization_msgs::Marker::POINTS;
  line.type = visualization_msgs::Marker::LINE_LIST;
  point1.action = point2.action = line.action = visualization_msgs::Marker::ADD;

  // points.pose.position.x = input_centroid[0];
  // points.pose.position.y = input_centroid[1];
  // points.pose.position.z = input_centroid[2];
  // points.pose.orientation.x = 0.0;
  // points.pose.orientation.y = 0.0;
  // points.pose.orientation.z = 0.0;
  point1.pose.orientation.w = point2.pose.orientation.w = line.pose.orientation.w = 1.0;

  point1.scale.x = 0.02;
  point1.scale.y = 0.02;
  point1.scale.z = 0.02;
  point2.scale.x = line.scale.x = 0.01;
  point2.scale.y = line.scale.y = 0.01;
  point2.scale.z = line.scale.z = 0.01;

  point1.color.r = 1.0;
  point1.color.g = 0.0;
  point1.color.b = 0.0;
  point1.color.a = 1.0;

  point2.color.r = 0.0;
  point2.color.g = 1.0;
  point2.color.b = 0.0;
  point2.color.a = 1.0;

  line.color.r = 0.0;
  line.color.g = 0.0;
  line.color.b = 1.0;
  line.color.a = 1.0;

  point1.lifetime = point2.lifetime = line.lifetime = ros::Duration();

  geometry_msgs::Point centroid_point;
  geometry_msgs::Point origin;
  geometry_msgs::Point cylinder_point1;
  geometry_msgs::Point cylinder_point2;

  centroid_point.x = input_centroid[0];
  centroid_point.y = input_centroid[1];
  centroid_point.z = input_centroid[2];

  origin.x = 0.0;
  origin.y = 0.0;
  origin.z = 0.0;

  cylinder_point1.x = coeff_cylinder->values[0];
  cylinder_point1.y = coeff_cylinder->values[1];
  cylinder_point1.z = coeff_cylinder->values[2];


  cylinder_point2.x = coeff_cylinder->values[0] + coeff_cylinder->values[3];
  cylinder_point2.y = coeff_cylinder->values[1] + coeff_cylinder->values[4];
  cylinder_point2.z = coeff_cylinder->values[2] + coeff_cylinder->values[5];

  point1.points.push_back(centroid_point);
  point2.points.push_back(cylinder_point1);
  line.points.push_back(cylinder_point1);
  line.points.push_back(cylinder_point2);

  vis_pub.publish(point1);
  vis_pub.publish(point2);
  vis_pub.publish(line);
}

void ObjectPose::Circular_PC_Marker(const Eigen::Vector4f& input_centroid)
{
  // Initialization.
  visualization_msgs::Marker circle_centroid;
  // Set time stamp and frame_id.
  circle_centroid.header.frame_id = "base_link";
  circle_centroid.header.stamp = ros::Time::now();

  circle_centroid.ns = "Circular_PC_points";
  circle_centroid.id = 0;

  circle_centroid.type = visualization_msgs::Marker::POINTS;
  circle_centroid.action = visualization_msgs::Marker::ADD;

  circle_centroid.pose.orientation.w = 1.0;

  circle_centroid.scale.x = 0.02;
  circle_centroid.scale.y = 0.02;
  circle_centroid.scale.z = 0.02;

  circle_centroid.color.r = 1.0;
  circle_centroid.color.g = 0.0;
  circle_centroid.color.b = 0.0;
  circle_centroid.color.a = 1.0;

  circle_centroid.lifetime = ros::Duration();

  geometry_msgs::Point centroid_point;

  centroid_point.x = input_centroid[0];
  centroid_point.y = input_centroid[1];
  centroid_point.z = input_centroid[2];

  circle_centroid.points.push_back(centroid_point);

  vis_pub.publish(circle_centroid);
}

// void ObjectPose::plane_segmentation(const pcl::PointCloud<PointT> &origin_pc)
// {
//   pcl::PointCloud<PointT>::Ptr cloud_filtered( new pcl::PointCloud<PointT> ()); // cloud to store the filter the data
//
//   cloud_filtered = origin_pc.makeShared(); // cloud to operate
//   cout << "PointCloud before filtering has: " << cloud_filtered->points.size() << " data points." << endl; //*
//
//
//   // // Down sample the pointcloud using VoxelGrid
//   //
//   // pcl::VoxelGrid<pcl::PointXYZ> pc_filter;
//   // pc_filter.setInputCloud(cloud1);
//   // pc_filter.setLeafSize(0.01f,0.01f,0.01f);
//   // pc_filter.filter(*cloud_filtered);
//
//   // cout << "PointCloud after filtering has: " << cloud_filtered->points.size() << " data points." << endl;
//
//   // Create the segmentation object for pcthe plane model and set all the parameters using pcl::SACSegmentation<pcl::PointXYZ>
//   pcl::SACSegmentation<PointT> seg;
//   pcl::PointIndices::Ptr inliers( new pcl::PointIndices );
//   pcl::ModelCoefficients::Ptr coefficients( new pcl::ModelCoefficients );
//   pcl::PointCloud<PointT>::Ptr cloud_plane( new pcl::PointCloud<PointT>() );
//   pcl::PointCloud<PointT>::Ptr cloud_backup( new pcl::PointCloud<PointT>() );
//
//   cloud_plane->width = 1;
//   cloud_plane->height = 1;
//
//   // set parameters of the SACS segmentation.
//   seg.setOptimizeCoefficients(true);
//   seg.setModelType(pcl::SACMODEL_PLANE);
//   seg.setMethodType(pcl::SAC_RANSAC);
//   seg.setMaxIterations(1000);
//   seg.setDistanceThreshold(0.01);
//
//   int nr_points = (int) cloud_filtered->points.size();
//
//   // Segment the planes using pcl::SACSegmentation::segment() function and pcl::ExtractIndices::filter() function
//       // If you want to extract more than one plane you have to do a while
//   pcl::ExtractIndices<PointT> extract;
//
//   // while (cloud_filtered->points.size () > 0.6 * nr_points)
//   // {
//   // Segment the largest planar compornent from the remaining cloud
//     seg.setInputCloud (cloud_filtered);
//     seg.segment (*inliers, *coefficients);
//     if (inliers->indices.size () == 0)
//     {
//       cerr << "Could not estimate a planar model for the given dataset." << endl;
//       //break;
//     }
//     cerr << "Model coefficients: " << coefficients->values[0] << " "
//                                    << coefficients->values[1] << " "
//                                    << coefficients->values[2] << " "
//                                    << coefficients->values[3] << endl;
//
//     // Extract the inliers
//     extract.setInputCloud (cloud_filtered);
//     extract.setIndices (inliers);
//     extract.setNegative (false);
//     extract.filter (*cloud_backup);
//     cout<<"---------------------------------------------------------------------"<<endl;
//     cout<<"---------------------------------------------------------------------"<<endl;
//     cout<<"points_amount: "<<cloud_backup->points.size()<<endl;
//     cout<<"---------------------------------------------------------------------"<<endl;
//     cout<<"---------------------------------------------------------------------"<<endl;
//     if ((cloud_backup->points.size()) > (cloud_plane->points.size()))
//     {
//         cloud_plane = cloud_backup->makeShared();
//     }
//
//     // Extract the objects on the table.
//     extract.setNegative (true);
//     extract.filter (*cloud_filtered);
//   // }
//
//   // Publish biggest plane
//   // Tips:
//       // - you can copy the pointcloud using pcl::copyPointCloud()
//   // - Set the header frame_id to the pc2 header frame_id
//   // - you can use pcl_conversions::toPCL() to convert the stamp from pc2 header to pointcloud header stamp
//   // - to publish -> pub_plane_pc_.publish( pointcloud_variable.makeShared() )
//
//   pcl::copyPointCloud(*cloud_filtered,curr_plane_pc);
//   curr_plane_pc.header.frame_id = "base_link";
//   curr_plane_pc.header.stamp = pc.header.stamp;
//   pub_clusters_pc_.publish(curr_plane_pc.makeShared());
//
//   // Publish other clusters
//   pcl::copyPointCloud(*cloud_filtered,curr_clusters_pc);
//   curr_clusters_pc.header.frame_id = "base_link";
//   curr_clusters_pc.header.stamp = pc.header.stamp;
//   pub_clusters_pc_.publish(curr_clusters_pc.makeShared());
//
//   pcl::io::savePCDFileASCII("/home/flowerfanggirl/test_pcd.pcd",curr_clusters_pc);
//   // ObjectPose::PclBBox(curr_clusters_pc);
//   // ROS_ERROR_STREAM(__LINE__);
//   return;
// }
//
// void ObjectPose::PclBBox(const pcl::PointCloud<PointT>& src_pc)
// {
//   //Initialising.
//   pcl::PointCloud<PointT>::Ptr cloud (new pcl::PointCloud<PointT> ());
//   pcl::MomentOfInertiaEstimation <PointT> feature_extractor;
//   // ROS_ERROR_STREAM(__LINE__);
//
//   cloud = src_pc.makeShared();
//   feature_extractor.setInputCloud (cloud);
//   feature_extractor.compute ();
//
//   vector <float> moment_of_inertia;
//   vector <float> eccentricity;
//   PointT min_point_AABB;
//   PointT max_point_AABB;
//   PointT min_point_OBB;
//   PointT max_point_OBB;
//   PointT position_OBB;
//   Eigen::Matrix3f rotational_matrix_OBB;
//   float major_value, middle_value, minor_value;
//   Eigen::Vector3f major_vector, middle_vector, minor_vector;
//   Eigen::Vector3f mass_center;
//
//   feature_extractor.getMomentOfInertia (moment_of_inertia);
//   feature_extractor.getEccentricity (eccentricity);
//   feature_extractor.getAABB (min_point_AABB, max_point_AABB);
//   feature_extractor.getOBB (min_point_OBB, max_point_OBB, position_OBB, rotational_matrix_OBB);
//   feature_extractor.getEigenValues (major_value, middle_value, minor_value);
//   feature_extractor.getEigenVectors (major_vector, middle_vector, minor_vector);
//   feature_extractor.getMassCenter (mass_center);
//
//
//   // Set the viewer staffs.
//   // pcl::visualization::PCLVisualizer::Ptr viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
//   // viewer->setBackgroundColor (0, 0, 0);
//   // viewer->addCoordinateSystem (1.0);
//   // viewer->initCameraParameters ();
//   // viewer->addPointCloud<PointT> (cloud, "sample cloud");
//   // viewer->addCube (min_point_AABB.x, max_point_AABB.x, min_point_AABB.y, max_point_AABB.y, min_point_AABB.z, max_point_AABB.z, 1.0, 1.0, 0.0, "AABB");
//   // viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_REPRESENTATION, pcl::visualization::PCL_VISUALIZER_REPRESENTATION_WIREFRAME, "AABB");
//   //
//   // Eigen::Vector3f position (position_OBB.x, position_OBB.y, position_OBB.z);
//   // Eigen::Quaternionf quat (rotational_matrix_OBB);
//   //
//   // viewer->addCube (position, quat, max_point_OBB.x - min_point_OBB.x, max_point_OBB.y - min_point_OBB.y, max_point_OBB.z - min_point_OBB.z, "OBB");
//   // viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_REPRESENTATION, pcl::visualization::PCL_VISUALIZER_REPRESENTATION_WIREFRAME, "OBB");
//   //
//   // ROS_ERROR_STREAM(__LINE__);
//   // PointT center (mass_center (0), mass_center (1), mass_center (2));
//   // PointT x_axis (major_vector (0) + mass_center (0), major_vector (1) + mass_center (1), major_vector (2) + mass_center (2));
//   // PointT y_axis (middle_vector (0) + mass_center (0), middle_vector (1) + mass_center (1), middle_vector (2) + mass_center (2));
//   // PointT z_axis (minor_vector (0) + mass_center (0), minor_vector (1) + mass_center (1), minor_vector (2) + mass_center (2));
//   //
//   // viewer->addLine (center, x_axis, 1.0f, 0.0f, 0.0f, "major eigen vector");
//   // viewer->addLine (center, y_axis, 0.0f, 1.0f, 0.0f, "middle eigen vector");
//   // viewer->addLine (center, z_axis, 0.0f, 0.0f, 1.0f, "minor eigen vector");
//   //
//   // ROS_ERROR_STREAM(__LINE__);
//   // cerr << "mass center is: " << mass_center(0) << mass_center(1) << mass_center(2) << endl;
//   // cerr<< "The vertex_1 of the cube: " << min_point_AABB.x << ", " << min_point_AABB.y << ", " << min_point_AABB.z << endl;
//   // cerr<< "The vertex_2 of the cube: " << max_point_AABB.x << ", " << max_point_AABB.y << ", " << max_point_AABB.z << endl;
//   //
//   // while(!viewer->wasStopped())
//   // {
//   //   viewer->spinOnce (100);
//   //   this_thread::sleep_for(chrono::seconds(1));
//   // }
//   // ROS_ERROR_STREAM(__LINE__);
// }
