#include <object_recognition/myClass/objectpose.h>

int main(int argc, char** argv)
{
    ros::init(argc, argv, "object_pose");
    ros::NodeHandle nh;
    ros::AsyncSpinner spinner(4); // Use 4 threads
    spinner.start();
    ObjectPose node(nh);
    ros::waitForShutdown();
    return 0;
}
